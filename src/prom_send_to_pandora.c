/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*
 *      prom_send_to_pandora.c
 *
 *      Brice Errandonea, Arnaud Blanchard (d'après Philippe Gaussier : prom_enet_debug.c)
 */

/* #define DEBUG 1 */
#include "pandora_connect.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <enet/enet.h>

#include <sys/timeb.h>
#include <signal.h>
#include <math.h>
#include <bits/local_lim.h>

#include "basic_tools.h"
#include "public.h"
#include "net_message_debug_dist.h"
#include "prom_enet_debug.h"
#include "prom_user/include/Struct/prom_images_struct.h"
#include "oscillo_kernel.h"
#include "reseau.h"

extern sem_t enet_pandora_lock;

int pandora_activated = 0;
int pandora_send_phases_info = 0;
int pandora_run_service = 0;
extern int pandora_debug_mem_grp;
extern int pandora_debug_mem_grp_cloture;

pthread_t pandora_thread = 0;
ENetHost *pandora_host = NULL;
ENetAddress pandora_address =
  { 0, 0 };
ENetPeer *pandora_peer = NULL;
pthread_attr_t custom_sched_attr;
int fifo_max_prio, fifo_min_prio;
struct sched_param fifo_param;
struct timeval dernier_timeval;
int pandora_premier_appel = 0;
double periode_update_pando_pour_ce_script = 0.02; // correspond au 50 par defaut de pandora

void free_mem(ENetPacket * packet)
{
  if (packet->data != NULL)
  {
    free(packet->data);
    packet->data = NULL;
  }
}

void pandora_update_service(int listen_time)
{
  char ip[HOST_NAME_MAX];

  int plop;
  ENetEvent event;
  ENetPacket *packet = NULL;
  size_t name_size, groups_size, links_size, packet_size;
  enet_uint8 *packet_data = NULL;
  enet_uint8 *current_data = NULL;
  maj_neuro_enet struct_maj;
  maj_neuro_enet struct_maj2_freq;
  double reception_freq;
  int no_groupe;

  int ret;

  do
  {
    sem_wait(&enet_pandora_lock);
    plop = enet_host_service(pandora_host, &event, listen_time);
    ret = plop > 0;
    sem_post(&enet_pandora_lock);

    if (plop < 0) PRINT_WARNING("ERREUR : enet_host_service retourne failure dans pandora_update_service \n ");
    switch (event.type)
    {
    case ENET_EVENT_TYPE_CONNECT:
      enet_address_get_host_ip(&event.peer->address, ip, HOST_NAME_MAX);
      kprints("A new client connected from ip %s:%i.\n", ip, event.peer->address.port);
      event.peer->data = NULL;
      pandora_peer = event.peer;

      /*Envoi du nom de la description
       Au debut de chaque bloc on met sa taille donc on reserve sizof(size_t) a chaque fois)
       */

      name_size = (strlen(promethe_full_id) + 1) * sizeof(char);
      groups_size = sizeof(type_com_groupe) * nbre_groupe;
      links_size = sizeof(type_liaison) * nbre_liaison;
      packet_size = sizeof(size_t) + name_size + sizeof(size_t) + groups_size + sizeof(size_t) + links_size;
      packet_data = malloc(packet_size);
      if (packet_data == NULL)
      {
        PRINT_WARNING("Fail to allocate memory");
        break;
      }

      current_data = packet_data;

      memcpy(current_data, (void*) &name_size, sizeof(size_t));
      current_data = &current_data[sizeof(size_t)];
      memcpy(current_data, (void*) promethe_full_id, name_size);
      current_data = &current_data[name_size];

      memcpy(current_data, &groups_size, sizeof(size_t));
      current_data = &current_data[sizeof(size_t)];
      init_com_def_groupe((type_com_groupe*) current_data);
      current_data = &current_data[groups_size];
      memcpy(current_data, &links_size, sizeof(size_t));
      current_data = &current_data[sizeof(size_t)];
      memcpy(current_data, (void*) liaison, links_size);

      packet = enet_packet_create(packet_data, packet_size, ENET_PACKET_FLAG_RELIABLE);
      sem_wait(&(enet_pandora_lock));
      if (packet == NULL)
      {
        PRINT_WARNING("The description packet has not been created.");
        break;
      }
      else if (enet_peer_send(event.peer, ENET_PROMETHE_DESCRIPTION_CHANNEL, packet) != 0)
      {
        PRINT_WARNING("The description packet has not been sent.");
        break;
      }
      enet_host_service(pandora_host, &event, 0);
      sem_post(&(enet_pandora_lock));
      pandora_activated = 1;
      pandora_premier_appel = 1;
      free(packet_data);

      break;

    case ENET_EVENT_TYPE_DISCONNECT:

      kprints("%s disconected.\n", (char*) event.peer->data);
      /* Reset the peer's client information. */
      event.peer->data = NULL;
      pandora_activated = 0;
      pandora_run_service = 0;
      pandora_send_phases_info = 0;

      if (pandora_host != NULL)
      {
        enet_host_destroy(pandora_host);
        pandora_host = NULL;
      }
      if (pandora_peer != NULL)
      {
        enet_peer_disconnect(pandora_peer, 0);
        pandora_peer = NULL;
      }

      printf("Deco cote promethe\n");
      break;

    case ENET_EVENT_TYPE_RECEIVE:
      current_data = event.packet->data;

      switch (event.channelID)
      {
      case ENET_MAJ_NEURONE:
        memcpy((void*) &struct_maj, (void*) current_data, sizeof(maj_neuro_enet));
        neurone[struct_maj.no_neuro].s = struct_maj.s;
        neurone[struct_maj.no_neuro].s1 = struct_maj.s1;
        neurone[struct_maj.no_neuro].s2 = struct_maj.s2;

        break;

      case ENET_MAJ_FREQ:
        if(event.packet->dataLength<=sizeof(double))
        {
          memcpy((void*) &reception_freq, (void*) current_data, sizeof(double));
          periode_update_pando_pour_ce_script = (1.0 / reception_freq);
        }
        else
        {
          memcpy((void*) &struct_maj2_freq, (void*) current_data, sizeof(maj_neuro_enet));
          no_groupe=struct_maj2_freq.no_group;
          para_pando_group[no_groupe].frequence_specifique_groupe=(1.0 / struct_maj2_freq.s);
        }
        break;

      default:
        break;

      }

      enet_packet_destroy(event.packet);

      break;
    default:
      break;
    }
  } while (ret > 0);
}

void *pandora_enet_manager(gpointer data)
{
  (void) data;

  pandora_run_service = 1;

  while (pandora_run_service)
  {
    if (pandora_host == NULL) break;

    pandora_update_service(0);
    usleep(100);
  }
  return NULL;
}

/*Appelée par generic_create_and_manage() (dans gestion_threads.c)
 Transmet à Japet les nouvelles valeurs de sortie des neurones du script chaque fois qu'elles changent*/
void send_neurons_to_pandora(int gpe)
{
  size_t packet_size, image_size;
  ENetPacket* packet = NULL;
  enet_uint8* pointeur;
  int i = 0, nbr_coeff = 0, j = 0, nbre_neuro_traitee = 0;
  int nbRefreshedNeurons;
  prom_images_struct *prom_images = NULL;
  type_neurone *neuro = NULL;
  type_coeff *iterateur = NULL;
  type_packet_links packet_links;
  type_neurone* neurons_to_send = NULL;
  double difference_seconde = 0.0;
  double periode_considere;
  // double difference_attendue_seconde=0.0001;
  struct timeval actu;

  gettimeofday(&actu, NULL);
  if (pandora_premier_appel)
  {
    gettimeofday(&(para_pando_group[gpe].dernier_timeval_group), NULL);
    pandora_premier_appel = 0;
  }

  difference_seconde = (double) (((double) (actu.tv_sec - (para_pando_group[gpe].dernier_timeval_group).tv_sec)) + ((double) (actu.tv_usec - (para_pando_group[gpe].dernier_timeval_group).tv_usec) / (double) 1000000));

  //si superieur à -1 c'est la frequence specifique du groupe qui prends le pas sur celle du script
  if(para_pando_group[gpe].frequence_specifique_groupe<0.0) periode_considere=periode_update_pando_pour_ce_script;
  else periode_considere=(double)(para_pando_group[gpe].frequence_specifique_groupe);

  if (pandora_activated == 1 && difference_seconde >= periode_considere)
  {
    gettimeofday(&(para_pando_group[gpe].dernier_timeval_group), NULL);
    if (def_groupe[gpe].debug & FLAG_PANDORA_DEBUG_NEURONS)
    {
      nbRefreshedNeurons = def_groupe[gpe].nbre;
      neurons_to_send = (type_neurone*) &neurone[def_groupe[gpe].premier_ele];
      packet_size = sizeof(type_neurone) * nbRefreshedNeurons;

      if (pandora_peer->reliableDataInTransit < (pandora_peer->windowSize) / 1.5)
      {
        packet = enet_packet_create(neurons_to_send, packet_size, ENET_PACKET_FLAG_RELIABLE | ENET_PACKET_FLAG_NO_ALLOCATE);
        sem_wait(&(enet_pandora_lock));
        if (packet == NULL)
        {
          PRINT_WARNING("The neurons packet (update) has not been created.");
        }
        else if (enet_peer_send(pandora_peer, ENET_UPDATE_NEURON_CHANNEL, packet) != 0)
        {
          PRINT_WARNING("The neurons packet has not been sent.");
          enet_packet_destroy(packet);
        }
        sem_post(&(enet_pandora_lock));
      }
    }

    if ((def_groupe[gpe].debug & FLAG_PANDORA_DEBUG_EXT) && def_groupe[gpe].ext != NULL)
    {
      prom_images = (prom_images_struct *) def_groupe[gpe].ext;
      image_size = prom_images->sx * prom_images->sy * prom_images->nb_band * sizeof(unsigned char);
      packet_size = sizeof(prom_images_struct) + image_size * prom_images->image_number + sizeof(int); // a verifier imagenumber=1

      if (pandora_peer->reliableDataInTransit < (pandora_peer->windowSize) / 1.5)
      {
        packet = enet_packet_create(prom_images, packet_size, ENET_PACKET_FLAG_RELIABLE | ENET_PACKET_FLAG_NO_ALLOCATE);
        packet->freeCallback = free_mem;
        packet->data = malloc(packet_size);

        memcpy(packet->data, &gpe, sizeof(int));
        memcpy(&packet->data[sizeof(int)], prom_images, sizeof(prom_images_struct));
        for (i = 0; i < prom_images->image_number; i++)
        {
          memcpy(&packet->data[sizeof(int) + sizeof(prom_images_struct) + i * image_size], prom_images->images_table[i], image_size);
        }
        sem_wait(&(enet_pandora_lock));
        if (packet == NULL) PRINT_WARNING("The ext packet (update) has not been created.");
        else if (enet_peer_send(pandora_peer, ENET_UPDATE_EXT_CHANNEL, packet) != 0)
        {
          PRINT_WARNING("The neurons packet has not been sent.");
          free(packet->data);
          enet_packet_destroy(packet);
        }
        sem_post(&(enet_pandora_lock));
      }
    }
    if (def_groupe[gpe].debug & FLAG_PANDORA_DEBUG_NEURO_LINKS) // si il y a des neurones à envoyer dans le groupe
    {
      if (pandora_peer->reliableDataInTransit < (pandora_peer->windowSize) / 1.5) //si la bande passante n'est pas saturee
      {
        packet_links.sub_packet_link = MANY_ALLOCATIONS(para_pando_group[gpe].nbre_neuronne_to_send_links, type_sub_packet_link);
        packet_links.nb_neuro = para_pando_group[gpe].nbre_neuronne_to_send_links;

        packet_size = sizeof(int);

        nbre_neuro_traitee = 0;
        for (j = def_groupe[gpe].premier_ele; j < def_groupe[gpe].nbre + def_groupe[gpe].premier_ele; j++)
        {
          if (info_neurone_pour_pando[j].have_to_send_link == 1)
          {
            packet_links.sub_packet_link[nbre_neuro_traitee].no_neuro = j;
            packet_size += sizeof(int);

            neuro = &(neurone[j]);
            iterateur = neuro->coeff;
            i = 0;
            while (iterateur != NULL)
            {
              i++;
              iterateur = iterateur->s;
            }
            nbr_coeff = i;
            neuro->nbre_coeff = nbr_coeff; //TODO : corriger nbr_coeff pour eviter ce genre de boucle

            packet_links.sub_packet_link[nbre_neuro_traitee].nb_neuro_link = neuro->nbre_coeff;
            packet_size += sizeof(int);

            if (neuro->nbre_coeff > 0)
            {
              packet_links.sub_packet_link[nbre_neuro_traitee].coeffs = MANY_ALLOCATIONS(nbr_coeff, type_coeff);
              iterateur = neuro->coeff;
              for (i = 0; i < nbr_coeff; i++)
              {
                memcpy(&(packet_links.sub_packet_link[nbre_neuro_traitee].coeffs[i]), iterateur, sizeof(type_coeff));
                iterateur = iterateur->s;
              }
            }
            packet_size += sizeof(type_coeff) * neuro->nbre_coeff;

            nbre_neuro_traitee++;

          }
        }

        packet = enet_packet_create(NULL, packet_size, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT | ENET_PACKET_FLAG_UNSEQUENCED | ENET_PACKET_FLAG_NO_ALLOCATE);
        packet->freeCallback = free_mem;
        packet->data = malloc(packet_size);
        pointeur = packet->data;

        memcpy(packet->data, &(packet_links.nb_neuro), sizeof(int));

        pointeur = &((packet->data)[sizeof(int)]);

        for (j = 0; j < packet_links.nb_neuro; j++)
        {

          memcpy(pointeur, &(((packet_links.sub_packet_link)[j]).no_neuro), sizeof(int));
          pointeur = &(pointeur[sizeof(int)]);

          memcpy(pointeur, &(((packet_links.sub_packet_link)[j]).nb_neuro_link), sizeof(int));
          pointeur = &(pointeur[sizeof(int)]);

          if (packet_links.sub_packet_link[j].nb_neuro_link > 0)
          {
            memcpy(pointeur, packet_links.sub_packet_link[j].coeffs,(sizeof(type_coeff) * packet_links.sub_packet_link[j].nb_neuro_link));
            pointeur = &(pointeur[(sizeof(type_coeff) * packet_links.sub_packet_link[j].nb_neuro_link)]);
            free(packet_links.sub_packet_link[j].coeffs);
          }
        }

        sem_wait(&(enet_pandora_lock));
        if (packet == NULL) PRINT_WARNING("The links packet (update) has not been created.");
        else if (enet_peer_send(pandora_peer, ENET_UPDATE_LINKS, packet) != 0)
        {
          PRINT_WARNING("The links packet has not been sent.");
        }
        sem_post(&(enet_pandora_lock));

        free(packet_links.sub_packet_link);

      }

      /*

       //limite
       no_neuro = para_pando_group[gpe].neuronne_to_send_links;
       neuro = &(neurone[no_neuro]);

       iterateur = neuro->coeff;
       i = 0;
       while (iterateur != NULL)
       {
       i++;
       iterateur = iterateur->s;
       }
       nbr_coeff = i;
       neuro->nbre_coeff = nbr_coeff; //TODO : corriger nbr_coeff

       if (nbr_coeff == 0)
       {
       return;
       }

       packet_size = (sizeof(type_coeff) * nbr_coeff) + sizeof(int);

       if (pandora_peer->reliableDataInTransit < (pandora_peer->windowSize) / 1.5)
       {
       packet_links.coeffs = MANY_ALLOCATIONS(nbr_coeff, type_coeff);

       iterateur = neuro->coeff;
       for (i = 0; i < nbr_coeff; i++)
       {
       memcpy(&(packet_links.coeffs[i]), iterateur, sizeof(type_coeff));
       iterateur = iterateur->s;
       }

       packet = enet_packet_create(NULL, packet_size, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT | ENET_PACKET_FLAG_UNSEQUENCED | ENET_PACKET_FLAG_NO_ALLOCATE);
       packet->freeCallback = free_mem;
       packet->data = malloc(packet_size);
       memcpy(packet->data, &no_neuro, sizeof(int));
       memcpy(&(packet->data[sizeof(int)]), packet_links.coeffs, (sizeof(type_coeff) * nbr_coeff));

       sem_wait(&(enet_pandora_lock));
       if (packet == NULL) PRINT_WARNING("The links packet (update) has not been created.");
       else if (enet_peer_send(pandora_peer, ENET_UPDATE_LINKS, packet) != 0)
       {
       PRINT_WARNING("The links packet has not been sent.");
       }
       sem_post(&(enet_pandora_lock));
       if (packet_links.coeffs != NULL) free(packet_links.coeffs);
       }

       */

    }

    /*
     if (send_control && (!strcmp(def_groupe[gpe].nom,"f_vue_metres") || !strcmp(def_groupe[gpe].nom,"f_checkbox")) && def_groupe[gpe].nbre_de_1==1) // on utilise nbre_de_1 (variable inutilisé, pour savoir si le groupe est modifié ou non)
     {
     packet_size = sizeof(type_neurone) * nbRefreshedNeurons;

     if (pandora_peer->reliableDataInTransit < (pandora_peer->windowSize) / 1.5)
     {
     packet = enet_packet_create(&neurone[def_groupe[gpe].premier_ele], packet_size, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT | ENET_PACKET_FLAG_UNSEQUENCED | ENET_PACKET_FLAG_NO_ALLOCATE);
     sem_wait(&(enet_pandora_lock));
     if (packet == NULL)
     {
     PRINT_WARNING("The neurons packet (update) has not been created.");
     }
     else if (enet_peer_send(pandora_peer, ENET_UPDATE_NEURON_CHANNEL, packet) != 0)
     {
     PRINT_WARNING("The neurons packet has not been sent.");
     }
     sem_post(&(enet_pandora_lock));
     }
     }
     */

  }
}

void send_token_pandora(int gpe, int phase, long temps)
{
  ENetPacket* packet = NULL;
  enet_uint8 *data = NULL;
  enet_uint8 *cursor = NULL;
  size_t packet_size = 0;

  if (pandora_activated == 1 && pandora_send_phases_info == 1)
  {

    packet_size = sizeof(int) * 2 + sizeof(long);
    cursor = data = (enet_uint8 *) malloc(packet_size);
    *((int *) cursor) = gpe;
    cursor = &cursor[sizeof(int)];
    *((long *) cursor) = temps;
    cursor = &cursor[sizeof(long)];
    *((int *) cursor) = phase;

    packet = enet_packet_create(data, packet_size, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT | ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT | ENET_PACKET_FLAG_NO_ALLOCATE);
    packet->freeCallback = free_mem;
    sem_wait(&(enet_pandora_lock));
    if (packet == NULL)
    {
      PRINT_WARNING("The debug info packet (update) has not been created.");
    }
    else if (enet_peer_send(pandora_peer, ENET_UPDATE_PHASES_INFO_CHANNEL, packet) != 0)
    {
      PRINT_WARNING("The debug info packet has not been sent.");
    }
    sem_post(&(enet_pandora_lock));
  }
}

/* init network for udp connexion using enet : used for distant oscillo_kernel */
/* ip_adr= 127.0.0.1 par ex ou nom_adr=localhost... */

void* pandora_connect(void *data)
{
  arg_pandora* arguments = (arg_pandora*) data;
  char* ip_adr = ((arg_pandora*) data)->ivyget;
  int port = ((arg_pandora*) data)->port;
  int compression = ((arg_pandora*) data)->compression;

  if ((pandora_activated != 0) || (pandora_send_phases_info != 0) || (pandora_host != NULL) || (pandora_peer != NULL))
  {
    pandora_disconnect();
  }

  if (enet_initialize() < 0)
  {
    PRINT_WARNING("An error occurred while initializing ENet.\nAn error occurred while initializing ENet.\n");
    //sem_post(&(pandora_lock));
    free(data);
    return NULL;
  }
  atexit(enet_deinitialize);
  atexit(pandora_disconnect);
  enet_time_set(0);

  sem_wait(&(enet_pandora_lock));
  pandora_host = enet_host_create(NULL, 1, ENET_NUMBER_OF_CHANNELS, ENET_UNLIMITED_BANDWITH, ENET_UNLIMITED_BANDWITH);
  sem_post(&(enet_pandora_lock));

  if (pandora_host == NULL)
  {
    PRINT_WARNING("An error occurred while trying to create an ENet client host.\n");
    free(data);
    return NULL;
  }

  if (enet_address_set_host(&(pandora_address), ip_adr) < 0)
  {
    PRINT_WARNING("Error with address %s", ip_adr);
    free(data);
    return NULL;
  }
  pandora_address.port = port;

  sem_wait(&(enet_pandora_lock));
  pandora_peer = enet_host_connect(pandora_host, &(pandora_address), ENET_NUMBER_OF_CHANNELS, 0);

  if (compression == 1) enet_host_compress_with_range_coder(pandora_host);
  sem_post(&(enet_pandora_lock));

  pandora_update_service(1);
  pandora_is_activating = 0;

  pthread_attr_init(&custom_sched_attr);
  pthread_attr_setinheritsched(&custom_sched_attr, PTHREAD_EXPLICIT_SCHED);
  pthread_attr_setschedpolicy(&custom_sched_attr, SCHED_BATCH);
  fifo_max_prio = sched_get_priority_max(SCHED_BATCH);
  fifo_min_prio = sched_get_priority_min(SCHED_BATCH);
  fifo_param.sched_priority = fifo_max_prio;
  pthread_attr_setschedparam(&custom_sched_attr, &fifo_param);

  pthread_create(&pandora_thread, &custom_sched_attr, pandora_enet_manager, NULL);
  pthread_setschedparam(pandora_thread, SCHED_BATCH, &fifo_param);

  free(arguments->ivyget);
  free(data);
  return NULL;
}

void pandora_disconnect()
{
  pandora_activated = 0;
  pandora_send_phases_info = 0;
  pandora_run_service = 0;
  if (pandora_debug_mem_grp == 1)
  {
    pandora_debug_mem_grp_cloture = 1; // Sécurite : Si le mode debug fonctionne, alors on lance la cloture de ce mode à la fermeture de pandora, appelant ainsi la routine de cloture dans gestion_thread.
  }
  if (pandora_peer != NULL)
  {
    enet_peer_disconnect(pandora_peer, 0);
    pandora_peer = NULL;
  }
  if (pandora_host != NULL)
  {
    enet_host_destroy(pandora_host);
    pandora_host = NULL;
    /* enet_deinitialize(); */
  }
}

