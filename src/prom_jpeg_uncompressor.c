/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * prom_jpeg_uncompressor.c
 *
 * A JPEG uncompressor for prom_images_struct_compressed -> prom_images_struct
 *
 *  Created on: 31 juil. 2014
 *      Author: jfellus
 */


#include <stdio.h>
#include <string.h>

#include <net_message_debug_dist.h>
#include <libx.h>
#include <limits.h>

#include <jpeglib.h>
#include <setjmp.h>
#include "prom_jpeg_uncompressor.h"



METHODDEF(void) my_error_exit (j_common_ptr cinfo) {(void)cinfo;}


struct _prom_jpeg_uncompressor {
	prom_images_struct_compressed *images_in;
	prom_images_struct *images_out;

	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
};



prom_jpeg_uncompressor* prom_jpeg_uncompressor_create(prom_images_struct_compressed* images_in, prom_images_struct* images_out) {
	prom_jpeg_uncompressor* data = ALLOCATION(prom_jpeg_uncompressor);
	data->images_in = images_in;
	if(images_out == NULL) {
		int i;
		images_out = ALLOCATION(prom_images_struct);
		images_out->image_number = images_in->super.image_number;
		images_out->sx = images_in->super.sx;
		images_out->sy = images_in->super.sy;
		images_out->nb_band = images_in->super.nb_band;
		for(i=0; i<images_out->image_number; i++) images_out->images_table[i] = MANY_ALLOCATIONS(images_out->sx*images_out->sy*images_out->nb_band, unsigned char);
	}
	data->images_out = images_out;
	data->jerr.error_exit = my_error_exit;
	data->cinfo.err = jpeg_std_error(&data->jerr);
	jpeg_create_decompress(&data->cinfo);
	return data;
}

void prom_jpeg_uncompressor_destroy(prom_jpeg_uncompressor* c) {
	jpeg_destroy_decompress(&c->cinfo);
	free(c); c = NULL;
}

void prom_jpeg_uncompressor_uncompress(prom_jpeg_uncompressor* data){
	int i, row_stride;
	JSAMPROW p[1];


	for(i=0;i<data->images_in->super.image_number;i++){
		jpeg_mem_src(&data->cinfo, data->images_in->super.images_table[i], data->images_in->images_size[i]);
		jpeg_read_header(&data->cinfo, TRUE);
		row_stride = data->cinfo.output_width * data->cinfo.output_components;
		jpeg_start_decompress(&data->cinfo);
		p[0] = data->images_out->images_table[i];
		while (data->cinfo.output_scanline < data->cinfo.output_height) {
			jpeg_read_scanlines(&data->cinfo, p, 1);
			p[0] += row_stride;
		}

		jpeg_finish_decompress(&data->cinfo);
	}
}

prom_images_struct_compressed* prom_jpeg_uncompressor_get_in(prom_jpeg_uncompressor* c) {return c->images_in;}
prom_images_struct* prom_jpeg_uncompressor_get_out(prom_jpeg_uncompressor* c) {return c->images_out;}
