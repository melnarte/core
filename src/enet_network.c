/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 * @author Arnaud Blanchard
 * @date
 *
 * @description
 * UNe fonction send ou receive envoie via ivy, une information de disponiblité (available) ou d'attente (waiting) d'une variable donnee.
 * Si une variable est ettendu respectivement disponible alors une connection est utilisée ou se crée entre les 2 promethes.
 * De même pour creer une connection, le promethe indique la presence de son serveur enet et son port.
 * Alors s'il fait partie des connections nécessaires, le promethe distant se connectera sur ce serveur.
 *
 */

#include "../include/enet_network.h"
#include "basic_tools.h"
#include "prom_bus.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <enet/enet.h>

#define CONNECTIONS_MAX 64
#define DEFAULT_PORT 21000
#define CREATE_SERVER_TRIES_MAX 64

type_connection *connections[CONNECTIONS_MAX];
type_net_var *net_vars[NET_VARS_MAX];

int connections_nb = 0;
int net_vars_nb = 0;
int current_port = DEFAULT_PORT;
ENetHost *server_host = NULL;
int server_port = -1;
sem_t server_semaphore;
sem_t manual_semaphore;
sem_t enet_network_semaphore;

FILE *log_file;

type_connection * connection_find_with_peer(ENetPeer *peer);
void connection_disconnect(type_connection * connection);

void connection_send_buffer(type_connection *connection, int channel, const char* buffer, size_t size);
void net_var_declare(type_net_var *net_var);

/*
 *
 * Sera utile pour debugger le réseau
 */
void enet_network_init()
{
	sem_init(&enet_network_semaphore, 0, 1);
	sem_init(&manual_semaphore,0,1);
	/*
   log_file = fopen("enet_network.dot", "w");
   fprintf(log_file, "digraph enet_network {\n");*/
}

void enet_network_finish()
{
	sem_destroy(&enet_network_semaphore);
	/* fprintf(log_file, "}\n");
   fclose(log_file);*/
}

/*
 * Add a connection to the list of possible connections.
 *  Does not mean that the connection is or will be connected.
 */
type_connection * connection_new(const char *prom_name, const char *ip, int port){
	type_connection *connection = ALLOCATION(type_connection);

	if (connections_nb >= CONNECTIONS_MAX) EXIT_ON_ERROR("Too many connections (%d).", connections_nb);
	connections[connections_nb] = connection;
	connections_nb++;

	strncpy(connection->prom_name, prom_name, NAME_MAX);
	strncpy(connection->ip, ip, HOST_NAME_MAX);
	connection->port = port;
	connection->host = NULL;
	connection->type = CONNECTION_DISCONNECTED;
	connection->busy_channels_nb = 1; /* Channel 0 is for management of the variables */

	return connection;
}

void net_var_connect_channel(type_net_var *net_var){
	type_connection *connection = net_var->connection;

	net_var->channel = connection->busy_channels_nb;
	net_var->connected = 1;
	connection->net_vars[net_var->channel] = net_var;
	connection_send_buffer(net_var->connection, 0, (const char*) net_var, sizeof(type_net_var));
	kprints("Asked connection var '%s' on channel %d recv callback '%p'\n", net_var->name, net_var->channel, net_var->recv_callback);
	connection->busy_channels_nb++;
}

/**
 *
 * Once a connection is done to a enet peer, we set a channel for each variable using this connection.
 *
 */

void connection_connect_net_vars(type_connection *connection)
{
	int i;
	type_net_var *net_var;

	for (i = 0; i < net_vars_nb; i++)
	{
		net_var = net_vars[i];

		kprints("netvar %s type %d, net_var pointer %p\n", net_var->name, net_var->type, net_var->recv_callback);

		if (net_var->connection == connection && !net_var->connected)
		{

			net_var_connect_channel(net_var);
		}
	}
}


void connection_disconnect(type_connection * connection){
	int i;
	type_net_var *net_var;


	for (i = 0; i < net_vars_nb; i++){
		net_var = net_vars[i];
		if (net_var->connection == connection && net_var->connected){
			net_var->connected = 0;
			net_var->channel = -1;
		}
	}
	enet_peer_reset(connection->peer);
	connection->peer = NULL;
	connection->type = CONNECTION_TRYING;
	connection->busy_channels_nb = 1;
}

/**
 *
 * When we loose a connection we set all the variables using this connection as not connected.
 *
 */
void connection_remove(type_connection *connection)
{
	int i;
	type_net_var *net_var;

	for (i = 0; i < net_vars_nb; i++)
	{
		net_var = net_vars[i];
		if (net_var->connection == connection)
		{

			if ((net_var->type == NET_VAR_WAITING) || (net_var->type == NET_VAR_AVAILABLE)) /** The distant script is probably dead. We remove its variables */
			{
				net_vars_nb--;
				net_vars[i] = net_vars[net_vars_nb];
				free(net_var);
				i--; /* Il faut tester la variable qui vient de prendre la place de i */
			}
			else /* Sinon on note juste que les variables ne sont plus connectee */
			{
				net_var->connected = 0;
				net_var->connection = NULL;
			}
		}
	}

	for (i = 0; i < connections_nb; i++)
	{
		if (connection == connections[i])
		{
			connections_nb--;
			connections[i] = connections[connections_nb];
			if (connection->semaphore != &server_semaphore)
			{
				sem_destroy(connection->semaphore);
				free(connection->semaphore);
			}
			free(connection);
			break;
		}
	}
}

int enet_command_waiting(ENetHost *host)
{
	unsigned int i;
	int command_waiting = 0;

	for (i = 0; i < host->peerCount; i++)
	{
		if (!enet_list_empty(&(host->peers[i].outgoingReliableCommands)) || !enet_list_empty(&(host->peers[i].outgoingUnreliableCommands)))
		{
			command_waiting = 1;
			break;
		}
	}
	return command_waiting;
}

void enet_handle_receive_data(type_connection *connection, ENetEvent event){
	type_net_var *net_var = connection->net_vars[event.channelID];
	if (net_var->recv_callback != NULL ){
		net_var->recv_callback((char*) event.packet->data, event.packet->dataLength, net_var->user_data);
	}
	else PRINT_WARNING("Data for variable '%s' received on channel %d but no callback associated ", net_var->name, event.channelID);
}

/*
 * One thread running for the server if needed. TODO manage the disconnection of all clients and stop it.
 * Some parts should be mutualised with enet_client_manager.
 *  The granularity is 1ms
 *
 */
static void *enet_server_manager(ENetHost *host)
{
	ENetEvent event;
	type_net_var distant_net_var, *net_var;
	type_connection *connection;
	char ip[HOST_NAME_MAX + 1];
	int ret;

	while (1)
	{
		do
		{
			sem_wait(&server_semaphore);
			ret = enet_host_service(host, &event, 0);
			sem_post(&server_semaphore);

			switch (event.type)
			{
			case ENET_EVENT_TYPE_CONNECT:

				enet_address_get_host(&event.peer->address, ip, HOST_NAME_MAX);
				connection = connection_new(ip, ip, event.peer->address.port);
				connection->peer = event.peer;
				event.peer->data = connection;
				connection->semaphore = &server_semaphore;
				connection->type = CONNECTION_CONNECTED;
				kprints("Connected from client '%s' \n", connection->ip);

				break;

			case ENET_EVENT_TYPE_RECEIVE:
				connection = event.peer->data;
				if (event.channelID == 0)
				{
					if (event.packet->dataLength != sizeof(type_net_var)) EXIT_ON_ERROR("Packet not of the size of a type_net_var");
					memcpy(&distant_net_var, event.packet->data, sizeof(type_net_var));
					net_var = net_var_find(distant_net_var.name);
					net_var->channel = distant_net_var.channel;
					connection->net_vars[distant_net_var.channel] = net_var;
					net_var->connection = connection;
					net_var->connected = 1;
					kprints("Connected from '%s' for variable '%s' channel '%d'\n", connection->ip, net_var->name, net_var->channel);
				}
				else
				{
					enet_handle_receive_data(connection, event);
				}
				enet_packet_destroy(event.packet);
				break;

			case ENET_EVENT_TYPE_DISCONNECT:
				connection = event.peer->data;
				kprints("Disconnected from '%s'\n", connection->ip);
				connection_remove(connection);
				if (connections_nb == 0) return NULL ;
				break;

			default:
				break;
			}
		} while (ret > 0);
		usleep(1000); /* Granularité 1000 Hz 1 ms */
	}
	return NULL ;
}

/*
 * Thread running for each client. TODO manage the lost of connection.
 * The granularity is 1ms
 *
 */
static void *enet_client_manager(type_connection *connection)
{
	ENetEvent event;
	int ret;

	while (1)
	{
		do
		{
			sem_wait(connection->semaphore);
			ret = enet_host_service(connection->host, &event, 0);
			sem_post(connection->semaphore);

			switch (event.type)
			{
			case ENET_EVENT_TYPE_CONNECT:
				connection->type = CONNECTION_CONNECTED;
				kprints("Connected to '%s'\n", connection->prom_name);
				connection_connect_net_vars(connection);
				break;

			case ENET_EVENT_TYPE_RECEIVE:
				enet_handle_receive_data(connection, event);
				break;

			case ENET_EVENT_TYPE_DISCONNECT:
				kprints("Disconnected to '%s'\n", connection->prom_name);
				connection_remove(connection);
				return NULL ;
				break;

			default:
				break;
			}
		} while(ret>0);
		usleep(1000); /* Granularité 1KHz 1 ms */
	}
	return NULL ;
}

static void *enet_manual_manager(type_connection *connection){

	ENetEvent event;
	ENetAddress address;
	ENetHost *host = connection->host;
	int ret;

	printf("Starting new ENetServer on port %i.\n", connection->port);

	sem_wait(connection->semaphore);
	ret = enet_host_service(host, &event, 500);
	sem_post(connection->semaphore);

	if (ret > 0 && event.type == ENET_EVENT_TYPE_CONNECT){
		connection->type = CONNECTION_CONNECTED;
		connection_connect_net_vars(connection);
		enet_client_manager(connection);
	}else{
		address.host = ENET_HOST_ANY;
		address.port = connection->port;
		host = connection->host = enet_host_create(&address, CONNECTIONS_MAX, NET_VARS_MAX, ENET_UNLIMITED_BANDWITH, ENET_UNLIMITED_BANDWITH);

		while(1){
			ret = 0;
			do{
				sem_wait(connection->semaphore);
				ret = enet_host_service(host, &event, 0);
				sem_post(connection->semaphore);

				switch(event.type){
				case ENET_EVENT_TYPE_CONNECT:
					if(connection->type == CONNECTION_TRYING){
						connection->type = CONNECTION_CONNECTED;
						connection->peer = event.peer;
						connection_connect_net_vars(connection);
						printf("Connection to %s:%i successful.\n",connection->ip, connection->port);
					}
					break;
				case ENET_EVENT_TYPE_RECEIVE:
					enet_handle_receive_data(connection,event);
					break;
				case ENET_EVENT_TYPE_DISCONNECT:
					if(connection->type == CONNECTION_CONNECTED){
						connection_disconnect(connection);
						printf("Disconnected from %s:%i.\n", connection->ip, connection->port);
					}
					break;
				default:
					break;
				}

			}while(ret > 0);
			usleep(50);
		}

	}

	return NULL;
}

void enet_close_connections()
{
	int i;

	for (i = 0; i < connections_nb; i++)
	{
		if (connections[i]->type == CONNECTION_CONNECTED) enet_peer_disconnect(connections[i]->peer, 0);
		free(connections[i]);
	}
	connections_nb = 0;
}

/**
 * Create an enet server.
 * Tries many times if the port is busy.
 * It should have only one server for a program with as many clients as we need.
 */
void create_server()
{
	ENetAddress address;
	int connection_tries_nb = 0;
	pthread_t enet_thread;
	pthread_attr_t pthread_attr;

	address.host = ENET_HOST_ANY;
	for (connection_tries_nb = 0; connection_tries_nb < CREATE_SERVER_TRIES_MAX; connection_tries_nb++)
	{
		address.port = current_port;
		server_host = enet_host_create(&address, CONNECTIONS_MAX, NET_VARS_MAX, ENET_UNLIMITED_BANDWITH, ENET_UNLIMITED_BANDWITH);
		current_port++;

		if (server_host != NULL )
		{
			server_port = address.port;
			sem_init(&server_semaphore, 0, 1);
			pthread_attr_init(&pthread_attr);
			pthread_create(&enet_thread, &pthread_attr, (void*(*)(void*)) enet_server_manager, server_host);

			/*  for (i = 0; i < net_vars_nb; i++)
       {
       net_var = net_vars[i];
       if ((net_var->connection == NULL )|| (net_var->connection->type == CONNECTION_DISCONNECTED)){
       net_var_declare(net_var);
       }
       }*/
			break;
		}
	}
	if (server_host == NULL ) EXIT_ON_ERROR("Fail to create an enet server after %d tries. The last port tried was : %d", connection_tries_nb, address.port);
}

/**
 * Find 'connection', the parameters of a connection with the name of the distant script.
 */
type_connection* connection_find(const char *prom_name)
{
	int i;

	for (i = 0; i < connections_nb; i++)
	{
		if ((strncmp(connections[i]->prom_name, prom_name, NAME_MAX) == 0)) return connections[i];
	}
	return NULL ;
}

/**
 * Find 'connection', the parameters of a connection with the enet peer.
 */
type_connection * connection_find_with_peer(ENetPeer *peer)
{
	int i;

	for (i = 0; i < connections_nb; i++)
	{
		if (connections[i]->peer == peer) return connections[i];
	}
	return NULL ;
}

/**
 * Send data on a specific channel using the 'connection'/
 */
void connection_send_buffer(type_connection *connection, int channel, const char* buffer, size_t size)
{

	ENetPacket *packet;

	packet = enet_packet_create(buffer, size, ENET_PACKET_FLAG_RELIABLE);
	sem_wait(connection->semaphore);
	enet_peer_send(connection->peer, channel, packet);
	sem_post(connection->semaphore);
}

/**
 * Return a connection. Either by finding one for 'prom_name' or creating a new one with the parameter ip and port.
 */
type_connection *connection_get(const char *prom_name, const char *ip, int port)
{
	type_connection *connection;

	connection = connection_find(prom_name);
	if (connection == NULL ) connection = connection_new(prom_name, ip, port);
	else
	{
		strncpy(connection->ip, ip, HOST_NAME_MAX);
		connection->port = port;
	}

	return connection;
}

/*
 * Broadcast a message on ivy to declare the net_var
 */
void net_var_declare(type_net_var *net_var)
{
	if (net_var->type == NET_VAR_SEND) prom_bus_send_message("available_variable(%s, %d)", net_var->name, server_port);
	else if (net_var->type == NET_VAR_RECV) prom_bus_send_message("waiting_variable(%s, %d)", net_var->name, server_port);
}

/**
 * Connect the variable. Either creating a server and broadcasting the information or by creating and connecting a client to an existing server.
 */
void net_var_connect(type_net_var *net_var)
{
	ENetAddress address;
	pthread_t enet_thread;
	pthread_attr_t pthread_attr;

	type_connection *connection = net_var->connection;

	if (connection->port == -1)
	{
		if (server_host == NULL ) create_server();
		net_var_declare(net_var);
	}
	else if (connection->type == CONNECTION_DISCONNECTED)
	{
		enet_address_set_host(&address, connection->ip);
		address.port = connection->port;
		connection->host = enet_host_create(NULL, CONNECTIONS_MAX, NET_VARS_MAX, ENET_UNLIMITED_BANDWITH, ENET_UNLIMITED_BANDWITH);
		if (connection->host == NULL ) EXIT_ON_ERROR("Fail creating the client host for connection with : %s", connection->prom_name);
		connection->peer = enet_host_connect(connection->host, &address, NET_VARS_MAX, 0);
		if (connection->peer == NULL ) EXIT_ON_ERROR("Failing to create peer in connection to '%s'", connection->ip);
		connection->type = CONNECTION_TRYING;
		connection->semaphore = ALLOCATION(sem_t);

		sem_init(connection->semaphore, 0, 1);
		pthread_attr_init(&pthread_attr);

		if(net_var->mode == NET_VAR_AUTO)
			pthread_create(&enet_thread, &pthread_attr, (void*(*)(void*)) enet_client_manager, connection);
		else
			pthread_create(&enet_thread, &pthread_attr, (void*(*)(void*)) enet_manual_manager, connection);
	}
	else if (connection->type == CONNECTION_CONNECTED) net_var_connect_channel(net_var);
}

/**
 * Find a variable with its name. Return NULL if it does not exist.
 */
type_net_var *net_var_find(const char *name)
{
	int i;
	for (i = 0; i < net_vars_nb; i++)
		if (strncmp(net_vars[i]->name, name, NAME_MAX) == 0) return net_vars[i];
	return NULL ;
}

/**
 * Declare a new variable. 4 types are possible.
 */

type_net_var *net_var_new(const char* name, NET_VAR_TYPE type, NET_VAR_MODE mode)
{
	type_net_var *net_var = ALLOCATION(type_net_var);

	net_vars[net_vars_nb] = net_var;
	strncpy(net_var->name, name, NAME_MAX);
	net_var->connection = NULL;
	net_var->connected = 0;
	net_var->type = type;
	net_var->mode = mode;
	net_vars_nb++;
	return net_var;
}

/**
 * Declare a variable waiting or available from a distant script.
 */
type_net_var *net_var_distant_new(const char *name, NET_VAR_TYPE type, NET_VAR_MODE mode, const char* prom_name, const char *ip, int port)
{
	type_net_var *net_var;

	net_var = net_var_new(name, type, mode);
	net_var->connection = connection_get(prom_name, ip, port);

	return net_var;
}

/*
 * Declare a variable to be sent. Typicaly from a f_send_enet
 */

type_net_var *net_var_send(const char *name, type_recv_callback recv_callback, void *user_data)
{
	type_net_var *net_var;

	sem_wait(&enet_network_semaphore);

	net_var = net_var_find(name);
	if (net_var == NULL )
	{
		net_var = net_var_new(name, NET_VAR_SEND,NET_VAR_AUTO);
		net_var->user_data = user_data;
		net_var->recv_callback = recv_callback;
		net_var_declare(net_var);
	}
	else
	{
		if ((net_var->type == NET_VAR_SEND) || (net_var->type == NET_VAR_RECV)) EXIT_ON_ERROR("You use twice the same variable in one script");
		net_var->user_data = user_data;
		net_var->recv_callback = recv_callback;
		net_var_connect(net_var);
	}

	kprints("'send %p'\n", recv_callback);
	sem_post(&enet_network_semaphore);

	return net_var;
}


type_net_var * net_var_send_manual(const char *name, const char *ip, int port, type_recv_callback recv_callback, void *user_data){

	type_net_var *net_var = NULL;
	char prom_name[30];
	sprintf(prom_name, "%s:%i:manual", ip,port);

	printf("Declare new SEND_VAR on port %i\n", port);

	sem_wait(&manual_semaphore);

	net_var = net_var_find(name);

	if(net_var == NULL){
		net_var = net_var_new(name,NET_VAR_SEND, NET_VAR_MANUAL);
		net_var->user_data = user_data;
		net_var->recv_callback = recv_callback;
		net_var->connection = connection_get(prom_name,ip,port);
		net_var_connect(net_var);
	}else{
		EXIT_ON_ERROR("Manual connection attempt failed for variable \"%s\" : variable already exists.",name);
	}

	sem_post(&manual_semaphore);

	return net_var;

}

/*
 * Declare a variable to be received. Typicaly from a f_recv_enet
 */
type_net_var *net_var_recv(const char *name, type_recv_callback recv_callback, void *user_data)
{
	type_net_var *net_var;
	sem_wait(&enet_network_semaphore);

	net_var = net_var_find(name);

	if (net_var == NULL ) /* No available variable yet. */
	{
		net_var = net_var_new(name, NET_VAR_RECV, NET_VAR_AUTO);
		net_var->user_data = user_data;
		net_var->recv_callback = recv_callback;
		net_var_declare(net_var);
	}
	else
	{
		if ((net_var->type == NET_VAR_SEND) || (net_var->type == NET_VAR_RECV)) EXIT_ON_ERROR("You use twice the same variable in one script");
		net_var->user_data = user_data;
		net_var->recv_callback = recv_callback;
		net_var_connect(net_var);
	}
	kprints("%s recv '%p'\n", name, recv_callback);
	sem_post(&enet_network_semaphore);

	return net_var;
}


type_net_var * net_var_recv_manual(const char *name, const char *ip, int port, type_recv_callback recv_callback, void *user_data){

	type_net_var *net_var = NULL;
	char prom_name[30];
	sprintf(prom_name, "%s:%i:manual", ip,port);

	sem_wait(&manual_semaphore);

	net_var = net_var_find(name);

	if(net_var == NULL){
		net_var = net_var_new(name,NET_VAR_RECV, NET_VAR_MANUAL);
		net_var->user_data = user_data;
		net_var->recv_callback = recv_callback;
		net_var->connection = connection_get(prom_name,ip,port);
		net_var_connect(net_var);
	}else{
		EXIT_ON_ERROR("Manual connection attempt failed for variable \"%s\" : variable already exists.",name);
	}

	sem_post(&manual_semaphore);

	return net_var;
}


/**
 * function called by an ivy callback telling that a distant script is waiting a variable
 * */
void net_var_waiting(const char *name, const char *prom_name, const char *ip, int port)
{
	type_net_var *net_var;

	sem_wait(&enet_network_semaphore);

	/* We check if we can send the variable */
	net_var = net_var_find(name);

	/* No, we note it is waiting */
	if (net_var == NULL ) net_var_distant_new(name, NET_VAR_WAITING, NET_VAR_AUTO, prom_name, ip, port);
	else
	{
		if (net_var->connected)
		{
			PRINT_WARNING("Variable '%s' is already connected !", net_var->name);
		}
		else
		{
			net_var->connection = connection_get(prom_name, ip, port);
			net_var_connect(net_var);
		}
	}
	sem_post(&enet_network_semaphore);

}

/**
 * function called by an ivy callback telling that a distant script can send a variable
 * */
void net_var_available(const char *name, const char *prom_name, const char* ip, int port)
{

	type_net_var *net_var;
	sem_wait(&enet_network_semaphore);

	/* We check if we can recv the variable */
	net_var = net_var_find(name);

	/* No, but we note it is available */
	if (net_var == NULL )
	{

		net_var = net_var_distant_new(name, NET_VAR_AVAILABLE, NET_VAR_AUTO, prom_name, ip, port);
		kprints("available new netvar %s type %d, net_var pointer %p\n", net_var->name, net_var->type, net_var->recv_callback);

	}

	else
	{
		if (net_var->connected)
		{
			PRINT_WARNING("Variable '%s' is already connected !", net_var->name);
		}
		else
		{
			kprints("available netvar %s type %d, net_var pointer %p\n", net_var->name, net_var->type, net_var->recv_callback);

			net_var->connection = connection_get(prom_name, ip, port);
			net_var_connect(net_var);
		}
	}
	sem_post(&enet_network_semaphore);

}

/**
 * Send data for the variable
 */
void net_var_send_data(type_net_var *net_var, const char *buffer, size_t size)
{
	if(net_var->connected)
		connection_send_buffer(net_var->connection, net_var->channel, buffer, size);
}
