/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/* Fichier gestion_diagnostic.c                                                                       */
/* P. Gaussier Aout 2005                                                                              */

/* Ce fichier contient des fonctions simulant de maniere simplifiee l'execution d'un script.          */
/* Elles permettent de faire ensuite un diagnostic sur les causes du mauvais fonctionnement du reseau.*/
/* On pourrait aussi envisager d'ajouter a chaque groupe un drapeau permettant de savoir s'il a ete   */
/* execute au moins une fois. Mais on a besoin de faire reellement tourner le reseau completement.    */
/* Outil d'analyse hors ligne vs debug en ligne */

#include <stdlib.h>

#include "public.h"
#include "rttoken.h"            /* A.Patard */

/*#define USE_THREADS 1 */

/* #define DEBUG 1 */
/* verbose ne concerne que les messages affiches pendant le mode pas_a_pas.
 On peut donc laisser active cette option sans grand soucis d'affichage.
 Il suffit de ne pas se mettre en pas a pas. */
#define VERBOSE 1

/* fonctions de gestion_sequ.c */
extern int directement_activable(int gpe, int mvt);
extern void initialise_jetons(int ech);

/* fonction utilisee en mode diagnostic pour verifier si le reseau peut tourner sans lien secondaire. */
/* Cette fonction permet d'identifier les points delicats d'un reseau. */

int hard_directement_activable(int gpe, int mvt)
{
	int gpe_entree, i, j;

	if (def_groupe[gpe].deja_active == 1) return 0;

	if (def_groupe[gpe].type2 != 0 && mvt == 0) return (0);

	j = 0;
	i = find_input_link(gpe, j);
	while (i != -1) /* tant que le groupe a encore un lien en entree a verifier */
	{
		gpe_entree = liaison[i].depart;
		if (def_groupe[gpe_entree].deja_active == 0) return (0);
		j = j + 1;
		i = find_input_link(gpe, j);
	}
	return 1;
}

/*----------------------------------------------------------------------------------------------------*/
/* Ici, on ne detaille pas les plans de mise a jour reellement parallele dans le detail.              */
/* Dans la vrai mise a jour le flag deja_active n'est mis a 1                                         */
/* qu'une fois tous les groupes paralleles actives.                                                   */

/* use_secondary_link=0 est utilise pour tester si les liens secondaires sont vraiment necessaires.   */


int simule_mise_a_jour_fonction_parallele(int gestion_STM, int ech, int mvt, int print_detail, int use_secondary_link)
{
	int i;
	int flag;
	/*int nbre_parallele; */
	int nb_rttoken_boxes;
	int activable;
	(void)gestion_STM;

	flag = 1;
	while (flag != 0)
	{ /* tant qu'un groupe peut etre active */
		flag = 0; /* plus de groupe a activer a cette echelle ? */
		/*nbre_parallele = 0; nbre de groupes a lancer en parallele   */
		nb_rttoken_boxes = 0;
		for (i = 0; i < nbre_groupe; i++)
		{
			if (def_groupe[i].ech_temps == ech)
			{
				if (use_secondary_link == 0) activable = hard_directement_activable(i, mvt);
				else activable = directement_activable(i, mvt);
				if (activable == 1)
				{
					if (print_detail == 1) printf(" gpe no_name %s (num %d) activable \n", def_groupe[i].no_name, i);
					if (def_groupe[i].rttoken == 1) nb_rttoken_boxes++;
					def_groupe[i].deja_active = 1; /* on simule son activation */
					flag = 1; /* racourci par rapport a l'execution parallele normale */
				}
			}
		}
	}
	return 0;
}

/* on tient pas compte des breaks ... */
/* ni des iterations par ech de temps */

void diagnostic_detaille(int ech, int use_secondary_link)
{
	int k;
	int gestion_STM;
	//int nn_break;

	if (ech > 0) gestion_STM = 1;
	else gestion_STM = 0;

	flag_temps_dynamique = 0;

	printf("ech=%d , echelle_temps_debug= %d \n", ech, echelle_temps_debug);
	printf("simule les %d iterations de mise a jour faites strcitement a cette echelle\n", echelle_temps[ech]);
	printf("initialisation des jetons pour l'echelle %d \n", ech);
	initialise_jetons(ech);
	simule_mise_a_jour_fonction_parallele(gestion_STM, ech, 0
	/* pour groupe sans dynamique */
	, 1, use_secondary_link);

	if (ech > 0)
	{
		diagnostic_detaille(ech - 1, use_secondary_link);
		/* remontee de l'information issus des echelles temporelles */
		/*                   de plus bas niveau                     */
		simule_mise_a_jour_fonction_parallele(gestion_STM, ech, 0, 1, use_secondary_link);
	}
	if (ech <= echelle_temps_learn)
	{
		printf("apprentissage des groupes de l'ech %d \n", ech);
	}

	if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1)  && echelle_temps_dynamique[ech] == 1)
	{ /* pour Arnaud */
		flag_temps_dynamique = 1;
		initialise_jetons(ech);
		for (k = ech; k >= 0; k--)
			simule_mise_a_jour_fonction_parallele(gestion_STM, k, 1, 1, use_secondary_link);
		for (k = 0; k <= ech; k++)
			simule_mise_a_jour_fonction_parallele(gestion_STM, k, 1, 1, use_secondary_link);
	}

	/************** gestion des mouvements t+GrandDT  *************************/

	else if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 2)
	{ /* pour Cedric */
		/*initialise_jetons_juste_echelle(ech);   */
		simule_mise_a_jour_fonction_parallele(gestion_STM, ech, 1, 1, use_secondary_link); /* pour groupe avec dyn. mvt */
		if (ech <= echelle_temps_learn) printf("apprentissage des groupes de l'ech %d \n", ech);
	}
	printf("Fin diagnostic ech %d \n", ech);
}

void diagnostic_short(int ech, int use_secondary_link)
{
	int k;
	int gestion_STM;
	int detail = 1;

	if (detail == 1) printf("diagnostic short ech = %d \n", ech);
	if (ech > 0) gestion_STM = 1;
	else gestion_STM = 0;

	flag_temps_dynamique = 0;
	simule_mise_a_jour_fonction_parallele(gestion_STM, ech, 0
	/*pour groupe sans dynamique */
	, detail, use_secondary_link);

	if (ech > 0)
	{
		diagnostic_short(ech - 1, use_secondary_link);
		/* remontee de l'information issus des echelles temporelles */
		/*                   de plus bas niveau                     */
		simule_mise_a_jour_fonction_parallele(gestion_STM, ech, 0, detail, use_secondary_link);
	}

	if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 1)
	{ /* pour Arnaud */
		flag_temps_dynamique = 1;
		for (k = ech; k >= 0; k--)
			 simule_mise_a_jour_fonction_parallele(gestion_STM, k, 1, detail, use_secondary_link); /* nn_break ???? */
		for (k = 0; k <= ech; k++)
			simule_mise_a_jour_fonction_parallele(gestion_STM, k, 1, detail, use_secondary_link);
	}

	/************** gestion des mouvements t+GrandDT  *************************/

	else if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 2)
	{ /* pour Cedric */
		/*initialise_jetons_juste_echelle(ech);   */
		simule_mise_a_jour_fonction_parallele(gestion_STM, ech, 1, detail, use_secondary_link); /*pour gpe avec dyn mvt */
	}
}

void diagnostic(int use_secondary_link)
{
	printf("***** debut diag %d , max_ech_temps=%d\n", use_secondary_link, max_ech_temps);
	initialise_jetons(max_ech_temps); /*sortie de la mise a jour pour eviter les resets */
	diagnostic_short(max_ech_temps, use_secondary_link);
}
