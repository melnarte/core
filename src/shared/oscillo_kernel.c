/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 *      oscillo_kernel.c
 *
 *      Copyright 2010 philippe gaussier <pgaussier@wanadoo.fr>
 *
 */

#include <net_message_debug_dist.h>
#include "public.h"
/*#include <rttoken.h>*/
#include <sys/time.h>
#include <unistd.h>
#include <glib.h>
#include <oscillo_kernel.h>

#include <prom_tools/include/prom_bus.h>

/*----------------------------------------------------------------------------------------*/

#ifndef AVEUGLE
#ifdef OSCILLO_KERNEL

#define PHASE_OF_RECEIVE_TOKEN 0
#define PHASE_OF_ACTIVATION 1
#define PHASE_OF_EXECUTION 2
#define PHASE_OF_END_ALGO 3
#define PHASE_OF_END_RTTOKEN_AFTER_SEM_POST 4
#define PHASE_OF_END_RTTOKEN_PROPAGATION 5

/* dimension de la zone de dessin */
#define DA_WIDTH 2000
#define DA_HEIGHT 20


TxDonneesFenetre fenetre_oscillo_kernel;
oscillo_profiler *oscillo_prof;

GtkWidget *entry_periode_reset_oscillo_kernel;

int oscillo_kernel_activated = 0;
int oscillo_dist_activated = 0;
int demande_reset_oscillo_kernel = 1;

long temps_old;
long last_reset_oscillo_kernel=0; /* variable globale pour la synchro de l'affichage / pas plus d'un reset par periode globale de l'oscillo kernel */
int hauteur_display_gpe=23;
int x_offset_display_gpe=20;
int y_offset_display_gpe=20;
long periode_reset_oscillo_kernel= 1000000;  /* 1 secondes */
long pas_temps_oscillo_kernel;
int longueur_affichage_oscillo_kernel=2000;
int recent_reset_affichage=0; /*pour gestion par oscillo_distant multi RNs: reaffichage de tous les RNs */

struct timeval InitTimeTrace={-1,-1};

/* initialise la variable globale servant de temps init pour la mesure du temps */
void init_oscillo_kernel_timing()
{
	if(InitTimeTrace.tv_usec==-1 && InitTimeTrace.tv_sec==-1)
    {
      gettimeofday(&InitTimeTrace, (void *) NULL);
    }
}

void reset_affichage_oscillo_kernel()
{
 /* demande_reset_oscillo_kernel=1;*/
}

void group_profiler_refresh(type_group_profiler *group_profiler)
{
	snprintf(group_profiler->text_of_period, SIZE_OF_DISPLAY_INFO, "%f", group_profiler->period);
	gtk_label_set_text(GTK_LABEL(group_profiler->widget_of_period), group_profiler->text_of_period);
}

void profiler_refresh(type_profiler *profiler)
{
	int i;
	type_group_profiler *group_profiler;

	for (i=0; i<profiler->number_of_groups; i++)
	{
		group_profiler = profiler->groups[i].data_of_profiling;
		group_profiler_refresh(group_profiler);
	}
}

gboolean expose_event_callback (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
  cairo_t *cr;
	(void)data;
	(void)widget;

  cr = gdk_cairo_create (event->window);
  cairo_set_source_rgb (cr, 0.0, 0.0, 1.0);
  cairo_paint (cr);
  cairo_destroy (cr);
  return TRUE;
}

void reset_affichage(type_profiler *profiler)
{
  cairo_t *cr;
  int i, j;

  TxPoint point;
  type_group_profiler *group_profiler;


  pas_temps_oscillo_kernel = periode_reset_oscillo_kernel / longueur_affichage_oscillo_kernel; /* 2000  pixels au total pour l'affichage */
  if(pas_temps_oscillo_kernel<1)  pas_temps_oscillo_kernel=1;
  point.x= 0; point.y= 0;

  recent_reset_affichage = 1; /* la raz est effectuee par l'oscillo distant */

  gdk_threads_enter();

  if(oscillo_kernel_activated==1)
  {
	for(i=0; i<profiler->number_of_groups; i++)
	{
	  group_profiler = profiler->groups[i].data_of_profiling;
	  group_profiler->size_of_sample = 0; /* reset of the average of the period */

	  group_profiler_refresh(group_profiler);


	  cr = gdk_cairo_create(group_profiler->drawing_area->window);
	  cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
	  cairo_paint (cr);
	  cairo_destroy(cr);

	  gdk_window_process_updates(group_profiler->drawing_area->window, TRUE);
	}
  }

  if(oscillo_dist_activated==1)
  {
	for (j=0; j<nb_max_channels; j++)
	{
	  if(NN[j].promethe_connecter_name!=NULL)
		{
		  for(i=0; i<NN[j].nbre_groupe; i++)
		  {
/*			group_profiler = NN[j].def_groupe[i].data_of_profiling;
			TxDessinerRectangle(group_profiler->fenetre_group, noir, TxPlein, point, longueur_affichage_oscillo_kernel, group_profiler->fenetre_group->da->allocation.height, 1);
			TxDisplay(group_profiler->fenetre_group);*/
			}
		  }
		}
	}
  /*TxDisplay(image);*/

	gdk_threads_leave();
	demande_reset_oscillo_kernel = 0;
}

/* les phases de la mise a jour du rttoken : */
/* 0 (blanc) : rttoken active */
/* 1 (bleu/jaune) : fin du reset propagation rtttoken */
/* 2 (rouge) : fin de la mise a jour du rttoken apres le sem_post(&attend_un_rt_token) */
/* 3 idem 1 mais pour RN */
/* 4 inconnu */


/**
 * Offset_y n'est pas utilise.
 * temps_old n'est pas utilise !!
 */
void display_oscillo_kernel(type_group_profiler *group_profiler, long *last_pos_oscillo_kernel, int gpe, int phase, glong temps, glong temps_old, int offset_y)
{
  cairo_t *cr;
  GdkColor *color;
  glong temps2;
  int longueur;
  int phase2;
  TxPoint point ,point2;
  type_profiler *profiler =  group_profiler->profiler;

  (void)offset_y;
  (void) temps_old;

  if(oscillo_kernel_activated == 0) return; /* Should not be called at all */


  /*if(demande_reset_oscillo_kernel==1)
  {
	demande_reset_oscillo_kernel = 0;
	local_time = long_us_time();*/

	if((temps - last_reset_oscillo_kernel) >= periode_reset_oscillo_kernel)
	{
	  reset_affichage(profiler);
	  last_reset_oscillo_kernel = temps;
	}
  /*}*/

/*
  if((temps - group_profiler->time_of_last_received_token) > periode_reset_oscillo_kernel)*/ /* 10 secondes de modulo */
  /*{
    group_profiler->time_of_last_received_token = temps;
    demande_reset_oscillo_kernel = 1;
  }*/


  temps2 = (temps -  last_reset_oscillo_kernel) / pas_temps_oscillo_kernel;

  point2.x=(int)temps2;

  phase2 = phase;

  if(phase==PHASE_OF_END_ALGO) phase2 = PHASE_OF_ACTIVATION;

  point2.y=phase2*5;
  point.y=point2.y;

  switch (phase)
  {
    case PHASE_OF_RECEIVE_TOKEN: color = &couleurs[blanc];  break;  /*  token ou rttoken active */
    case PHASE_OF_ACTIVATION : color = &couleurs[ciel]; break;   /*  fin du reset propagation rtttoken */
    case PHASE_OF_END_ALGO : color = &couleurs[vert]; break;   /*  idem 1 mais pour fin RN */
    case PHASE_OF_END_RTTOKEN_AFTER_SEM_POST : color = &couleurs[rouge]; break;
    case PHASE_OF_END_RTTOKEN_PROPAGATION : color = &couleurs[jaune]; break;
    default : color = &couleurs[violet]; /* couleur = violet; */
  }

  gdk_threads_enter();
  cr = gdk_cairo_create(group_profiler->drawing_area->window);
  gdk_cairo_set_source_color(cr, color);

  cairo_rectangle (cr, point2.x, point2.y, 1, 6);
  cairo_fill(cr);

  if(phase == PHASE_OF_RECEIVE_TOKEN) last_pos_oscillo_kernel[gpe]=point2.x;
  else
  if(phase == PHASE_OF_ACTIVATION || phase== PHASE_OF_END_ALGO)
  {
    longueur = point2.x - last_pos_oscillo_kernel[gpe];

    if(last_pos_oscillo_kernel[gpe]==-1 || longueur<1)
	{
      longueur = 1;
	  point.x = 0;
	}
    else
	{
	  point.x = last_pos_oscillo_kernel[gpe];
	}

    if(profiler->groups[gpe].type==No_RTTOKEN) gdk_cairo_set_source_color (cr, &couleurs[jaune]);

    if(longueur>0)
    {
      cairo_rectangle (cr, point.x, point.y, longueur, 3);
      cairo_fill(cr);
	}
  }

  gdk_window_process_updates (group_profiler->drawing_area->window, FALSE);
  cairo_destroy(cr);
  gdk_threads_leave();
}


void group_profiler_update_info(type_group_profiler *group_profiler, long *last_pos_oscillo_kernel, int gpe, int phase, long temps)
{
	float eta;
	long old, group_period;

	old = 1;

	if(oscillo_kernel_activated == 0 && oscillo_dist_activated ==0) return;

	if(group_profiler != NULL)
	{
		if(phase == PHASE_OF_RECEIVE_TOKEN )
		{
			if(group_profiler->time_of_last_received_token != -1 && (oscillo_kernel_activated || oscillo_dist_activated))
			{
				group_profiler->size_of_sample = group_profiler->size_of_sample + 1;

				eta = 1 / group_profiler->size_of_sample;
				group_period = temps - group_profiler->time_of_last_received_token;
				group_profiler->period = group_profiler->period + eta * (group_period/1000.0 - group_profiler->period);
			}
			group_profiler->time_of_last_received_token = temps;
		}

		display_oscillo_kernel(group_profiler, last_pos_oscillo_kernel, gpe, phase, temps, old, 0); /*int offset_y*/
	}
}

void add_legend(GtkWidget *box_of_legend, const char *content, int index_of_color)
{
	GtkWidget *label_of_legend;

	label_of_legend = gtk_label_new(content);
	gtk_widget_set_size_request(label_of_legend, 250, -1);
	gtk_widget_modify_fg(label_of_legend, GTK_STATE_NORMAL, &couleurs[index_of_color]);
	gtk_box_pack_start(GTK_BOX(box_of_legend), label_of_legend, FALSE, TRUE, 0);
}

void destroy_profiler(GtkWidget *window, GdkEvent *event, type_profiler *profiler)
{
	int i;
	(void)event;
  (void)window;


	oscillo_kernel_activated = 0;

	for (i=0; i<profiler->number_of_groups; i++)
	{
		free(profiler->groups[i].data_of_profiling);
		profiler->groups[i].data_of_profiling = NULL;
	}
	free(profiler);
	gtk_widget_destroy(oscillo_prof->profiler_window);
	oscillo_prof->profiler_window = NULL;
}

/* gere l activation ou non du boutton a cocher */
void toggle_check(GtkWidget *check_button, type_group_profiler *group_profiler)
{
	gboolean etat;
	etat = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(check_button));

  if(etat)
  {
		gtk_widget_show(group_profiler->widget);
  }
  else
  {
		gtk_widget_hide(group_profiler->widget);
  }
}

void destroy_fenetre_oscillo_kernel()
{

  gtk_widget_destroy(oscillo_prof->profiler_window);
  free(oscillo_prof); /* Merite d'autres effacements  !!!*/
  oscillo_prof = NULL;
}


void arrete_oscillo_kernel(GtkWidget * widget)
{

	(void)widget;
  if(oscillo_kernel_activated==1) /* arret de l'oscillo dans le cas du promethe normal */
	{
		oscillo_kernel_activated = 0;
		gtk_widget_destroy(oscillo_prof->profiler_window);
		oscillo_prof->profiler_window = NULL;
	}

	if(oscillo_dist_activated==1) /* arret de l'oscillo dans le cas du promethe distant */
	{
	/* 	oscillo_dist_off();
		printf("arret de l oscillo distant \n");
		*/
	}

	/*for (i=0; i<profiler->number_of_groups; i++)
	{
		free(profiler->groups[i].data_of_profiling);
		profiler->groups[i].data_of_profiling = NULL;
	}
	free(profiler);*/

	/*return;*/
}


void change_periode_oscillo_kernel(GtkWidget * widget, GtkWidget * entry)
{
  const gchar *entry_text;
  (void)widget;

  entry_text = gtk_entry_get_text(GTK_ENTRY(entry));

  periode_reset_oscillo_kernel=atol(entry_text)*1000;
  dprints("Nouvelle periode oscillo = %ld \n", periode_reset_oscillo_kernel/1000);
  gtk_range_set_value(GTK_RANGE(oscillo_prof->hscale), (double) periode_reset_oscillo_kernel/10000);
  reset_affichage_oscillo_kernel();
}


void periode_oscillo_kernel_get_value(GtkWidget * widget, int *val)
{
  char valeur_periode[255];
  *val = gtk_range_get_value( GTK_RANGE(widget));
  periode_reset_oscillo_kernel=(*val)*1000;

  sprintf(valeur_periode,"%d",(int)*val/1000);
  gtk_entry_set_text(GTK_ENTRY(entry_periode_reset_oscillo_kernel), valeur_periode);
  reset_affichage_oscillo_kernel();
}

/* Button oscillo kernel */
void oscillo_kernel_pressed()
{
	if(oscillo_kernel_activated==0)
	{
	  oscillo_kernel_activated=1;
	}
	else
	{
	  oscillo_kernel_activated=0;
	}

	/*
	if(oscillo_dist_activated==1)
	{
		oscillo_dist_activated = 0;
		prom_bus_send_message("oscillo_off\n");
	}
	else if(oscillo_dist_activated==0)
	{
		oscillo_dist_activated = 1;
		prom_bus_send_message("oscillo_on\n");
	}*/
}

void create_window_of_profiler(const char *title)
{

	if (oscillo_prof == NULL) oscillo_prof = ALLOCATION(oscillo_profiler);

	oscillo_prof->profiler_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(oscillo_prof->profiler_window, -1 , 400);
	gtk_window_set_title(GTK_WINDOW(oscillo_prof->profiler_window), title);

	oscillo_prof->box_of_legend = gtk_hbox_new(FALSE, 1);
	add_legend(oscillo_prof->box_of_legend, "start activation", blanc);
	add_legend(oscillo_prof->box_of_legend, "execution", ciel);
	add_legend(oscillo_prof->box_of_legend, "end algo group", vert);
	add_legend(oscillo_prof->box_of_legend, "end update rt-token after sem_post", rouge);
	add_legend(oscillo_prof->box_of_legend, "end reset rt-token propagation", jaune);

	oscillo_prof->vbox = gtk_vbox_new(FALSE, 1);
	gtk_box_pack_start(GTK_BOX(oscillo_prof->vbox), oscillo_prof->box_of_legend, FALSE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(oscillo_prof->profiler_window), oscillo_prof->vbox);

	oscillo_prof->sw = gtk_scrolled_window_new(NULL, NULL);

	oscillo_prof->vbox2 = gtk_vbox_new(FALSE, 1);

	gtk_box_pack_start(GTK_BOX(oscillo_prof->vbox), oscillo_prof->sw, TRUE, TRUE, 0);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(oscillo_prof->sw), oscillo_prof->vbox2);

	/* ***** */
	oscillo_prof->box3 = gtk_hbox_new(FALSE, 1);
	gtk_container_set_border_width(GTK_CONTAINER(oscillo_prof->box3), 10);
	gtk_box_pack_start(GTK_BOX(oscillo_prof->vbox), oscillo_prof->box3, FALSE, TRUE, 0);

	oscillo_prof->button = gtk_button_new_with_label("Oscillo Kernel");
	g_signal_connect(GTK_OBJECT(oscillo_prof->button), "clicked", G_CALLBACK(oscillo_kernel_pressed), oscillo_prof->profiler_window);

	gtk_box_pack_start(GTK_BOX(oscillo_prof->box3), oscillo_prof->button, TRUE, TRUE, 0);
	GTK_WIDGET_SET_FLAGS(oscillo_prof->button, GTK_CAN_DEFAULT);
	gtk_widget_grab_default(oscillo_prof->button);

	oscillo_prof->box_entree = gtk_vbox_new(FALSE, 1);
	gtk_container_add(GTK_CONTAINER(oscillo_prof->box3), oscillo_prof->box_entree);
	gtk_widget_set_size_request(oscillo_prof->box_entree, 100, 20);

	entry_periode_reset_oscillo_kernel = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(entry_periode_reset_oscillo_kernel), 30);
	g_signal_connect(G_OBJECT(entry_periode_reset_oscillo_kernel), "activate", G_CALLBACK(change_periode_oscillo_kernel), (gpointer) entry_periode_reset_oscillo_kernel);

	sprintf(oscillo_prof->valeur_periode,"%ld", periode_reset_oscillo_kernel/1000);
	gtk_entry_set_text(GTK_ENTRY(entry_periode_reset_oscillo_kernel), oscillo_prof->valeur_periode);

	gtk_editable_select_region(GTK_EDITABLE(entry_periode_reset_oscillo_kernel), 0, GTK_ENTRY(entry_periode_reset_oscillo_kernel)->text_length);
	gtk_box_pack_start(GTK_BOX(oscillo_prof->box_entree), entry_periode_reset_oscillo_kernel, TRUE, TRUE, 0);


	oscillo_prof->box_ascenseur = gtk_vbox_new(FALSE, 1);
	oscillo_prof->label = gtk_label_new("time cst");
	gtk_container_add(GTK_CONTAINER(oscillo_prof->box3), oscillo_prof->box_ascenseur);
	gtk_widget_set_size_request(oscillo_prof->box_ascenseur, 300, 55);

	oscillo_prof->box4 = gtk_vbox_new(FALSE, 3);
	gtk_container_set_border_width(GTK_CONTAINER(oscillo_prof->box4), 10);
	gtk_box_pack_start(GTK_BOX(oscillo_prof->box_ascenseur), oscillo_prof->box4, TRUE, TRUE, 0);

	oscillo_prof->hscale = gtk_hscale_new_with_range (1, 1000, 10);
	gtk_range_set_value(GTK_RANGE(oscillo_prof->hscale), periode_reset_oscillo_kernel/1000)  ;

	gtk_widget_set_usize(GTK_WIDGET(oscillo_prof->hscale), 200, 30);
	g_signal_connect(GTK_OBJECT(oscillo_prof->hscale), "value_changed", G_CALLBACK(periode_oscillo_kernel_get_value), &periode_reset_oscillo_kernel);

	gtk_box_pack_start(GTK_BOX(oscillo_prof->box4), oscillo_prof->hscale, TRUE, TRUE, 0);

	g_signal_connect(GTK_OBJECT(oscillo_prof->profiler_window), "destroy", G_CALLBACK(arrete_oscillo_kernel), NULL);


}

/* Passer en parametre le bouton permettant de la fermer et se connecter dessus. Ne plus avoir d'affiche oscillo */
void create_fenetre_oscillo_kernel()
{
	create_window_of_profiler(fenetre_oscillo_kernel.name);
	gtk_widget_show_all(oscillo_prof->profiler_window);
}


void add_promethe_oscillo(type_groupe *d_groupe, int number_of_groups, const char *name_promethe)
{
	GtkWidget *box_of_group, *label_of_group;
  GtkWidget *box_of_group_profilers;
  GtkWidget *check_button, *check_button_promethe;

  type_profiler *profiler;
	type_group_profiler *group_profiler;

	int i;

	check_button_promethe = gtk_toggle_button_new_with_label(name_promethe);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_button_promethe), TRUE);
	gtk_widget_set_size_request(check_button_promethe, -1, -1);
	gtk_box_pack_start(GTK_BOX(oscillo_prof->vbox2), check_button_promethe, FALSE, FALSE, 0);

    box_of_group_profilers = gtk_vbox_new(FALSE, 1);

	profiler = ALLOCATION(type_profiler);
	profiler->time_offset = 0;
	profiler->period = periode_reset_oscillo_kernel;  /* 1s */
	profiler->groups = d_groupe;
	profiler->number_of_groups = number_of_groups;

	for (i=0; i<number_of_groups;i++)
	{
		group_profiler = ALLOCATION(type_group_profiler);
		group_profiler->profiler = profiler;
		group_profiler->time_of_last_received_token = -1;
		group_profiler->number_of_historical_events = 0;
		check_button = gtk_check_button_new_with_label(d_groupe[i].nom);

        box_of_group = gtk_hbox_new(FALSE, 1);

		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_button), TRUE);

		gtk_widget_set_size_request(check_button, 75, -1);
		gtk_box_pack_start(GTK_BOX(box_of_group), check_button, FALSE, TRUE, 0);

		label_of_group = gtk_label_new(d_groupe[i].no_name);
		gtk_widget_set_size_request(label_of_group, 50, -1);
		gtk_box_pack_start(GTK_BOX(box_of_group), label_of_group, FALSE, FALSE, 0);
		group_profiler->widget = box_of_group;

        group_profiler->drawing_area = gtk_drawing_area_new();

        gtk_widget_set_size_request(group_profiler->drawing_area, DA_WIDTH, DA_HEIGHT);
        gtk_box_pack_start(GTK_BOX(box_of_group), group_profiler->drawing_area, TRUE, TRUE, 0);

		group_profiler->widget_of_period = gtk_label_new("period");
		gtk_widget_set_size_request(group_profiler->widget_of_period, 100, -1);
		gtk_box_pack_start(GTK_BOX(box_of_group), group_profiler->widget_of_period, FALSE, FALSE, 0);

		gtk_box_pack_start(GTK_BOX(box_of_group_profilers), box_of_group, FALSE, FALSE, 0);

		d_groupe[i].data_of_profiling = group_profiler;

		/* ecoute si on cache ou non l un des groupes */
		g_signal_connect(check_button, "toggled", G_CALLBACK(toggle_check), group_profiler);
	}

	gtk_box_pack_start(GTK_BOX(oscillo_prof->vbox2), box_of_group_profilers, FALSE, FALSE, 0);
	g_signal_connect(check_button_promethe, "toggled", G_CALLBACK(toggle_check), box_of_group_profilers);
	gtk_widget_show_all(oscillo_prof->vbox2);
}

void affiche_oscillo_kernel(GtkWidget * widget)
{
  (void)widget;

	if(oscillo_prof == NULL)
	{
	  oscillo_prof = ALLOCATION(oscillo_profiler);
	  oscillo_prof->profiler_window = NULL;

		create_fenetre_oscillo_kernel();
		add_promethe_oscillo(def_groupe, nbre_groupe, "Oscillo Kernel");
		oscillo_kernel_activated = 1;
		demande_reset_oscillo_kernel = 1;
	}

	/*
	else if(oscillo_kernel_activated == 1)
	{
		oscillo_kernel_activated = 1 - oscillo_kernel_activated;
		gtk_widget_hide_all(oscillo_prof->profiler_window);
	}
	else if(oscillo_kernel_activated == 0)
	{
	 oscillo_kernel_activated = 1 - oscillo_kernel_activated;
	 gtk_widget_show_all(oscillo_prof->profiler_window);
	}*/
}

void affiche_oscillo_distant()
{
	/*if(first) {*/
	/*  init_oscillo_window(); */

	if(oscillo_prof->profiler_window == NULL)
	{
		create_fenetre_oscillo_kernel();
		oscillo_dist_activated = 1;
		demande_reset_oscillo_kernel = 1;
	}
	else if(oscillo_dist_activated == 0)
	{
		/*oscillo_dist_activated = 0;*/
		gtk_widget_hide_all(oscillo_prof->profiler_window);
	}
	else if(oscillo_dist_activated == 1)
	{
		/*oscillo_dist_activated = 1;*/
		gtk_widget_show_all(oscillo_prof->profiler_window);
	}
}

void show_oscillo_kernel()
{
	affiche_oscillo_kernel(NULL);
}

/* pour dire au promethe que l'on a active l'oscillo distant et qu'il doit envoyer les donnees de son l'oscillo a celui ci */
void oscillo_dist_status()
{
	if(oscillo_dist_activated==0)
	{
		dprints("active envoie donnees dist_prom\n");
		oscillo_dist_activated = 1;
	}
	else if(oscillo_dist_activated==1)
	{
		oscillo_dist_activated = 0;
		dprints("desactive envoie donnees dist_prom\n");
	}
}
#endif /* oscillo kernel */
#endif /* aveugle */
