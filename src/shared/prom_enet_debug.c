/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 *      prom_enet_debug.c
 *
 *      philippe gaussier 2009/2010
 *
 */

#define DEBUG 1

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <enet/enet.h>

#include <sys/timeb.h>
#include <signal.h>
#include <math.h>
#include "public.h"
#include "net_message_debug_dist.h"
#include "prom_enet_debug.h"


long secsFirstCall = 0;

size_t packetSize = 1000; /*256;  size of a single packet to be sent */

int verbose_debug = 0;
int channel_number = 2; /*PG: provisoire */

type_com_groupe com_def_groupe[nb_max_groupes];
type_connexion_udp network_debug_link;
type_trame_init trame_def_groupe;

inline float convert_time(struct timeval my_time)
{
	float f_time;
	f_time = (float) (my_time.tv_sec - secsFirstCall) + (float) my_time.tv_usec / (1000.0 * 1000.0);
	return (f_time);
}

int no_message = 0;

/*************************************************************/
/* Attention: peut etre appeler depuis une section critique pour init */

/* ou poour l'envoie des tags de l'oscillo kernel                     */
void send_debug_data(type_connexion_udp *connexion, char *data, int packetSize, int channel_number)
{
	if (!connexion->peer)
	{
		dprints("pb 1...%s (peer==NULL)\n", __FUNCTION__);
		return;
	}
	/*
	if (!connexion->peer->data)
	{
		dprints("pb 2...(perr->data==NULL)%s\n", __FUNCTION__);
		return;
	} *//* client cannot send some data: */

	/*  printf("client send data %d\n",((type_nn_message *)data)->no_message); */
	/*++connexion->sendCounter;*/

	/* plante si pas de create ! */
	connexion->packet = enet_packet_create(data, packetSize, (ENET_PACKET_FLAG_RELIABLE & ENET_PACKET_FLAG_NO_ALLOCATE ));
	enet_peer_send(connexion->peer, 0, connexion->packet); /* queue the packet to be sent to the peer over channel id 0 */
	enet_host_flush(connexion->host); /* not necessary, included in enet_host_service and dangerous --> lost packets / to fast ! */
}

/* Dialogue:
 * 1) dist_prom detecte la connexion de promethe
 * 2) dist_prom s'identifie par connect_distant_oscillo
 * 3) promethe repond en donnant son def_groupe
 *
 * 4) promethe envoie son debug oscillo_kernel

 *************************************************************/

void init_com_def_groupe(type_com_groupe com_def_groupe[], type_groupe def_groupe[], int nbre_groupe)
{
	int i;
	dprints("init_com_def_groupe %s\n", __FILE__);
	for (i = 0; i < nbre_groupe; i++)
	{
		memcpy(&(com_def_groupe[i]), &(def_groupe[i]), sizeof(type_com_groupe));
	}
}

void send_def_groupe_data(type_connexion_udp *connexion, int channel_number)
{

	dprints("Envoie du 1er message %s channel_number=%d \n", __FUNCTION__, channel_number);
	dprints("%s taille int = %ld, ptr=%ld\n", __FUNCTION__, sizeof(int), sizeof(void*));
	trame_def_groupe.type_trame = INIT_DEFGROUPE;
	dprints("taille un groupe = %ld \n", sizeof(def_groupe[0]));
	trame_def_groupe.size = sizeof(com_def_groupe[0]) * nbre_groupe;
	trame_def_groupe.nbre_groupe = nbre_groupe;
	dprints("%s type_trame = %d , size = %ld , nbre_groupe = %d \n", __FILE__, INIT_DEFGROUPE, trame_def_groupe.size, nbre_groupe);
	memcpy(trame_def_groupe.com_def_groupe, com_def_groupe, trame_def_groupe.size);
	send_debug_data(connexion, (char*) &trame_def_groupe, sizeof(trame_def_groupe), channel_number);

	dprints("FIN %s \n", __FUNCTION__);
}

int phase_reception_data = 0;

void reception_debug_data(type_connexion_udp *connexion, int channel_number)
{
	char message[] = "connect_distant_oscillo";
	int buf_len;

	printf("donnees recues... \n");
	printf("  A packet of length %lu containing [%s] was received from %s on channel %u\n", (connexion->event.packet)->dataLength, connexion->event.packet->data, (char*) connexion->event.peer->data, connexion->event.channelID);

	buf_len = strlen(message);
	printf("1\n");
	if (phase_reception_data == 0 && strncmp(message, (char *) connexion->event.packet->data, buf_len) == 0)
	{
		printf("2\n");
		phase_reception_data = 1;

		send_def_groupe_data(connexion, channel_number);
		/*
		 printf("OK c'est le bon message, on envoie def_groupe \n");
		 packetSize=nbre_groupe*sizeof(def_groupe[0]);
		 printf("taille defgroupe = %ld %ld\n",packetSize,sizeof(def_groupe[0]));
		 send_debug_data(connexion,(char*) &def_groupe[0], packetSize,channel_number);*/

		return; /*manage_connexion_udp(connexion); *//*envoie et attend la suite*/
	}
	else printf("pas le bon message \n");
	printf("FIN %s \n", __FUNCTION__);
}

int manage_connexion_udp(type_connexion_udp *connexion, int channel_number)
{
	int res, running = 1;
	int event_return;
	char buf[buffer_size];
	int buflen;
	char buffer[buffer_size];

	dprints("sem_wait %s \n", __FUNCTION__);
	res = sem_wait(&(network_debug_link.lock)); /* attend que le message precedent ait ete envoye */

	event_return = enet_host_service(connexion->host, &(connexion->event), connexion->waitTime); /* processing incoming events*/

	if (event_return < 0)
	{
		dprints("sem_post %s \n", __FUNCTION__);
		sem_post(&(network_debug_link.lock)); /* on relibere le jeton pour les autres */
		EXIT_ON_ERROR("error in event_host_service \n");
	}

	switch (connexion->event.type)
	{
	case ENET_EVENT_TYPE_CONNECT:
		dprints("  A new connection has been established to %u:%u\n", connexion->event.peer->address.host, connexion->event.peer->address.port);
		sprintf(buf, "%u:%u", connexion->event.peer->address.host, connexion->event.peer->address.port);
		buflen = strlen(buf);
		network_debug_link.event.peer->data = malloc(buflen + 1);
		strncpy((char*) connexion->event.peer->data, buf, buflen);
		connexion->peer = connexion->event.peer;

		event_return = enet_address_get_host_ip(&connexion->event.peer->address, buffer, buffer_size);
		printf("serveur ip = %s \n", buffer);
		break;
	case ENET_EVENT_TYPE_RECEIVE:
		/*  if(verbose_debug) */
	{
		dprints("  A packet of length %lu containing [%s] was received from %s on channel %u\n", connexion->event.packet->dataLength, connexion->event.packet->data, (char*) connexion->event.peer->data, connexion->event.channelID);
	}
		/* .... traitement des donnees */
		connexion->data_treatment(connexion, channel_number);
		enet_packet_destroy(connexion->event.packet); /* clean up the packet now that we're done using it */
		break;
	case ENET_EVENT_TYPE_DISCONNECT:
		dprints("\n\n  ***** enet: HOST DISCONNECTED !!! ***** \n\n");
		free(connexion->event.peer->data);
		connexion->event.peer->data = 0;
		connexion->peer = 0;
		running = 0;/* reset the peer's client information. */
		break;
	default: /*printf("rien default case \n");*/
		break;
	}
	dprints("sem_post %s \n", __FUNCTION__);
	sem_post(&(network_debug_link.lock)); /* on relibere le jeton pour les autres */
	return (running);
}

/******************************************************************/

/* init network for udp connexion using enet : used for distant oscillo_kernel */
/* ip_adr= 127.0.0.1 par ex ou nom_adr=localhost... */

int init_nework_debug(type_connexion_udp *connexion, char *ip_adr, int port)
{
	int value, res;
	int packetSize = 1000;
	char *buffer;
  ENetEvent event;

	buffer = getenv("CID"); /* channel id global var */
	if (buffer == NULL)
	{
		printf("WARNING %s: la variable d'environnement CID n'a pas ete definie: faire export CID=1 par ex. \n", __FUNCTION__);
		channel_number = 1;
	}
	else
	{
		channel_number = atoi(buffer);
		printf("env CID= %s, ChannelID= %d \n", buffer, channel_number);
	}

	/* network initialization: */

	if (enet_initialize() != 0) EXIT_ON_ERROR("An error occurred while initializing ENet.\nAn error occurred while initializing ENet.\n");

	atexit(enet_deinitialize); /* PG: A verifier si pas de conflit au niveau global de promethe */
	enet_time_set(0);

	connexion->address.port = port; /* bind connection to this port */
	connexion->packet = NULL;
	connexion->data_treatment = reception_debug_data;
	connexion->waitTime = event_waitTime; /* number of milliseconds to wait for network events, corresponding to time between frames  */
	connexion->sendCounter = 0;
	connexion->host = enet_host_create(NULL, 1, ENET_PROTOCOL_MAXIMUM_CHANNEL_COUNT, 0,  0);

	if (!connexion->host) EXIT_ON_ERROR("An error occurred while trying to create an ENet client host.\n");

	if (enet_address_set_host(&(connexion->address), ip_adr) != 0) EXIT_ON_ERROR("Error with address %s", ip_adr);
	connexion->address.port = port;

	connexion->peer = enet_host_connect(connexion->host, &(connexion->address), ENET_PROTOCOL_MAXIMUM_CHANNEL_COUNT, 0);

  /* Wait up to 2 seconds for the connection attempt to succeed. */
  if (enet_host_service (connexion->host, &event, 2000) > 0 &&  event.type == ENET_EVENT_TYPE_CONNECT)  dprints("Connection to %s:%u succeeded. \n", ip_adr, port);
  else dprints("Connection to %s:%u failed \n.", ip_adr, port);

	if (!connexion->peer) EXIT_ON_ERROR("No available peers for initiating an ENet connection.\n");

	packetSize = sizeof(type_trame_init);
	connexion->data = (char *) malloc(packetSize);
	connexion->packet = enet_packet_create(connexion->data, packetSize, (ENET_PACKET_FLAG_RELIABLE & ENET_PACKET_FLAG_NO_ALLOCATE ));

	if (connexion->packet == NULL) EXIT_ON_ERROR("The packet has not been created.");

	sleep(1);
	enet_peer_send (connexion->peer, 0, connexion->packet);
	sleep(1);
	enet_host_flush(connexion->host);
	/*, &(connexion->event), connexion->waitTime);  processing incoming events*/


/*	res = sem_init(&(connexion->lock), 0, 1);*/ /* pas de lock au depart */
/*	if (res != 0) EXIT_ON_ERROR("res init lock  %s = %d \n", __FUNCTION__, res); */

/*	value = manage_connexion_udp(connexion, channel_number);*/
/*
	init_com_def_groupe(com_def_groupe, def_groupe, nbre_groupe);
	send_def_groupe_data(connexion, channel_number);*/
	/*
	value = manage_connexion_udp(connexion, channel_number);*/ /* connexion reelle a l'oscillo et envoie effectif des donnees */

	return (0);
}

void quit_debug_client(type_connexion_udp *connexion)
{
	int event_return;

	printf("quit_debug_client()\n");

	if (connexion->data != NULL)
	{
		free(connexion->data);
		connexion->data = NULL;
	}
	if (connexion->peer)
	{
		enet_peer_disconnect(connexion->peer, 0);
		event_return = enet_host_service(connexion->host, &connexion->event, connexion->waitTime);
		if (event_return < 0) printf("error in event_host_service\n");

		printf("resetting peer...");
		fflush(stdout);
		enet_peer_reset(connexion->peer);
		printf(" done.\n");
	}

	printf("closing down...\n");
	fflush(stdout);
	enet_host_destroy(connexion->host);
	printf(" done.\n");
	enet_deinitialize();
	exit(1);
}

/*------------------------------------------------------------------*/

void init_enet_token_oscillo_kernel(char *ip_adr, int port)
{
	int value;

	value = init_nework_debug(&network_debug_link, ip_adr, port);
	if (value != 0) printf("FAILURE init enet network for oscillo_kernel \n");

}

void quit_enet_token_oscillo_kernel()
{
	quit_debug_client(&network_debug_link);
}

void send_token_oscillo_kernel(int gpe, int phase, long temps)
{
	type_nn_message data_network; /*PG: pas malin la structure a ete aussi alloue par malloc dans connexion */
	int running, res;

	dprints("sem_post %s \n", __FUNCTION__);
	res = sem_wait(&(network_debug_link.lock)); /* attend que le message precedent ait ete envoye */
	data_network.type_trame = GROUP_DEBUG;

	if (sizeof(struct type_nn_message) > packetSize)
	{
		printf("ERROR: taille des packets trop petite / donnes (%d > %ld) \n", (int) sizeof(struct type_nn_message), packetSize);
		exit(EXIT_FAILURE);
	}
	/*  data_network.NN_id=1; *//* No attribue a un script / promethe */

	no_message++;
	data_network.val_nn_message_id = NN_message_magic_card;
	data_network.no_message = no_message;
	data_network.time_stamp = temps;
	/*  printf("send_token no_message=%d, time_stamp=%f /// gpe=%d phase=%d \n",no_message,data_network.time_stamp,gpe,phase); */
	data_network.gpe = gpe;
	data_network.type_message = phase;
	send_debug_data(&network_debug_link, (char *) &data_network, packetSize, channel_number);
	dprints("sem_post %s \n", __FUNCTION__);
	sem_post(&(network_debug_link.lock)); /* on relibere le jeton pour les autres */

	running = manage_connexion_udp(&network_debug_link, channel_number);
}

/*--- main ---------------------------------------------------------*/

/*
 int main (int argc, char ** argv)
 {
 int nPacketsMax=200000; / total number of packets to before application terminates, use 0 to disable /
 int verbose=1;              / should complete traffic be dumped to stdout? /

 ENetAddress address;

 unsigned int sendCounter=0;
 unsigned int recvCounter=0;
 unsigned int sendPerSec=0, recvPerSec=0;
 double tLastFps,tNow;
 unsigned int fps=0;
 int running=1;

 char buf[buffer_size];
 char * data, *data2;
 int buflen;
 int reliable=1;
 int channel_number=0;
 unsigned int i;
 int event_return,value;

 struct sigaction action;

 if(argc<4)
 {
 fprintf(stderr,"usage: clien port [servername] no_nn\n");
 return 1;
 }

 / network initialization: /
 if (enet_initialize() != 0)
 {
 fprintf(stderr,"An error occurred while initializing ENet.\n");
 return 1;
 }
 atexit (enet_deinitialize);

 sigfillset(&action.sa_mask);
 action.sa_handler = quit_debug_client;
 action.sa_flags = 0;

 / sigaction(SIGQUIT, &action, NULL); /
 if (sigaction(SIGINT, &action, NULL) == 0)
 fprintf(stdout,"signal SIGINT capture par client pour tout fermer proprement\n");
 else
 fprintf(stderr,"signal SIGINT non capture par client pour tout fermet proprement\n");

 enet_time_set(0);
 address.port = atoi(argv[1]); / bind connection to this port /
 init_nework_debug(&network_debug_link);

 printf("I am client...\n");
 fflush(stdout);
 network_debug_link.host = enet_host_create (0, / create a client host /
 60, / allow only 1 outgoing connection /
 0, / use 57600 / 8 for 56K modem with 56 Kbps downstream bandwidth /
 0);/ use 14400 / 8 for 56K modem with 14 Kbps upstream bandwidth /
 if (!network_debug_link.host)
 {
 fprintf(stderr,"An error occurred while trying to create an ENet client host.\n");
 exit (EXIT_FAILURE);
 }

 / connect to server: /
 printf("connect to server\n");
 value=enet_address_set_host (&address, argv[2]);
 if(value!=0) printf("error %d with adr: %s \n",value,argv[2]);
 network_debug_link.peer = enet_host_connect (network_debug_link.host, &address, 60);
 network_debug_link.peer->data=0; / use this as mark that connection is not yet acknowledged /
 if (!network_debug_link.peer)
 {
 fprintf(stderr,"No available peers for initiating an ENet connection.\n");
 exit (EXIT_FAILURE);
 }

 printf("network id = %d \n",atoi(argv[3]));
 channel_number=data_network.NN_id=atoi(argv[3]);

 gettimeofday(&my_time, 0);
 secsFirstCall=my_time.tv_sec;   printf("temps init = %d \n",secsFirstCall);
 tLastFps=timestamp();

 / main loop: /
 printf("main loop \n");

 data=(char*)malloc(packetSize);
 network_debug_link.packet = enet_packet_create(data, packetSize, (ENET_PACKET_FLAG_RELIABLE & ENET_PACKET_FLAG_NO_ALLOCATE ));
 /  network_debug_link.event.packet=enet_packet_create(data, packetSize, ENET_PACKET_FLAG_RELIABLE & ENET_PACKET_FLAG_NO_ALLOCATE); /
 running=manage_connexion_udp(&network_debug_link);

 while(running)
 {
 tNow=timestamp();
 ++fps;
 if(tNow>=tLastFps+1.0)
 {
 printf("%f secs, %u fps, containers sent per sec:%u recv:%u total sent:%u recv: %u\n", tNow-tLastFps,fps,sendPerSec,recvPerSec,sendCounter,recvCounter);
 fflush(stdout);
 sendPerSec=recvPerSec=fps=0;
 tLastFps=tNow;
 }

 data_to_be_sent(data,packetSize);
 send_debug_data(&network_debug_link,data,packetSize,channel_number);
 running=manage_connexion_udp(&network_debug_link);
 } / main loop /

 quit_debug_client();
 return 0;
 }
 */

