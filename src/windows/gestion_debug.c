/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*! \file  gestion_debug.c
 *   \brief Ensemble de procedures utilisees pour le debugging des RN
 *          - affiche les sorties de chaque groupe
 *          - affiche les poids des neurones
 *          - dessine les liaisons entre les neurones des gpes
 *
 * Copyrights Philippe GAUSSIER Dec 1992.
 * Modifie Dec 1995
 *
 *   - debug =0 no display
 *   - debug =1 display values [0-1]
 *   - debug =2 normalization [0-max]
 *
 *
 */

#include "public.h"
#include "outils.h"
#include "outils_script.h"

/*#define VERBOSE 1
 #define DEBUG 1*/

#ifndef PUBLIC_H
#warning toto
#endif
int taille_bloc_debug = 128;
int selected_group = -1; /*!< groupe selectionne pour etre deplace avec la sourie */
TxPoint selected_group_point;
int last_debugged_grp = -1;
int debug_output_number = 1; /*!< s1 est debugge par defaut */
extern type_groupe def_groupe[];
extern type_pando_groupe para_pando_group[];

void led_groupe(TxDonneesFenetre *fenetre, int gpe, int couleur)
{
	TxPoint point;

	point.x = def_groupe[gpe].p_posx;
	point.y = def_groupe[gpe].p_posy - 14;
	TxDessinerCercle(fenetre, couleur, TxPlein /*TxVide*/, point, 6, 3);
	TxDisplay(fenetre);
}

void affiche_breakpoint(TxDonneesFenetre *fenetre, int gpe, int visible)
{
	TxPoint point, point1, point2, centre;
	int couleur;
	int rayon = 6;

	point.x = def_groupe[gpe].p_posx - 4 * rayon;
	point.y = def_groupe[gpe].p_posy;

	point1.x = point.x - rayon;
	point1.y = point.y - rayon;
	point.x = point.x + rayon + rayon / 3;
	point2.x = point.x + rayon;
	point2.y = point.y + rayon;

	if (visible == TRUE) /* on affiche le symbole du breakpoint */
	{
		couleur = bordeau;
		printf("--- %s \n", __FUNCTION__);
		centre.x = (point1.x + point2.x) / 2;
		centre.y = (point1.y + point2.y) / 2;
		TxDessinerCercle(fenetre, couleur, TxVide, centre, 2 * rayon, rayon / 2);
		/*  TxDessinerCercle(fenetre, couleur, TxVide, point, rayon, rayon/2);*/
		TxDessinerSegment(fenetre, couleur, point1, point2, rayon / 2);
		point.x = point2.x;
		point.y = point1.y;
		point2.x = point1.x;
		point2.y = point2.y;
		TxDessinerSegment(fenetre, couleur, point, point2, rayon / 2);
	}
	else /* on efface le symbole du breakpoint */
	{
		couleur = blanc;
		point1.x = point1.x - rayon / 2;
		point1.y = point1.y - 1.5 * rayon;
		TxDessinerRectangle(fenetre, couleur, TxPlein, point1, 5 * rayon, 7 * rayon, rayon / 2);
	}
	TxFlush(fenetre);
	TxDisplay(fenetre);
}

/*!  Mode de debugging. Affichage des sorties des neurones
 *               pour tous les groupes
 */
void affiche_debug_gpe(TxDonneesFenetre * fenetre, int j)
{
	TxPoint point, point2;
	int i, x, y, dx, dy, taillex, tailley;
	float pasx, pasy, Si, Si_abs;
	float taille_bloc, longueur;
	float max;
	int deb, nbre, nbre2;
	int increment;
	char buffer[256];
	float seuil_affichage;

	if (debug == 0) return;

#ifdef DEBUG
	printf("---affiche_debug_gpe %s\n", def_groupe[j].no_name);
#endif

	taille_bloc = (float) taille_bloc_debug;
	/*  TxEffacerAireDessin(fenetre, blanc); */
	couleur = noir;

	/*  if(inverse_video==1)
	 {
	 TxDessinerRectangle(fenetre,blanc,TxPlein,point,300,300,0);
	 couleur=noir;
	 }
	 */
	dx = def_groupe[j].taillex;
	dy = def_groupe[j].tailley;
	nbre2 = dx * dy;
	deb = def_groupe[j].premier_ele;
	nbre = def_groupe[j].nbre;
	if (nbre2 < 1)
	{
		printf("ERREUR debug: la taille du groupe %s (%d) est nulle \n", def_groupe[j].no_name, j);
		exit(0);
	}
	increment = nbre / nbre2; /*!< != 1 cas des micro neurones */

	if (dx < 1) dx = 1;
	if (dy < 1) dy = 1;

	if (dx > 1 && dy > 1) seuil_affichage = 0.1;
	else seuil_affichage = 0.0;

	pasx = taille_bloc / ((float) dx);
	pasy = taille_bloc / ((float) dy);

	x = 0;
	y = 0;
	if (def_groupe[j].rttoken <= 0) couleur = blanc;
	else couleur = 1 + (def_groupe[j].rttoken * 5) % nb_max_couleurs;

	/*printf("groupe %d (%s): dx=%d, dy=%d, pasx=%f, pasy=%f \n",j,def_groupe[j].nom,dx,dy,pasx,pasy);
	 getchar(); */
	/*recherche le max pour normaliser les sorties, division par max*/

	if (def_groupe[j].debug == 2)
	{
		max = -99999.;
		for (i = deb + increment - 1; i < deb + nbre; i += increment)
		{
			switch (debug_output_number)
			{
			case 0:
				Si = neurone[i].s;
				break;
			case 1:
				Si = neurone[i].s1;
				break;
			case 2:
				Si = neurone[i].s2;
				break;
			default:
				printf("ERROR: It is impossible to debug the output neuron[i].s%d, good interval = [0,2]\n ", debug_output_number);
				printf("debug_output_number %d \n", debug_output_number);
				exit(1);
			}

			if (Si > max) max = Si;
		}
#ifdef DEBUG
		printf("groupe %s : max = %f \n", def_groupe[j].no_name, max);
#endif
		max = fabs(max);
		if (max < 0.00001) max = 1.;
	}
	else max = 1.;

	point.x = def_groupe[j].p_posx - 4;
	point.y = def_groupe[j].p_posy - 4;

	/*effacer le groupe */
	TxDessinerRectangle(fenetre, noir, TxPlein, point, taille_bloc_debug + 6, taille_bloc_debug + 6, 0);
	TxDessinerRectangle(fenetre, bleu, TxVide, point, 6 + (int) taille_bloc, 6 + (int) taille_bloc, 1);

	/* affichage du groupe de neurones */

	for (i = deb + increment - 1; i < deb + nbre; i += increment)
	{
		point.x = (int) (((float) x) * pasx) + def_groupe[j].p_posx;
		point.y = (int) (((float) (y)) * pasy) + def_groupe[j].p_posy;
		/*      Si=neurone[i].s1*pas;
		 if(Si<neurone[i].s2*pas) Si=neurone[i].s2*pasx;
		 if(Si<neurone[i].s*pas)
		 */
		switch (debug_output_number)
		{
		case 0:
			Si = neurone[i].s / max;
			break;
		case 1:
			Si = neurone[i].s1 / max;
			break;
		case 2:
			Si = neurone[i].s2 / max;
			break;
		default:
			printf("ERROR: It is impossible to debug the output neuron[i].s%d, good interval = [0,2]\n ", debug_output_number);
			printf("debug_output_number %d \n", debug_output_number);
			exit(1);
		}

		Si_abs = fabs(Si);

		taillex = (int) (fabs(Si * pasx));
		if (taillex > (int) pasx) taillex = (int) pasx;
		else
		{
			if (taillex < 1 && Si_abs > seuil_affichage) taillex = 1;
			else if (pasx > 1. && Si_abs <= seuil_affichage) taillex = 1;
		}
		/*if(taillex<=1 && pasx>2) taillex=1; */

		tailley = (int) (fabs(Si * pasy));
		if (tailley > (int) pasy) tailley = (int) pasy;
		else
		{
			if (tailley < 1 && Si_abs > seuil_affichage) tailley = 1;
			else if (pasy > 1. && Si_abs <= seuil_affichage) tailley = 1;
		}
		/*if(tailley<=1 && pasy>2) tailley=1; */

		if (taillex > 0 && pasx > 2. && tailley == 0) tailley = 1;
		else if (tailley > 0 && pasy > 2. && taillex == 0) taillex = 1;

		point.x = point.x + (pasx - taillex) / 2;
		point.y = point.y + (pasy - tailley) / 2;

		/*printf("Si= %f ,taillex= %d, tailley=%d \n",Si,taillex,tailley); */
		if (Si >= 0. /*&& pasx>1 */) TxDessinerRectangle(fenetre, couleur, TxPlein, point, taillex, tailley, 0);
		else TxDessinerRectangle(fenetre, rouge, TxVide, point, taillex, tailley, 1);

		x++;
		if (x >= dx)
		{
			x = 0;
			y++;
		}
	}

	if (p_trace > 0 && last_debugged_grp >= 0)
	{ /* efface le point lie au gpe precedent */
		led_groupe(fenetre, last_debugged_grp, ciel);
	}

	point2.x = def_groupe[j].p_posx - 5;
	point2.y = def_groupe[j].p_posy - 24;
	point.x = def_groupe[j].p_posx + 10;
	point.y = def_groupe[j].p_posy - 11;
	/* petite modif a la demande de micka par JC */
	/* affichage du numero du groupe, en plus de son nom */
	/*TxEcrireChaine(fenetre, noir, point, def_groupe[j].nom, NULL); */
	snprintf(buffer, 256, "%s (%s) %d", def_groupe[j].nom, def_groupe[j].no_name, def_groupe[j].rttoken);
	longueur = 8 * strlen(buffer) + 10;
	TxDessinerRectangle(fenetre, jaune, TxPlein, point2, longueur, 16, 1);
	TxEcrireChaine(fenetre, noir, point, buffer, NULL);

	if (p_trace > 0)
	{
		led_groupe(fenetre, last_debugged_grp, vert);
		led_groupe(fenetre, last_debugged_grp, ciel);
		last_debugged_grp = j;
	}

	if (def_groupe[j].breakpoint > 0)
	{
		affiche_breakpoint(fenetre, j, TRUE);
	}
	/*  TxFlush(fenetre); */
}

/*------------------------------------------------------------------------*/

/* affiche uniquement le debug des groupes de l'echelle ech */

void affiche_debug(TxDonneesFenetre * fenetre, int ech)
{
	int j;

	/*  if(inverse_video==1)
	 {
	 TxDessinerRectangle(fenetre,blanc,TxPlein,point,300,300,0);
	 couleur=noir;
	 }
	 */

	/*    printf("echelle debug = %d \n",ech);*/
	/*    GdkRectangle rectangle;

	 if(fenetre->region_rectangle==NULL)
	 {
	 rectangle.x=0;rectangle.y=0;rectangle.width=fenetre->width;rectangle.height=fenetre->height;
	 fenetre->region_rectangle=gdk_region_rectangle(&rectangle);
	 }
	 gdk_window_begin_paint_region(fenetre->window_pixmap,fenetre->region_rectangle);*/
	for (j = 0; j < nbre_groupe; j++)
		if (def_groupe[j].debug > 0 && def_groupe[j].ech_temps == ech)
		{
			affiche_debug_gpe(fenetre, j);
		}
	/*  XFlush(fenetre->display); */

	/*  TxFlush(fenetre); */
	/*  gdk_window_end_paint(fenetre->window_pixmap); */
#ifdef DEBUG
	printf("affiche debug\n");
#endif
	TxDisplay(fenetre);
}

/*------------------------------------------------------------------------*/
/*               dessine le RN                                            */
/*------------------------------------------------------------------------*/

void init_position_neurones_groupe(int j)
{
	int dx, dy;
	int deb, nbre, nbre2;
	int increment;
	float pasx, pasy;
	float taille_bloc;
	int x, y;
	int i, k;

	taille_bloc = (float) taille_bloc_debug;
#ifdef VERBOSE
	printf("init position gpe %d:\n", def_groupe[j].no_name);
	printf("\t%s\n",def_groupe[j].nom);
	printf("\t%s\n",def_groupe[j].no_name);
#endif

	dx = def_groupe[j].taillex;
	dy = def_groupe[j].tailley;
	deb = def_groupe[j].premier_ele;
	nbre = def_groupe[j].nbre;
	nbre2 = dx * dy;
	/*printf("nbre2 =%d,dx=%d,dy=%d \n",nbre2,dx,dy); */
	increment = nbre / nbre2; /* pour les micro neurones */

	if (increment == 0)
	{
		printf("init position gpe %s:\n", def_groupe[j].no_name);
		printf("nbre = %d , nbre2 =%d,dx=%d,dy=%d \n", nbre, nbre2, dx, dy);
		printf("\t%s_%s\n", def_groupe[j].nom, def_groupe[j].no_name);
		printf("Error in init_position_neurones_groupe()/gestion_debug.c\nincrement nul\nAttention au nombre de neurone (peut etre un pb de complation du .res)\n");
		exit(0);
	}

	if (dx > 0)
	{
		/* if(dx==1) dx=2; */
		pasx = taille_bloc / ((float) dx);
		/* if(pasx<1.) pasx=1.; */
	}
	else pasx = 1.;
	if (dy > 0)
	{
		/* if(dy==1) dy=2; */
		pasy = taille_bloc / ((float) dy);
		/* if(pasy<1.) pasy=1.; */
	}
	else pasy = 1.;

	x = 0;
	y = 0;
	for (i = deb; i < deb + nbre; i += increment)
	{
		if (increment > 1) for (k = 0; k < increment - 1; k++)
		{ /* s'occupe des micro neurones */
			pos_n_x[i + k] = (int) (((float) x) * pasx + pasx / 2.) + def_groupe[j].p_posx + (int) (pasx / 3. * cos(1. + 6.28 * ((float) k) / ((float) (increment - 1))));
			pos_n_y[i + k] = (int) (((float) (y)) * pasy + pasy / 2.) + def_groupe[j].p_posy + (int) (pasy / 3. * sin(1. + 6.28 * ((float) k) / ((float) (increment - 1))));
		}
		pos_n_x[i + increment - 1] = (int) (((float) x) * pasx + pasx / 2.) + def_groupe[j].p_posx;
		pos_n_y[i + increment - 1] = (int) (((float) (y)) * pasy + pasy / 2.) + def_groupe[j].p_posy;
		x++;
		if (x >= dx)
		{
			x = 0;
			y++;
		}
	}
}

void init_position_neurones(void)
{
	int j;

	if (position_neurones_deja_initialisee == 1) return;
	/*printf("initialisation de la position des neurones \n"); */

	pos_n_x = (int *) malloc(sizeof(int) * nbre_neurone);
	pos_n_y = (int *) malloc(sizeof(int) * nbre_neurone);
	if (pos_n_x == NULL || pos_n_y == NULL)
	{
		printf("pb d'allocation memoire dans init_position_neurones() \n");
		exit(0);
	}

	for (j = 0; j < nbre_groupe; j++)
	{
		init_position_neurones_groupe(j);
	}
	position_neurones_deja_initialisee = 1;
	/*printf("fin de l'initialisation\n"); */
}

/*! affiche les neurones initialises dans la fonction init_position_neurones()
 * a condition que leur flqg debug soit mis a 1
 */
#define TAILLE_BUFFER 256
void affiche_rn(TxDonneesFenetre * fenetre)
{
	TxPoint point;
	int i, j;
	int deb, nbre;
	char buffer[TAILLE_BUFFER];
	/*printf("Affiche le reseau de neurone\n"); */
	TxEffacerAireDessin(fenetre, blanc);
	couleur = blanc;

	if (position_neurones_deja_initialisee != 1) init_position_neurones();

	/*  if(inverse_video==1)
	 {
	 TxDessinerRectangle(fenetre,blanc,TxPlein,point,300,300,0);
	 couleur=noir;
	 }
	 */

	for (j = 0; j < nbre_groupe; j++)
		if (def_groupe[j].debug > 0)
		{
			deb = def_groupe[j].premier_ele;
			nbre = def_groupe[j].nbre;
			point.x = def_groupe[j].p_posx - 5;
			point.y = def_groupe[j].p_posy - 5;

			/*TxDessinerRectangle(fenetre,bleu,TxPlein,point,taille_bloc_debug,taille_bloc_debug,1); */
			couleur = noir;
			for (i = deb; i < deb + nbre; i++)
			{
				point.x = pos_n_x[i];
				point.y = pos_n_y[i];
				TxDessinerRectangle(fenetre, couleur, TxPlein, point, 1, 1, 1);
			}

			point.x = def_groupe[j].p_posx + 10;
			point.y = def_groupe[j].p_posy - 10;
			/* petite modif a la demande de micka par JC */
			/* affichage du numero du groupe, en plus de son nom */
			/*TxEcrireChaine(fenetre, noir, point, def_groupe[j].nom, NULL); */
			snprintf(buffer, TAILLE_BUFFER, "%s (%s)", def_groupe[j].nom, def_groupe[j].no_name);
			TxEcrireChaine(fenetre, noir, point, buffer, NULL);

			point.x = def_groupe[j].p_posx;
			point.y = def_groupe[j].p_posy;
			TxDessinerRectangle(fenetre, bleu, TxVide, point, taille_bloc_debug, taille_bloc_debug, 2);

			if (def_groupe[j].breakpoint > 0) affiche_breakpoint(fenetre, j, TRUE);
		}
	/*printf("Fin d'affichage le reseau de neurone\n"); */
	TxDisplay(fenetre);
}

/*! affiche les liaisons du neurone selectionne a la sourie      */
void affiche_liaisons(GdkEventButton * event)
{
	TxPoint l_point, l_point2;
	type_coeff *coeff;
	int i = -1, j;
	int local_selected_group = -1;
	int deb, nbre;
	int succes = 0;
	float d;
	char nom[255];

	l_point.x = (int) event->x;
	l_point.y = (int) event->y;

	j = 0;
	while (j < nbre_groupe && succes == 0)
	{
		if (def_groupe[j].debug > 0)
		{
			deb = def_groupe[j].premier_ele;
			nbre = def_groupe[j].nbre;
			couleur = blanc;

			for (i = deb; i < deb + nbre; i++)
			{
				d = fabs((float) (pos_n_x[i] - l_point.x)) + fabs((float) (pos_n_y[i] - l_point.y));
				if (d < 4.)
				{
					succes = 1;
					local_selected_group = j;
					break;
				}
			}
		}
		j++;
	}

	j = local_selected_group;
	if (succes == 0 || local_selected_group < 0) return;

	kprints("groupe %s neurone %d : \n", def_groupe[j].no_name, i);
	kprints("   s= %f, s1= %f, s2=%f \n", neurone[i].s, neurone[i].s1, neurone[i].s2);
	kprints("   seuil= %f, flag= %d, d=%f , cste=%f\n", neurone[i].seuil, neurone[i].flag, neurone[i].d, neurone[i].cste);
	kprints("   nbre_voie=%d, nbre_coeff=%d, max?=%c \n", neurone[i].nbre_voie, neurone[i].nbre_coeff, neurone[i].max);
	kprints("   posx= %f, posy= %f, posz= %f \n", neurone[i].posx, neurone[i].posy, neurone[i].posz);

	coeff = neurone[i].coeff;
	l_point.x = pos_n_x[i];
	l_point.y = pos_n_y[i];
	strcpy(nom, "aff_neurone.lena");
	affiche_neurone2(im4, 256, 256, nom, i, 0.01);

	while (coeff != NULL)
	{ /* si special alors entree +1, -1  */
		l_point2.x = pos_n_x[coeff->entree];
		l_point2.y = pos_n_y[coeff->entree];

		TxDessinerSegment(&fenetre1, rouge, l_point, l_point2, 1);
		coeff = coeff->s;
	}
	/*  TxFlush(&fenetre1); */
	TxDisplay(&fenetre1);
}

void affiche_selected_group(TxDonneesFenetre *fenetre, int gpe, int visible)
{
	TxPoint point;
	int couleur;

	if (visible == TRUE) couleur = rouge;
	else couleur = blanc;
	point.x = def_groupe[gpe].p_posx;
	point.y = def_groupe[gpe].p_posy;
	selected_group_point.x = point.x - selected_group_point.x;
	selected_group_point.y = point.y - selected_group_point.y;
	TxDessinerRectangle(fenetre, couleur, TxVide, point, taille_bloc_debug, taille_bloc_debug, 2);
	TxDisplay(fenetre);
}

void select_group(GdkEventButton * event)
{
	TxPoint point;
	int j, succes = 0;

	point.x = selected_group_point.x = event->x;
	point.y = selected_group_point.y = event->y;

	j = 0;
	while (j < nbre_groupe && succes == 0) /* recherche du no du groupe dont les coord correspondent */
	{
		if (def_groupe[j].debug > 0)
		{
			if (def_groupe[j].p_posx - 2 < point.x && def_groupe[j].p_posx + taille_bloc_debug > point.x && def_groupe[j].p_posy - 20 < point.y && def_groupe[j].p_posy + taille_bloc_debug > point.y)
			{ /* on est dans le rectangle englobant le groupe: succes */
				succes = 1;
				break;
			}
		}
		j++;
	}

	if (succes == 0)
	{
		if (selected_group > 0) def_groupe[selected_group].selected = 0; /* de-selection de l'ancien groupe */
		selected_group = -1;
		return;
	}
	printf("groupe %s \n", def_groupe[j].no_name);
	selected_group = j;

	def_groupe[selected_group].selected = 1; /* peut servir pour mettre un breakpoint */
	affiche_selected_group(&fenetre1, selected_group, TRUE);
}

void modifier_groupe(GdkEventButton * event)
{
	TxPoint point;

	point.x = event->x;
	point.y = event->y;

	if (selected_group < 0) select_group(event);
	else
	{
		printf("groupe %s : %s\n", def_groupe[selected_group].no_name, def_groupe[selected_group].nom);
		printf("    type=%d, type2= %d, nbre= %d \n", def_groupe[selected_group].type, def_groupe[selected_group].type2, def_groupe[selected_group].nbre);
		printf("    seuil= %f, tolerance= %f \n", def_groupe[selected_group].seuil, def_groupe[selected_group].tolerance);
		printf("    nbre_voie = %d \n", def_groupe[selected_group].nbre_voie);
		printf("    ech_temps= %d, deja_active= %d \n", def_groupe[selected_group].ech_temps, def_groupe[selected_group].deja_active);

		printf("    premier_ele= %d \n", def_groupe[selected_group].premier_ele);
		printf("    dvn=%d, dvp=%d \n", def_groupe[selected_group].dvn, def_groupe[selected_group].dvp);
		printf("    alpha=%f, nbre_de_1=%f, sigma_f=%f \n", def_groupe[selected_group].alpha, def_groupe[selected_group].nbre_de_1, def_groupe[selected_group].sigma_f);

		if (selected_group < 0)
		{
			printf("ERROR: group not selected, cannot be moved \n");
			return;
		}

		def_groupe[selected_group].selected = 0; /* de-selection de l'ancien groupe */
		def_groupe[selected_group].p_posx = point.x + selected_group_point.x;
		def_groupe[selected_group].p_posy = point.y + selected_group_point.y;
		init_position_neurones_groupe(selected_group);
		selected_group = -1;
		affiche_rn(&fenetre1);
	}
}

/*------------------------------------------------------------*/

void supprime_debug_groupe(GdkEventButton * event)
{
	TxPoint point;
	int j;
	int succes = 0;

	point.x = event->x;
	point.y = event->y;

	j = 0;
	while (j < nbre_groupe && succes == 0)
	{
		if (def_groupe[j].debug > 0)
		{
			if (def_groupe[j].p_posx - 5 < point.x && def_groupe[j].p_posx + taille_bloc_debug > point.x && def_groupe[j].p_posy - 5 < point.y && def_groupe[j].p_posy + taille_bloc_debug > point.y)
			{
				succes = 1;
				break;
			}
		}
		j++;
	}

	if (succes == 0) return;
	printf("groupe %s \n", def_groupe[j].no_name);
	def_groupe[j].debug = 0;
	affiche_rn(&fenetre1);
}

/*------------------------------------------------------------*/

void *gestion_debug_thread(void *event)
{
	int action = 0;

	gestion_mask_signaux();
	action = ((GdkEventButton *) event)->button; /* numero du bouton appuy� */

	/*    down = event->button; !!!!!pb ici */
	/*    action = event_action(event);  PROVISOIRE PG */

	if (position_neurones_deja_initialisee == 0)
	{
		affiche_rn(&fenetre1);
		position_neurones_deja_initialisee = 1;
		pthread_exit(NULL);
		return NULL;
	}
	/* event->states possibles GDK_BUTTON_PRESS, GDK_2BUTTON_PRESS (pressee fois) ,
	 GDK_3BUTTON_PRESS, and GDK_BUTTON_RELEASE. */

	if (action == 1) affiche_liaisons(event); /* ACTION_SELECT) */
	else if (action == 3) modifier_groupe(event); /* ACTION_ADJUST) */
	else if (action == 2) supprime_debug_groupe(event); /* ACTION_MENU)   */

	pthread_exit(NULL);
	return NULL;
}

static GdkEventButton last_event; /* memoire de l'etat du bouton au moment de l'appel */

/* fonction appelee lorsque l'on clique sur la fenetre principale: fenetre1 */
gboolean gestion_debug(GtkWidget * widget, GdkEventButton * event, gpointer data)
{
	pthread_t un_thread;
	(void)widget;
	(void)data;

	/* sauvegarde l'etat du bouton au moment de l'appel.
	 Evite les interference avec le futur de l'etat du bouton*/
	memcpy(&last_event, event, sizeof(GdkEventButton));

	pthread_create(&un_thread, NULL, gestion_debug_thread, (void *) &last_event);
	return TRUE;
}

/*------------------------------------------------------------*/

void *reset_all_debug_thread(void *data)
{
	int j;
	(void)data;

	kprints("%s: tous les groupes sont affiches pour le debug \n", __FUNCTION__);
	for (j = 0; j < nbre_groupe; j++)	def_groupe[j].debug = 1;

	printf("re-affichage \n");
	affiche_rn(&fenetre1);
	return NULL;
}

void reset_all_debug(GtkWidget * widget, gpointer data)
{
	int res;
	pthread_t un_thread;
	(void)widget;
	(void) data;

	res = pthread_create(&un_thread, NULL, reset_all_debug_thread, NULL);
	if (res != 0)
	{
		printf("ERROR in %s : pthread_create failed \n", __FUNCTION__);
	}

}

/** ATTENTION il faudrait separer le moide aveugle (sans fgcd) du non aveugle avec fgcd. AB.*/
void save_script(GtkWidget * widget, gpointer data)
{
	(void) widget;
	(void) data;

	ecriture_script(fscript, fgcd);
}

/*! Recherche le groupe en entree du groupe gpe qui a la taillex la plus grande
 taillex et tailley sont des parameres de retour.
 Ils correspondent aux dim max en x et y.*/

void recherche_dim_entree_max(int gpe, int *taillex, int *tailley)
{
	int gpe_entree, i, lien;

	*taillex = -1;
	*tailley = -1;
	for (i = 0; (lien = find_input_link(gpe, i)) != -1; i++)
	{
		gpe_entree = liaison[lien].depart;
		if (*taillex < def_groupe[gpe_entree].taillex) *taillex = def_groupe[gpe_entree].taillex;
		if (*tailley < def_groupe[gpe_entree].tailley) *tailley = def_groupe[gpe_entree].tailley;
	}
}

/*!   affiche les poids d'un macro neurone
 *    tient compte des voies de liaisons
 *    n est le numero du neurone
 */
int flag_normalisation = 1;


/* Appele par affiche la liaison affiche_liaisons avec seuil de 0.01 mais pas utilise ici !!! */
void affiche_neurone2(unsigned char *im3, int xmax, int ymax, char *fsortie, int n, float seuil)
{
	int kx, ky;
	type_coeff *coeff;
	TxPoint point;
	int gpe;
	int pasx, pasy;
	int taillex_max, tailley_max;
	float max;
	float echx, echy;
	int entree;
	int nb_link = 0;
	int affiche = 1;
	int no_groupe;
	/*   char c;
	 char rep;*/
	couleur = noir;
	TxEffacerAireDessin(&fenetre2, blanc);
	(void)im3;
	(void)seuil;

	/* cherche le poids d'entree max pour normaliser la taille des poids */

	if (flag_normalisation < 0) return;

	coeff = neurone[n].coeff;
	max = -99999.;
	while (coeff != NULL)
	{
		if (fabs(coeff->val) > max) max = fabs(coeff->val);
		coeff = coeff->s;
	}

	kprints("%s : ", fsortie);
	kprints("coefficient le plus grand en valeur abs = %f \n", max);

	/* cherche le groupe d'entree qui a la taillex la plus grande */

	gpe = neurone[n].groupe;
	recherche_dim_entree_max(gpe, &taillex_max, &tailley_max);

	pasx = xmax / taillex_max;
	pasy = ymax / tailley_max;
	if (pasx < 1) pasx = 1;
	if (pasy < 1) pasy = 1;

	if (flag_normalisation == 1)
	{
		echx = ((float) pasx) / max;
		echy = ((float) pasy) / max;
	}
	else if (flag_normalisation == 1)
	{
		echx = ((float) pasx) / (1. + log(fabs(max)));
		echy = ((float) pasy) / (1. + log(fabs(max)));
	}
	else
	{
		echx = ((float) pasx);
		echy = ((float) pasy);
	}

	kprints("echx = %f, echy = %f  \n", echx, echy);

	coeff = neurone[n].coeff;
	while (coeff != NULL)
	{
		nb_link++;
		coeff = coeff->s;
	}
	kprints("nombre de lien(s): %d\n", nb_link);
	/*   if (nb_link > 20)
	 {
	 printf("There are %d links print them? [y/n]\n", nb_link);
	 scanf("%c", &rep);
	 while ((c = getchar()) != '\n') ;
	 if (rep == 'n')
	 affiche = 0;
	 }*/
	coeff = neurone[n].coeff;
	if (affiche) while (coeff != NULL)
	{
		entree = coeff->entree;
		no_groupe = neurone[entree].groupe;
		point.x = def_groupe[no_groupe].p_posx + ((int) neurone[entree].posx) * pasx;
		point.y = def_groupe[no_groupe].p_posy + ((int) neurone[entree].posy) * pasy;
		if (flag_normalisation == 1)
		{
			kx = (int) (coeff->val * echx);
			ky = (int) (coeff->val * echy);
		}
		else if (flag_normalisation == 2)
		{
			kx = (int) fabs((log(fabs(coeff->val)) * echx));
			ky = (int) fabs((log(fabs(coeff->val)) * echy));
		}
		else
		{
			kx = (int) (coeff->val * echx);
			ky = (int) (coeff->val * echy);
			if (kx > pasx) kx = pasx;
			else if (kx < -pasx) kx = -pasx;
			if (ky > pasy) ky = pasy;
			else if (ky < -pasy) ky = -pasy;
		}

		if (coeff->val < 0.)
		{
			kx = -kx;
			ky = -ky;
		}
		if (echx > 2. && kx == 0) kx = 1;
		if (echy > 2. && ky == 0) ky = 1;
		if (kx > 0 || ky > 0)
		{
			if (coeff->val >= 0.) TxDessinerRectangle(&fenetre2, couleur, TxPlein, point, kx, ky, 0);
			else TxDessinerRectangle(&fenetre2, couleur, TxVide, point, kx, ky, 1);
		}
		kprints("w = %f , entree = %d ", coeff->val, coeff->entree);
		kprints("         proba= %f, Nbre_ES= %f, Nbre_S=%f, Nbre_E=%f \n", coeff->proba, coeff->Nbre_ES, coeff->Nbre_S, coeff->Nbre_E);
		kprints("         mode= %d, type= %d, evolution= %d \n", liaison[coeff->gpe_liaison].mode, coeff->type, coeff->evolution);
		coeff = coeff->s;
	}
	TxDisplay(&fenetre2);
}

/*---------------------------------------------------------------------*/

void affiche_neurone(unsigned char *im3, int xmax, int ymax, char *fsortie)
{
	int n;
	float seuil;
	kprints("affichage\n");
	kprints("No du neurone : ");
	cscans("%d", &n);
	kprints("seuil : ");
	cscans("%f", &seuil); /** ATTENTION le seuil n'est pas pris en compte dans affiche_neurone2 : AB. */
	if (n < 0) return;
	if (n >= nbre_neurone) return;
	affiche_neurone2(im3, xmax, ymax, fsortie, n, seuil);
}

/*---------------------------------------------------------------------*/

void *affiche_liaisons_groupe_thread(void *data)
{
	int i, j, k, kx = 1., ky = 1., n, l, dx, dy, dx2, dy2;
	float x = 0, y = 0;
	type_coeff *pt;
	TxPoint point, l_point2;
	int nbre_liaisons;
	int taille_groupe;
	float ech2x, ech2y;
	float max, echx = 1., echy = 1.;
	int l_x0, l_y0;
	int normalisation;
	float seuil = 0.001;
	int xmax, ymax;
	int increment;
	int ym;
	char group_name[255];
	int no_gpe = -1;
	(void) data;

	gestion_mask_signaux();
	while (no_gpe < 0)
	{
		printf("identifiant du groupe a afficher (int ou char) : ");
		if (scanf("%s", group_name) != 1) PRINT_WARNING("Wrong fromat");
		no_gpe = find_no_associated_to_symbolic_name(group_name, NULL);
	}
	printf("no du groupe = %d\n", no_gpe);
	printf("normalisation globale (1) : ");
	if (scanf("%d", &normalisation) != 1) PRINT_WARNING("Not a integer.");

	taille_groupe = def_groupe[no_gpe].nbre;
	dx2 = def_groupe[no_gpe].taillex;
	dy2 = def_groupe[no_gpe].tailley;
	increment = taille_groupe / (dx2 * dy2);
	xmax = 40;
	ymax = 40;
	n = def_groupe[no_gpe].premier_ele;
	taille_groupe = n + taille_groupe;

	couleur = noir;

	nbre_liaisons = 0; /* nbre d'entrees                  */
	pt = neurone[n].coeff;
	while (pt != NULL)
	{ /* compte le nombre d'entrees      */
		nbre_liaisons++;
		pt = pt->s;
	}
	recherche_dim_entree_max(no_gpe, &dx, &dy);
	printf("dim matrice entree = %d , %d\n", dx, dy);
	if (dx < 1 || dy < 1)
	{
		printf("c'est une entree\n");
		pthread_exit(NULL);
		return NULL;
	}
	ech2x = (float) xmax / (float) dx;
	ech2y = (float) ymax / (float) dy / (float) increment; /* les micro neurones sont affich� verticalement
	 les uns dessous les autres */
	/*   if(ech2x<1.) ech2x=1.;
	 if(ech2y<1.) ech2y=1.;  */

	TxEffacerAireDessin(&fenetre1, blanc);

	if (normalisation != 1) for (l_y0 = 0; l_y0 < dy2; l_y0++)
	{
		for (l_x0 = 0; l_x0 < dx2; l_x0++)
		{
			printf("increment=%d\n", increment);
			for (ym = 0; ym < increment - 1; ym++)
			{ /* traite les micro neurones */
				pt = neurone[n].coeff;
				max = -99999.;
				while (pt != NULL) /* recherche le max */
				{
					if (fabs(pt->val) > max) max = fabs(pt->val);
					pt = pt->s;
				}
				echx = ((float) xmax) / ((float) dx) / max;
				echy = ((float) ymax) / ((float) dy) / max / ((float) increment);
				pt = neurone[n].coeff;
				while (pt != NULL) /* affiche le poids synaptique */
				{
					y = neurone[pt->entree].posy;
					x = neurone[pt->entree].posx;
					point.x = 10 + (int) (x * ech2x) + l_x0 * (xmax + 3);
					point.y = 10 + (int) (y * ech2y + ((float) (ym * dy)) * ech2y) + l_y0 * (ymax + 3);
					if (fabs(pt->val) < seuil)
					{
						kx = 0;
						ky = 0;
					}
					else
					{
						kx = (int) (pt->val * echx);
						ky = (int) (pt->val * echy);
					}
					if (kx >= 0 && kx < 1) kx = 1;
					else if (kx < 0 && kx > -1) kx = -1;
					if (ky >= 0 && ky < 1) ky = 1;
					else if (ky < 0 && ky > -1) ky = -1;
					if (pt->val > 0) TxDessinerRectangle(&fenetre1, couleur, TxPlein, point, kx, ky, 0);
					else TxDessinerRectangle(&fenetre1, couleur, TxVide, point, -kx, -ky, 1);
					pt = pt->s;
				}
				point.x = 20 + (dx2 + l_x0) * (xmax + 3);
				point.y = 10 + l_y0 * (ymax + 3) + (int) (((float) (ym * dy)) * ech2y);
				k = (int) (2. * (neurone[n].seuil - 0.45) * (float) (xmax - 1) / (float) increment);
				if (k >= 0 && k < 1) k = 1;
				if (k > 0) TxDessinerRectangle(&fenetre1, couleur, TxPlein, point, k, k, 1);
				else TxDessinerRectangle(&fenetre1, couleur, TxVide, point, -k, -k, 1);

				n++; /* neurone suivant */
				if (n >= taille_groupe) break;
			} /* fin de traitement des micro neurones */

			if (increment > 1)
			{ /* traite le macro neurone */
				pt = neurone[n].coeff;
				max = -99999.;
				while (pt != NULL)
				{
					if (fabs(pt->val) > max) max = fabs(pt->val);
					pt = pt->s;
				}
				echx = ((float) xmax) / ((float) dx) / max;
				echy = ((float) ymax) / ((float) dy) / max / ((float) increment);
				pt = neurone[n].coeff;
				i = 0;
				j = 0;
				while (pt != NULL)
				{
					point.x = 10 + (int) (x * ech2x) + l_x0 * (xmax + 3);
					point.y = 10 + (int) (y * ech2y + ((float) ((increment - 1) * dy)) * ech2y) + l_y0 * (ymax + 3);
					if (fabs(pt->val) < seuil)
					{
						kx = 0;
						ky = 0;
					}
					else
					{
						kx = (int) (pt->val * echx);
						ky = (int) (pt->val * echy);
					}
					if (kx >= 0 && kx < 1) kx = 1;
					else if (kx < 0 && kx > -1) kx = -1;
					if (ky >= 0 && ky < 1) ky = 1;
					else if (ky < 0 && ky > -1) ky = -1;
					if (pt->val > 0) TxDessinerRectangle(&fenetre1, couleur, TxPlein, point, kx, ky, 0);
					else TxDessinerRectangle(&fenetre1, couleur, TxVide, point, -kx, -ky, 1);

					pt = pt->s;
					i++;
				}
				point.x = 20 + (dx2 + l_x0) * (xmax + 3);
				point.y = 10 + l_y0 * (ymax + 3) + (int) (((float) (ym * dy)) * ech2y);
				k = (int) (2. * (neurone[n].seuil - 0.45) * (float) (xmax - 1) / (float) increment);
				if (k >= 0 && k < 1) k = 1;
				if (k > 0) TxDessinerRectangle(&fenetre1, couleur, TxPlein, point, k, k, 1);
				else TxDessinerRectangle(&fenetre1, couleur, TxVide, point, -k, -k, 1);

				n++; /* neurone suivant */
				if (n >= taille_groupe) break;
			}
			else
			{ /* un seul neurone : pas de macro neurone */
				ym = 0;
				pt = neurone[n].coeff;
				max = -99999.;
				printf("Pas de micro neurone \n");
				while (pt != NULL)
				{
					if (fabs(pt->val) > max) max = fabs(pt->val);
					pt = pt->s;
				}
				echx = ((float) xmax) / ((float) dx) / max;
				echy = ((float) ymax) / ((float) dy) / max / ((float) increment);
				pt = neurone[n].coeff;
				while (pt != NULL)
				{
					y = neurone[pt->entree].posy;
					x = neurone[pt->entree].posx;
					point.x = 10 + (int) (x * ech2x) + l_x0 * (xmax + 3);
					point.y = 10 + (int) (y * ech2y + ((float) (ym * dy)) * ech2y) + l_y0 * (ymax + 3);
					if (fabs(pt->val) < seuil)
					{
						kx = 0;
						ky = 0;
					}
					else
					{
						kx = (int) (pt->val * echx);
						ky = (int) (pt->val * echy);
					}

					if (kx >= 0 && kx < 1) kx = 1;
					else if (kx < 0 && kx > -1) kx = -1;
					if (ky >= 0 && ky < 1) ky = 1;
					else if (ky < 0 && ky > -1) ky = -1;
					/*  printf("- %d %d \n",kx,ky); */
					if (pt->val > 0) TxDessinerRectangle(&fenetre1, couleur, TxPlein, point, kx, ky, 0);
					else TxDessinerRectangle(&fenetre1, couleur, TxVide, point, -kx, -ky, 1);
					pt = pt->s;
				}
				point.x = 20 + (dx2 + l_x0) * (xmax + 3);
				point.y = 10 + l_y0 * (ymax + 3) + (int) (((float) (ym * dy)) * ech2y);
				k = (int) (2. * (neurone[n].seuil - 0.45) * (float) (xmax - 1) / (float) increment);
				if (k >= 0 && k < 1) k = 1;
				if (k > 0) TxDessinerRectangle(&fenetre1, couleur, TxPlein, point, k, k, 1);
				else TxDessinerRectangle(&fenetre1, couleur, TxVide, point, -k, -k, 1);

				n++; /* neurone suivant */
				if (n >= taille_groupe) break;
			}
		}
		if (n >= taille_groupe) break;
	}

	else /* cas d'une normalisation globale */
	{
		max = -99999.;
		for (n = def_groupe[no_gpe].premier_ele; n < taille_groupe; n++)
		{
			pt = neurone[n].coeff;
			for (j = 0; j < nbre_liaisons; j++) /* cherche le max */
			{
				if (fabs(pt->val) > max) max = fabs(pt->val);
				pt = pt->s;
				if (pt == NULL) break;
			}
		}
		printf("max global = %f \n", max);
		echx = ((float) xmax) / ((float) dx) / max;
		echy = ((float) ymax) / ((float) dy) / max;
		printf("echx =%f, xmax=%d, dx=%d \n", echx, xmax, dx);

		n = def_groupe[no_gpe].premier_ele;
		for (l_y0 = 0; l_y0 < dy2; l_y0++)
		{
			for (l_x0 = 0; l_x0 < dx2; l_x0++)
			{
				pt = neurone[n].coeff;
				for (l = 0; l < nbre_liaisons; l++)
				{
					j = l / dx;
					i = l - j * dx;

					point.x = 10 + i * ech2x + l_x0 * (xmax + 3);
					point.y = 10 + j * ech2y + l_y0 * (ymax + 3);

					if (pt->val >= 0.) /* w>0: facteur d'echelle pour l'affichage */
					{
						kx = (int) (pt->val * echx);
						ky = (int) (pt->val * echy);
					}
					else /* w<0: facteur d'echelle pour l'affichage */
					{
						kx = (int) (-pt->val * echx);
						ky = (int) (-pt->val * echy);
					}

					if (kx < 1) kx = 1;
					if (ky < 1) ky = 1;

					/*printf("-- %d %d %d %d \n",point.x,point.y,kx,ky);*/
					if (pt->val >= 0.) TxDessinerRectangle(&fenetre1, noir, TxVide, point, kx, ky, 1);
					else TxDessinerRectangle(&fenetre1, rouge, TxVide, point, kx, ky, 1);

					pt = pt->s;
					if (pt == NULL) break;
				}
				point.x = 20 + (dx2 + l_x0) * (xmax + 3);
				point.y = 20 + (l_y0) * (xmax + 3);
				k = (int) (2. * (neurone[n].seuil - 0.45) * (float) (xmax - 5));
				if (k >= 0 && k < 1) k = 1;
				if (k > 0) TxDessinerRectangle(&fenetre1, couleur, TxPlein, point, k, k, 1);
				else TxDessinerRectangle(&fenetre1, couleur, TxVide, point, -k, -k, 1);

				n++; /* neurone suivant */
				if (n >= taille_groupe) break;
			}
			if (n >= taille_groupe) break;
		}
	}

	/* affiche le quadrillage */

	couleur = vert;
	point.x = 10;
	l_point2.x = 10 + dx2 * (xmax + 3);
	for (l_y0 = 0; l_y0 < dy2; l_y0++)
	{
		for (j = 0; j < increment; j++)
		{
			point.y = 10 + l_y0 * (ymax + 3) + j * ymax / increment;
			l_point2.y = 10 + l_y0 * (ymax + 3) + j * ymax / increment;
			TxDessinerSegment(&fenetre1, couleur, point, l_point2, 1);
		}
	}
	couleur = rouge;
	point.y = 10;
	l_point2.y = 10 + dy2 * (ymax + 3);
	for (l_x0 = 0; l_x0 <= dx2; l_x0++)
	{
		point.x = 10 + l_x0 * (xmax + 3);
		l_point2.x = 10 + l_x0 * (xmax + 3);
		TxDessinerSegment(&fenetre1, couleur, point, l_point2, 1);
	}
	point.x = 10;
	l_point2.x = 10 + dx2 * (xmax + 3);
	for (l_y0 = 0; l_y0 <= dy2; l_y0++)
	{
		point.y = 10 + l_y0 * (ymax + 3);
		l_point2.y = 10 + l_y0 * (ymax + 3);
		TxDessinerSegment(&fenetre1, couleur, point, l_point2, 1);
	}
	/* TxFlush(&fenetre1); */
	TxDisplay(&fenetre1);
	pthread_exit(NULL);
	return NULL;
}

void affiche_liaisons_groupe(GtkWidget * widget, gpointer data)
{
	pthread_t un_thread;
	(void)widget;
	(void) data;

	if (pthread_create(&un_thread, NULL, affiche_liaisons_groupe_thread, NULL) != 0)
	{
		printf("impossible de creer un thread pour 'affiche_liaisons_groupe_thread'\n");
	} /* JC :
	 j'ai modifie le type de retour pour la compatibilite avec (GtkItemFactoryEntry) menu_items
	 il n'y a donc plus de return TRUE en fin de fonction, mais retourner toujours TRUE sert a quelque chose ?
	 j'ai rajoute un test sur la valeur de retour de pthread_create, en cas d'echec un message est affiche
	 */
}

/* affiche debug d'un groupe en gros dans la deuxieme fenetre */
/*version modifee pour afficher les macro colonnes en 2D dans le cas d'un groupe 1D */

void ecriture_lena(unsigned char *im3, int xmax, int ymax, int gpe)
{
	int  i, j, dx, dy, p2, deb, taillex, tailley, tailleg, nbre, increment;
	float echx, echy, echg;
	/*  float taille_max, taille_min; */
	int x_offset = 10, y_offset = 40;
	TxPoint point;
	static char chaine[255];

	/*if (rapide == 1) return;*/
	TxEffacerAireDessin(&fenetre2, blanc);

	sprintf(chaine, "Debug .s activity of group %s, named : %s \n", def_groupe[gpe].no_name, def_groupe[gpe].nom);
	point.x = 10;
	point.y = 20;
	TxEcrireChaine(&fenetre2, bleu, point, chaine, NULL);

	dx = def_groupe[gpe].taillex;
	dy = def_groupe[gpe].tailley;
	if (dx < 1 || dy < 1)
	{
		printf("ERREUR: le groupe courant a une taille nulle \n");
		exit(0);
	}
	nbre = def_groupe[gpe].nbre;
	increment = nbre / (dx * dy);
	if (((1 + dy * dx) * increment) > 262144) /*si superieur en taille a la taille de im3, le groupe co,tient trop de neurones (M. Maillard) */
	{
		printf("Le groupe a debugger contient trop de neurones pour etre afficher dans la fenetre 2 de debug\n");
		return;
	}

	/* gestion d'une colonne ou ligne de macro colonne */
	if (increment > 1 && dx == 1)
	{
		dx = increment;
		increment = 1;
	}
	else
	{
		if (increment > 1 && dy == 1)
		{
			dy = increment;
			increment = 1;
		}
	}
	echx = ((float) xmax) / ((float) dx);
	echy = ((float) ymax) / ((float) dy);
	deb = def_groupe[gpe].premier_ele;

	for (j = 0; j < dy; j++)
		for (i = 0; i < dx; i++)
		{
			p2 = (1 + i + j * dx) * increment - 1;
			point.x = x_offset + (int) (((float) i) * echx);
			point.y = y_offset + (int) (((float) j) * echy);
			if (neurone[p2 + deb].s < 0.)
			{
				taillex = (int) (-neurone[p2 + deb].s * echx);
				tailley = (int) (-neurone[p2 + deb].s * echy);
				if (taillex < 1) taillex = 1;
				if (tailley < 1) tailley = 1;
				TxDessinerRectangle(&fenetre2, rouge, TxVide, point, taillex, tailley, 1);
			}
			else
			{
				taillex = (int) (neurone[p2 + deb].s * echx);
				tailley = (int) (neurone[p2 + deb].s * echy);
				if (taillex < 1) taillex = 1;
				if (tailley < 1) tailley = 1;
				TxDessinerRectangle(&fenetre2, noir, TxPlein, point, taillex, tailley, 1);
			}
			if (taillex < tailley)
			{
				tailleg = tailley;
				echg = echy;
			}
			else
			{
				tailleg = taillex;
				echg = echx;
			}
			if (tailleg > (int) echg) tailleg = (int) echg;
			if (tailleg <= 0) tailleg = 1;
			im3[p2] = (int) (((float) (tailleg - 1)) * 256. / echg);
		}
	TxDisplay(&fenetre2);
	/*   printf("appuyez sur une touche ");
	 scanf("%c",&c);
	 */

	/*   printf("ecriture de l'image\n");
	 decri.ny=xmax;
	 decri.nx=xmax;
	 p_im=ouvre('e',&decri,fsortie);
	 ecri(&p_im,im3,decri.ny); */
}

/* ancienne version sans la gestion des macro colonnes */
void ecriture_lena_old(unsigned char *im3, int xmax, int ymax, int gpe)
{
	int  i, j, dx, dy, p2, deb, taillex, tailley, tailleg, nbre, increment;
	float echx, echy, echg;
	/*  float taille_max, taille_min; */
	int x_offset = 10, y_offset = 40;
	TxPoint point;
	static char chaine[255];

	/*if (rapide == 1) return;*/
	TxEffacerAireDessin(&fenetre2, blanc);

	sprintf(chaine, "Debug .s activity of group %s, named : %s \n", def_groupe[gpe].no_name, def_groupe[gpe].nom);
	point.x = 10;
	point.y = 20;
	TxEcrireChaine(&fenetre2, bleu, point, chaine, NULL);

	dx = def_groupe[gpe].taillex;
	dy = def_groupe[gpe].tailley;
	if (dx < 1 || dy < 1)
	{
		printf("ERREUR: le groupe courant a une taille nulle \n");
		exit(0);
	}
	nbre = def_groupe[gpe].nbre;
	increment = nbre / (dx * dy);
	if (((1 + dy * dx) * increment) > 262144) /*si superieur en taille a la taille de im3, le groupe co,tient trop de neurones (M. Maillard) */
	{
		printf("Le groupe a debugger contient trop de neurones pour etre afficher dans la fenetre 2 de debug\n");
		return;
	}
	echx = ((float) xmax) / ((float) dx);
	echy = ((float) ymax) / ((float) dy);
	deb = def_groupe[gpe].premier_ele;

	for (j = 0; j < dy; j++)
		for (i = 0; i < dx; i++)
		{
			p2 = (1 + i + j * dx) * increment - 1;
			point.x = x_offset + (int) (((float) i) * echx);
			point.y = y_offset + (int) (((float) j) * echy);
			if (neurone[p2 + deb].s < 0.)
			{
				taillex = (int) (-neurone[p2 + deb].s * echx);
				tailley = (int) (-neurone[p2 + deb].s * echy);
				if (taillex < 1) taillex = 1;
				if (tailley < 1) tailley = 1;
				TxDessinerRectangle(&fenetre2, rouge, TxVide, point, taillex, tailley, 1);
			}
			else
			{
				taillex = (int) (neurone[p2 + deb].s * echx);
				tailley = (int) (neurone[p2 + deb].s * echy);
				if (taillex < 1) taillex = 1;
				if (tailley < 1) tailley = 1;
				TxDessinerRectangle(&fenetre2, noir, TxPlein, point, taillex, tailley, 1);
			}
			if (taillex < tailley)
			{
				tailleg = tailley;
				echg = echy;
			}
			else
			{
				tailleg = taillex;
				echg = echx;
			}
			if (tailleg > (int) echg) tailleg = (int) echg;
			if (tailleg <= 0) tailleg = 1;
			im3[p2] = (int) (((float) (tailleg - 1)) * 256. / echg);
		}
	TxDisplay(&fenetre2);
	/*   printf("appuyez sur une touche ");
	 scanf("%c",&c);
	 */

	/*   printf("ecriture de l'image\n");
	 decri.ny=xmax;
	 decri.nx=xmax;
	 p_im=ouvre('e',&decri,fsortie);
	 ecri(&p_im,im3,decri.ny); */
}
