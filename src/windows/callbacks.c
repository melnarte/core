/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "public.h"
#include "outils.h"

#define SIZE_OF_BUFFER 1024
TxPoint un_point, point1, point2, point_courant, point_precedent;
int couleur, couleur2, couleur_dessin, coul_ombre1, coul_ombre2;

int couleur_fond = blanc;
int couleur_texte = noir;
 
/* Create a new pixmap of the appropriate size to store our scribbles */
gboolean kernel_configure_event(GtkWidget * widget, GdkEventConfigure * event, gpointer data)
{
	return fenetre_configure_event((TxDonneesFenetre *) data, widget, event, data);
}

gboolean kernel_expose_event(GtkWidget * widget, GdkEventExpose * event, gpointer data)
{
	return fenetre_expose_event((TxDonneesFenetre *) data, widget, event, data);
}

gboolean image1_click_event(GtkWidget * widget, GdkEventButton * event, gpointer data)
{
	gint x, y;
	GdkModifierType state;
  
  int width, height, rowstride, n_channels;
  unsigned int etat;
  guchar *pixels, *p;
  GdkPixbuf *pixbuf;
	(void)event;
	(void)data;

	printf("image1_click_event \n");

	gdk_window_get_pointer(widget->window, &x, &y, &state);
	etat = ((state & GDK_BUTTON1_MASK)!=0) * 1 + ((state & GDK_BUTTON3_MASK)!=0) * 2 + ((state & GDK_BUTTON2_MASK)!=0) * 3; /*Test quel boutton a ete utilise gauche = 1 droit = 2)*/
	printf("Boutton pressed %d\n",etat);

/*	image = gdk_image_get(widget->window,0, 0,image1.width, image1.height);
	     if(image)
	     {
	       pixel = gdk_image_get_pixel(image, x,y);
	       gdk_image_destroy(image);
	     }*/

	pixbuf = gdk_pixbuf_get_from_drawable(NULL, widget->window, NULL, 0, 0, 0, 0, image1.width, image1.height);
	n_channels = gdk_pixbuf_get_n_channels(pixbuf);

	g_assert(gdk_pixbuf_get_colorspace(pixbuf) == GDK_COLORSPACE_RGB);
	g_assert(gdk_pixbuf_get_bits_per_sample(pixbuf) == 8);

	/*	       g_assert (gdk_pixbuf_get_has_alpha (pixbuf));
	 g_assert (n_channels == 4);*/

	width = gdk_pixbuf_get_width(pixbuf);
	height = gdk_pixbuf_get_height(pixbuf);
	g_assert(x >= 0 && x < width);
	g_assert(y >= 0 && y < height);

	rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	pixels = gdk_pixbuf_get_pixels(pixbuf);

	p = pixels + y * rowstride + x * n_channels;
	/*
	 p[0] = red;
	 p[1] = green;
	 p[2] = blue;

	 guchar    *newrgb = (guchar *) pixel;
	 GdkColor  *color  = g_malloc(sizeof(GdkColor));

	 if(big_endian())              * fixme: undeclared *
	 {
	 color->red   = (gushort) (newrgb[2] << 8);
	 color->green = (gushort) (newrgb[1] << 8);
	 color->blue  = (gushort) (newrgb[0] << 8);
	 }
	 else
	 {
	 color->red   = (gushort) (newrgb[1] << 8);
	 color->green = (gushort) (newrgb[2] << 8);
	 color->blue  = (gushort) (newrgb[3] << 8);
	 }

	 return (gdk_colormap_alloc_color(gdk_rgb_get_cmap (), color, FALSE, TRUE) ? color : NULL);
	 */

	printf("(x=%d,y=%d) : [R=%d G=%d B=%d] ; [NG=%d]\n", x, y, p[0], p[1], p[2], (p[0] + p[1] + p[2]) / 3);
	image1_posx = x;
	image1_posy = y;
  if(etat==1) { image1_obj.start_pos1.x=x;image1_obj.start_pos1.y=y; }
  else   if(etat==2) { image1_obj.start_pos2.x=x; image1_obj.start_pos2.y=y; }

  image1_obj.etat=etat;

	image_click_event = 1;
	dragging_image1 = 1;
  g_object_unref (G_OBJECT(pixbuf));
  
	return 1;
}

gboolean image2_click_event(GtkWidget * widget, GdkEventButton * event, gpointer data)
{
	gint x, y;
	GdkModifierType state;
	int width, height, rowstride, n_channels;
	guchar *pixels, *p;
	GdkPixbuf *pixbuf;
	(void) event;
	(void) data;
	printf("image2_click_event \n");

	gdk_window_get_pointer(widget->window, &x, &y, &state);
	pixbuf = gdk_pixbuf_get_from_drawable(NULL, widget->window, NULL, 0, 0, 0, 0, image2.width, image2.height);
	n_channels = gdk_pixbuf_get_n_channels(pixbuf);

	g_assert(gdk_pixbuf_get_colorspace(pixbuf) == GDK_COLORSPACE_RGB);
	g_assert(gdk_pixbuf_get_bits_per_sample(pixbuf) == 8);
	/*  g_assert (gdk_pixbuf_get_has_alpha (pixbuf));
	 g_assert (n_channels == 4);*/

	width = gdk_pixbuf_get_width(pixbuf);
	height = gdk_pixbuf_get_height(pixbuf);

	g_assert(x >= 0 && x < width);
	g_assert(y >= 0 && y < height);

	rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	pixels = gdk_pixbuf_get_pixels(pixbuf);

	p = pixels + y * rowstride + x * n_channels;
	/*
	 convention
	 p[0] = red;
	 p[1] = green;
	 p[2] = blue;
	 */

	printf("(x=%d,y=%d) : [R=%d G=%d B=%d] ; [NG=%d]\n", x, y, p[0], p[1], p[2], (p[0] + p[1] + p[2]) / 3);
	image2_posx = x;
	image2_posy = y;
	image_click_event2 = 1;
	dragging_image2 = 1;

  g_object_unref (G_OBJECT(pixbuf));
  
	return 1;
}

gboolean image1_motion_event(GtkWidget * widget, GdkEventButton * event, gpointer data)
{
	gint x, y;
	GdkModifierType state;
	(void) event;
	(void) data;

	gdk_window_get_pointer(widget->window, &x, &y, &state);
	image1_posx = x;
	image1_posy = y;
	return 1;
}
gboolean image1_release_event(GtkWidget * widget, GdkEventButton * event, gpointer data)
{
	gint x, y;
	GdkModifierType state;
  int etat=-1,res = -1;
  int couleur, tmp;
  int largeur=0, hauteur=0;
  TxPoint point={1,1};

  (void) widget;
	(void) event;
	(void) data;


  couleur=noir;
  gdk_window_get_pointer(widget->window, &x, &y, &state);
  
  printf("Boutton released %d\n",state);  

	printf("(x=%d,y=%d) \n", x, y);
	image1_posx = x;
	image1_posy = y;
	dragging_image1 = 0;
  
  etat=image1_obj.etat;
  printf("etat= %d \n",etat);

  
  if (etat==1) 
  { 
    image1_obj.end_pos1.x=x;image1_obj.end_pos1.y=y; 
    point.x= image1_obj.start_pos1.x;point.y= image1_obj.start_pos1.y;
    couleur=rouge; 
    if ( image1_obj.start_pos1.x > image1_obj.end_pos1.x) 
    {
      tmp = image1_obj.start_pos1.x;
      image1_obj.start_pos1.x = image1_obj.end_pos1.x;
      image1_obj.end_pos1.x = tmp;
    }
    largeur = image1_obj.end_pos1.x - image1_obj.start_pos1.x;
    hauteur = image1_obj.end_pos1.y - image1_obj.start_pos1.y;
  }
  else if(etat==2) 
  { 
    image1_obj.end_pos2.x=x;image1_obj.end_pos2.y=y; 
    point.x= image1_obj.start_pos2.x;point.y= image1_obj.start_pos2.y;
    couleur=vert; 
    if ( image1_obj.start_pos2.x > image1_obj.end_pos2.x) 
    {
      tmp = image1_obj.start_pos2.x;
      image1_obj.start_pos2.x = image1_obj.end_pos2.x;
      image1_obj.end_pos2.x = tmp;
    }
    largeur = image1_obj.end_pos2.x - image1_obj.start_pos2.x;
    hauteur = image1_obj.end_pos2.y - image1_obj.start_pos2.y;
  }

  printf("largeur=%d hauteur=%d, couleur=%d \n",largeur,hauteur,couleur);
  printf("point x=%d y=%d\n",point.x,point.y);
  TxDessinerRectangle_unsafe(&image1, couleur, TxVide, point, largeur, hauteur, 3);
  printf("dessinee \n");
  
  gtk_widget_queue_draw(image1.da);gdk_flush();
	/*TxFlush(&image1);*/
  
  if (image1_obj.etat==3)  /* click bouton du milieu on peut finir l'execution de la fonction algo */
  {
    res = sem_post(&image1_obj.sem_fini); /* reactive le thread en attente */
    if (res != 0)
      {
        dprints("fealure on sem_post in %s gpe %s \n",__FUNCTION__, def_groupe[gpe].no_name);
        exit(EXIT_FAILURE);
      }
  }
	printf("image1_release_event \n");
	return 1;
}

gboolean image2_release_event(GtkWidget * widget, GdkEventButton * event, gpointer data)
{
	(void) widget;

	(void) event;
	(void) data;

	dragging_image2 = 0;
	printf("image2_release_event \n");
	return 1;
}

gboolean image2_expose_event(GtkWidget * widget, GdkEventExpose * event, gpointer data)
{
	(void) widget;

	(void) event;
	(void) data;

	return fenetre_expose_event(&image2, widget, event, data);
}
gboolean image2_motion_event(GtkWidget * widget, GdkEventButton * event, gpointer data)
{
	gint x, y;
	GdkModifierType state;
	(void) widget;
	(void) event;
	(void) data;

	gdk_window_get_pointer(widget->window, &x, &y, &state);
	image2_posx = x;
	image2_posy = y;
	return 1;
}

void delete(GtkWidget * widget, GdkEvent * event, gpointer data)
{
	(void) widget;
	(void) event;
	(void) data;

	gtk_widget_destroy(widget);

}

gint delete_event(GtkWidget * widget, GdkEvent * event, gpointer data)
{
	(void) widget;
	(void) event;
	(void) data;

	g_print("le signal delete_event est survenu.\n");
	return (FALSE);
}

void file_quit_cmd_callback(GtkWidget * widget, gpointer data)
{
	(void) widget;
	(void) data;

	/*g_print("%s\n", (char *) data);*/

	exit(0); /*promethe_quit() appele avant le exit grace a atexit*/

	/*gtk_exit(0);*/
}

TxDonneesFenetre fenetre5;

void about(void)
{
	GtkWidget *button;
	GtkWidget *table;
	GtkWidget *notebook;
	GtkWidget *frame;
	GtkWidget *label;
	char bufferf[SIZE_OF_BUFFER];
	char bufferl[SIZE_OF_BUFFER];


	gtk_set_locale();

	fenetre5.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data(GTK_OBJECT(fenetre5.window), "PROMETHE 2 ABOUT", fenetre5.window);
	gtk_window_set_title(GTK_WINDOW(fenetre5.window), "PROMETHE 2 ABOUT");

	gtk_signal_connect(GTK_OBJECT(fenetre5.window), "delete_event", GTK_SIGNAL_FUNC(delete), NULL);

	gtk_container_border_width(GTK_CONTAINER(fenetre5.window), 25);

	table = gtk_table_new(2, 6, TRUE);
	gtk_container_add(GTK_CONTAINER(fenetre5.window), table);

	/* Creation d'un bloc-notes, placement des indicateurs de page. */

	notebook = gtk_notebook_new();
	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
	gtk_table_attach_defaults(GTK_TABLE(table), notebook, 0, 6, 0, 1);
	gtk_widget_show(notebook);

	/* Ajoute un groupe de pages a la fin du bloc-notes. */

	snprintf(bufferf, SIZE_OF_BUFFER, "Copyright (c) 1991-2003 Neurocybernetic Team, ETIS Lab., CNRS UMR 8051");
	snprintf(bufferl, SIZE_OF_BUFFER, "Fait Par P. Gaussier, A. Revel, M. Quoy,  C. Joulain, S. Lepretre, S. Moga, J.C. Baccon...");

	frame = gtk_frame_new("");
	gtk_container_border_width(GTK_CONTAINER(frame), 10);
	gtk_widget_set_usize(frame, 300, 75);
	gtk_widget_show(frame);

	label = gtk_label_new(bufferf);
	gtk_container_add(GTK_CONTAINER(frame), label);
	gtk_widget_show(label);

	label = gtk_label_new(bufferl);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), frame, label);

	/* Creation du bouton Fermer */

	button = gtk_button_new_with_label("Fermer");

	gtk_signal_connect_object(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(delete), fenetre5.window);

	gtk_table_attach_defaults(GTK_TABLE(table), button, 2, 4, 1, 2);
	gtk_widget_show(button);
	gtk_widget_show(table);
	gtk_widget_show(fenetre5.window);

}

void show_fenetre_2(GtkWidget * widget, gpointer data)
{
	(void) widget;

	if ((int *) data == 0) gtk_window_deiconify(GTK_WINDOW(fenetre2.window));
	else gtk_window_iconify(GTK_WINDOW(fenetre2.window));
}

void show_image_2(GtkWidget * widget, gpointer data)
{
	(void) widget;

	if ((int *) data == 0) gtk_window_deiconify(GTK_WINDOW(image2.window));
	else gtk_window_iconify(GTK_WINDOW(image2.window));
}

void show_image_1(GtkWidget * widget, gpointer data)
{
	(void) widget;

	if ((int *) data == 0) gtk_window_deiconify(GTK_WINDOW(image1.window));
	else gtk_window_iconify(GTK_WINDOW(image1.window));
}

/*************************************************************************/

void lecture_imageb(GtkWidget * widget, gpointer data)
{

	int i;
	(void) widget;
	(void) data;

	/*  lecture_image(im,im2,&xmax,&ymax); */

	if (nbre_fichiers < 4) return;
	printf("\n\n%d noms de fichiers a entrer \n", nbre_fichiers - 3);
	for (i = 3; i < nbre_fichiers; i++)
	{
		printf("nom du fichier %d a lire : ", i);
		if (scanf("%s", argv_fichier[i]) != 1) PRINT_WARNING("Wrong fichier:%s", argv_fichier[i]);
	}
	/*     scanf("%s",nomfich1);
	 init_bas_niveau(nomfich1);
	 mise_a_jour_bas_niveau(im_recons,im,im2,im8,&xmax,&ymax,1);*/
	/*   fabrique_image_carac(im,im2,xmax,ymax,distance_carac,im8); */
}

void modif_coeff(GtkWidget * widget, gpointer data)
{
	(void) widget;
	(void) data;

	/*
	 printf("\n\nnombre de cycles par fenetre : ");
	 scanf("%d", &cycle);
	 printf("nombre de periodes pour le moyennage ");
	 scanf("%f", &periode);
	 printf("Valeur de epsilon : ");
	 scanf("%f", &eps);
	 */
	if (fenetre3.window == NULL) create_range_controls(&fenetre3);
	gtk_widget_show(fenetre3.window);

}

/*---------------------------------------------------------------------------------*/

void *affiche_neurone_thread(void *arg)
{
	char nom[255];
	(void) arg;

	gestion_mask_signaux();
	strcpy(nom, "aff_neurone.lena");
	affiche_neurone(im4, 256, 256, nom);
	pthread_exit(NULL);
	return NULL;
}

void affiche_neuroneb(GtkWidget * widget, gpointer data)
{
	pthread_t un_thread;

	(void) widget;
	(void) data;

	 pthread_create(&un_thread, NULL, affiche_neurone_thread, NULL);
}

/*-----------------------------------------------------------------------------------*/

void *ecriture_lena_thread(void *arg)
{
	(void) arg;

	gestion_mask_signaux();
	kprints("groupe a afficher (devient groupe courant) : ");
	if (scanf("%d", &gpe_courant_a_afficher) != 1) PRINT_WARNING("Wrong format of number");
	nbre_element_gpe_courant = def_groupe[gpe_courant_a_afficher].nbre;
	kprints("nbre d'elements du groupe : %d \n", nbre_element_gpe_courant);
	premier_element_gpe_courant = def_groupe[gpe_courant_a_afficher].premier_ele;
	kprints("premier neurone du groupe : %d \n", premier_element_gpe_courant);
	ecriture_lena(im4, 256, 256, gpe_courant_a_afficher);
	g_message("Think to activate fenetre 2 for a display of the debug!!!\n");

	pthread_exit(NULL);
	return NULL;
}

void ecriture_lenab(GtkWidget * widget, gpointer data)
{
	pthread_t un_thread;

	(void) widget;
	(void) data;

	pthread_create(&un_thread, NULL, ecriture_lena_thread, NULL);
}

/*-----------------------------------------------------------------------------------*/

void rafraichissement_oui(GtkWidget * widget, gpointer data)
{
	(void) widget;
	(void) data;

	rafraichissement_image = 1;
}

void rafraichissement_non(GtkWidget * widget, gpointer data)
{
	(void) widget;
	(void) data;

	rafraichissement_image = 0;
}

void check_debug(gpointer callback_data, guint callback_action, GtkWidget * menu_item)
{
	(void)callback_data;
	(void)menu_item;

	if (callback_action != 3)
	{
		debug = 1;
		debug_output_number = callback_action;
	}
	else debug = 0;

}

void check_step_by_step_cb(gpointer callback_data, guint callback_action, GtkWidget * menu_item)
{
	(void) callback_data;
	(void) menu_item;

	g_message("step by step %d selected\n", callback_action);
	p_trace = callback_action;
	if (p_trace == 0)
	{
		printf("STEP by STEP mode OFF\n");
	}
	else
	{
		printf("STEP by STEP mode ON\n");
		if (p_trace == 2) printf("Detailled mode\n");
	}
}

void check_item_demo_callback(gpointer callback_data, guint callback_action, GtkWidget * menu_item)
{
	(void) callback_data;

	g_message("item button %d selected\n", callback_action);
	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menu_item)))
	{
		printf("demo ON\n");
		demo = 1;
	}
	else
	{
		printf("demo OFF\n");
		demo = 0;
	}
}

void check_item_fast_callback(gpointer callback_data, guint callback_action, GtkWidget * menu_item)
{
	(void) callback_data;
	(void) callback_action;

	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menu_item)))
	{
		printf("fast demo OFF (fenetre2 activated)\n");
		rapide = 0;
	}
	else
	{
		printf("fast demo ON (fenetre2 inactivated)\n");
		rapide = 1;
	}

	printf("fast = %d \n", rapide);
}

void check_inverse_video_callback(gpointer callback_data, guint callback_action, GtkWidget * menu_item)
{
	(void) callback_data;
	(void) callback_action;

	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menu_item)))
	{
		g_message("Inverse video ON\n");
		inverse_video = 1;
	}
	else
	{
		g_message("Inverse video OFF\n");
		inverse_video = 0;
	}
}

/*-----------------------------------------------------------------------------------------*/

void *affiche_rn_thread(void *arg)
{
	(void) arg;

	gestion_mask_signaux();
	affiche_rn(&fenetre1);
	kprints("retour du thread affiche rn vers gtk\n");
	pthread_exit(NULL);
	return NULL;
}

void dessine_rnb(GtkWidget * widget, gpointer data)
{
	pthread_t un_thread;
	int res;

	(void) widget;
	(void) data;

	res = pthread_create(&un_thread, NULL, affiche_rn_thread, NULL);
	printf("l'autre partie est dans le thread res=%d\n", res);
	res = pthread_detach(un_thread);
	if (res == 0) printf("succes detach\n");
	else printf("echec detach\n");

}

/***********************************************************************************/
/* gestion des vue metres pour le changement en ligne des parametres de simulation */
/***********************************************************************************/

void cb_hscale_get_value(GtkAdjustment * adj)
{
	/* Set the number of decimal places to which adj->value is rounded */
	/*gtk_scale_set_digits (GTK_SCALE (hscale), (gint) adj->value);*/
	eps = gtk_adjustment_get_value(adj);
	printf("eps=%f\n", eps);

}

gboolean cb_hide_fenetre(GtkWidget * widget, gpointer data)
{

	(void) widget;

	gtk_widget_hide(((TxDonneesFenetre *) data)->window /*widget ou fenetre3.window */);
	return TRUE;
}

gboolean cb_destroy_fenetre(GtkWidget * widget, gpointer data)
{
	(void) widget;

	((TxDonneesFenetre *) data)->window = NULL;
	return TRUE;
}

/***************************************************************************/
/*	Callback fenetre 2	*/
void check_debug_link(gpointer callback_data, guint callback_action, GtkWidget * menu_item)
{
	(void) callback_data;
	(void) menu_item;

	g_message("check debug link %d selected\n", callback_action);
	flag_normalisation = callback_action;

}

/* affiche dans le terminal la liste des threads en activite */

#define max_parallele 256

extern int gpes_en_parallele[max_parallele];

void stack_threads(GtkWidget * widget)
{
	int i, indice, last_indice;
	(void) widget;

	last_indice = gpes_en_parallele[0];
	kprints("Liste des threads en traitement: \n");
	for (i = 0; i < max_parallele; i++)
	{
		indice = gpes_en_parallele[i];
		if (indice < last_indice || last_indice < 0) break;
		kprints("i=%d --- gpe=%d, no_name=%s , name %s\n", i, indice, def_groupe[indice].no_name, def_groupe[indice].nom);
		last_indice = indice;
		/* if(arg[i].gpe>=0)
		 printf("i=%d --- gpe=%d, no_name=%s , no_thread = %d \n",i, arg[i].gpe,def_groupe[arg[i].gpe].no_name ,arg[i].no_thread);*/
	}

	kprints("fin de la pile des threads en traitement \n");
}
