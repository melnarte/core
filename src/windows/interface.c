/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "public.h"



#include <stdio.h>
#include <math.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <semaphore.h>
#include <graphic_Tx.h>

#define nb_vumetre 2

TxDonneesFenetre fenetre1; /* Main promethe window */
TxDonneesFenetre fenetre2; /* Fenetre 2 => for debug information */
TxDonneesFenetre fenetre3; /* Range control window */

TxDonneesFenetre image1; /* For user drawing */
TxDonneesFenetre image2; /* For user drawing */

float image1_posx;
float image1_posy;
int image_click_event = 0;
int dragging_image1 = 0;

float image2_posx;
float image2_posy;
int image_click_event2 = 0;
int dragging_image2 = 0;

GtkWidget * mouse_widget=NULL;

 
 type_image_obj image1_obj;

/****************************************************************************************/
/*CREATION DE DE LA FENETRE1                                                            */
/****************************************************************************************/


/* Our menu, an array of GtkItemFactoryEntry structures that defines each menu item */
static GtkItemFactoryEntry menu_items[] = {
    {(char*)"/_Simulation", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/Simulation/online learning", NULL, set_learn_and_use_mode, 0, (char*)"<Item>", NULL},
    {(char*)"/Simulation/use only", NULL, set_use_only_mode, 0, (char*)"<Item>", NULL},
    {(char*)"/Simulation/continue learning", NULL, set_continue_learning_mode, 0, (char*)"<Item>", NULL},
    {(char*)"/Simulation/continue using", NULL, set_continue_only_mode, 0, (char*)"<Item>", NULL},
    {(char*)"/_Param simul", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/_Param simul/_Step by Step", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/_Param simul/Step by Step/OFF", NULL, check_step_by_step_cb, 0, (char*)"<RadioItem>", NULL},
    {(char*)"/_Param simul/Step by Step/ON", NULL, check_step_by_step_cb, 1,  (char*)"/Param simul/Step by Step/OFF", NULL},
    {(char*)"/_Param simul/Step by Step/ON detailled", NULL, check_step_by_step_cb, 2, (char*)"/Param simul/Step by Step/OFF", NULL},
    {(char*)"/_Param simul/_Demo", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/_Param simul/Demo/activated", NULL, check_item_demo_callback, 0,  (char*)"<CheckItem>", NULL},
    {(char*)"/_Param simul/Demo/fenetre2 activated", NULL, check_item_fast_callback,  0, (char*)"<CheckItem>", NULL},
    {(char*)"/_Param simul/_Param Modif", NULL, modif_coeff, 0, (char*)"<StockItem>", GTK_STOCK_QUIT},

    {(char*)"/_Param simul/_Debug", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/_Param simul/Debug/yes s2", NULL, check_debug, 2, (char*)"<RadioItem>", NULL},
    {(char*)"/_Param simul/Debug/yes s1", NULL, check_debug, 1, (char*)"/Param simul/Debug/yes s2", NULL},
    {(char*)"/_Param simul/Debug/yes s", NULL, check_debug, 0, (char*)"/Param simul/Debug/yes s2", NULL},
    {(char*)"/_Param simul/Debug/no", NULL, check_debug, 3, (char*)"/Param simul/Debug/yes s2", NULL},
    {(char*)"/_File", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/File/Save script", NULL, save_script, 0, (char*)"<StockItem>", GTK_STOCK_FLOPPY},
    {(char*)"/File/_Save NN", NULL, ecriture_reseaub, 0, (char*)"<StockItem>",  GTK_STOCK_SAVE},

    {(char*)"/_Show NN", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/Show NN/Draw NN", NULL, dessine_rnb, 0, (char*)"<Item>", NULL},
    {(char*)"/Show NN/reset all", NULL, reset_all_debug, 0, (char*)"<Item>", NULL},
    {(char*)"/Show NN/save script", NULL, save_script, 0, (char*)"<Item>", NULL},
    {(char*)"/Show NN/show Synapses", NULL, affiche_liaisons_groupe, 0, (char*)"<Item>", NULL},

    {(char*)"/_Windows", NULL, NULL, 0,(char*) "<Branch>", NULL},
    {(char*)"/_Windows/show image1", NULL, show_image_1, 0, (char*)"<Item>", NULL},
    {(char*)"/_Windows/hide image1", NULL, show_image_1, 1, (char*)"<Item>", NULL},
    {(char*)"/_Windows/show image2", NULL, show_image_2, 0, (char*)"<Item>", NULL},
    {(char*)"/_Windows/hide image2", NULL, show_image_2, 1, (char*)"<Item>", NULL},
    {(char*)"/_Windows/show fenetre2", NULL, show_fenetre_2, 0, (char*)"<Item>", NULL},
    {(char*)"/_Windows/hide fenetre2", NULL, show_fenetre_2, 1, (char*)"<Item>", NULL},
    {(char*)"/_Windows/show oscillo kernel", NULL, show_oscillo_kernel, 1, (char*)"<Item>", NULL},
    {(char*)"/_Windows/hide oscillo kernel", NULL, show_oscillo_kernel, 0, (char*)"<Item>", NULL},

    {(char*)"/_Help", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/Help/About", NULL, about, 0, (char*)"<CheckItem>", NULL},

    {(char*)"/_Quit", NULL, file_quit_cmd_callback, 0, (char*)"<StockItem>", GTK_STOCK_QUIT},

};

static gint nmenu_items = sizeof(menu_items) / sizeof(menu_items[0]);


/* Returns a menubar widget made from the above menu */
GtkWidget *get_menubar_menu(GtkWidget * window)
{
    GtkItemFactory *item_factory;
    GtkAccelGroup *accel_group;
    GtkWidget *menubar;

    /* Make an accelerator group (shortcut keys) */
    accel_group = gtk_accel_group_new();

    /* Make an ItemFactory (that makes a menubar) */
    item_factory =
        gtk_item_factory_new(GTK_TYPE_MENU_BAR, "<main>", accel_group);

    /* This function generates the menu items. Pass the item factory,
       the number of items in the array, the array itself, and any
       callback data for the the menu items. */
    gtk_item_factory_create_items(item_factory, nmenu_items, menu_items,
                                  NULL);

    /* Attach the new accelerator group to the window. */
    gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

    /* Finally, return the actual menu bar created by the item factory. */
    menubar = gtk_item_factory_get_widget(item_factory, "<main>");

    gtk_widget_ref(menubar);
    gtk_object_set_data_full(GTK_OBJECT(window), "menubar", menubar, (GtkDestroyNotify) gtk_widget_unref);

    return menubar;
}

/* Returns a toolbar widget */
GtkWidget *get_toolbar_menu()
{
   GtkWidget *toolbar;
   GtkWidget *button;

   toolbar = gtk_toolbar_new();
   gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS);
   /*    gtk_toolbar_set_icon_size(GTK_TOOLBAR(toolbar),
	 GTK_ICON_SIZE_SMALL_TOOLBAR);*/

   /*------------------*/
#ifdef OSCILLO_KERNEL
   gtk_toolbar_append_space(GTK_TOOLBAR(toolbar));
   button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_PREFERENCES, "Oscillo kernel", "Oscillo kernel", G_CALLBACK(affiche_oscillo_kernel), NULL, -1);
   GTK_WIDGET_UNSET_FLAGS(button, GTK_CAN_FOCUS);
#endif
                           /*------------------*/

   gtk_toolbar_append_space(GTK_TOOLBAR(toolbar));
   button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_GO_FORWARD,"Continue simulation", "Continue simulation", GTK_SIGNAL_FUNC(continue_pressed), NULL, -1);
   GTK_WIDGET_UNSET_FLAGS(button, GTK_CAN_FOCUS);

                            /*------------------*/

   button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_MEDIA_PAUSE,"step by step", "step by step", GTK_SIGNAL_FUNC(step_by_step_pressed), NULL, -1);
   GTK_WIDGET_UNSET_FLAGS(button, GTK_CAN_FOCUS);

                            /*------------------*/

   button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_NO,"Breakpoint", "Breakpoint",GTK_SIGNAL_FUNC(breakpoint_pressed), NULL, -1);
   GTK_WIDGET_UNSET_FLAGS(button, GTK_CAN_FOCUS);
   gtk_toolbar_append_space(GTK_TOOLBAR(toolbar));

                            /*------------------*/

   button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_CANCEL,"Cancel simulation", "Cancel simulation",GTK_SIGNAL_FUNC(cancel_pressed), NULL, -1);
   GTK_WIDGET_UNSET_FLAGS(button, GTK_CAN_FOCUS);
   gtk_toolbar_append_space(GTK_TOOLBAR(toolbar));

   return toolbar;
}


/* Same again but return an option menu */
GtkWidget *get_option_menu(void)
{
    GtkItemFactory *item_factory;
    GtkWidget *option_menu;

    /* Same again, not bothering with the accelerators */
    item_factory = gtk_item_factory_new(GTK_TYPE_OPTION_MENU, "<main>", NULL);
    gtk_item_factory_create_items(item_factory, nmenu_items, menu_items, NULL);
    option_menu = gtk_item_factory_get_widget(item_factory, "<main>");

    return option_menu;
}

static GtkItemFactoryEntry menu_items2[] = {

    {(char*)"/_Inverse video", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/Inverse video/Actived", NULL, check_inverse_video_callback, 1, (char*)"<CheckItem>", NULL},


    {(char*)"/Online group Debug", NULL, ecriture_lenab, 0, (char*)"<Item>", NULL},

    {(char*)"/_Affiche neurone", NULL, affiche_neuroneb, 0, (char*)"<Item>", NULL},

    {(char*)"/_Debug", NULL, NULL, 0, (char*)"<Branch>", NULL},
    {(char*)"/_Debug/yes normal", NULL, check_debug_link, 1, (char*)"<RadioItem>", NULL},
    {(char*)"/_Debug/yes log", NULL, check_debug_link, 2, (char*)"/Debug/yes normal", NULL},
    {(char*)"/_Debug/no oublier", NULL, check_debug_link, -1, (char*)"/Debug/yes normal", NULL},
    {(char*)"/_Debug/no", NULL, check_debug_link, 0, (char*)"/Debug/yes normal", NULL},

    {(char*)"/_Quit", NULL, file_quit_cmd_callback, 0, (char*)"<StockItem>",   GTK_STOCK_QUIT},


};

static gint nmenu_items2 = sizeof(menu_items2) / sizeof(menu_items2[0]);



/* Returns a menubar widget made from the above menu */
GtkWidget *get_menubar_menu2(GtkWidget * window)
{
    GtkItemFactory *item_factory;
    GtkAccelGroup *accel_group;

    /* Make an accelerator group (shortcut keys) */
    accel_group = gtk_accel_group_new();

    /* Make an ItemFactory (that makes a menubar) */
    item_factory =
        gtk_item_factory_new(GTK_TYPE_MENU_BAR, "<main>", accel_group);

    /* This function generates the menu items. Pass the item factory,
       the number of items in the array, the array itself, and any
       callback data for the the menu items. */
    gtk_item_factory_create_items(item_factory, nmenu_items2, menu_items2,
                                  NULL);

    /* Attach the new accelerator group to the window. */
    gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

    /* Finally, return the actual menu bar created by the item factory. */
    return gtk_item_factory_get_widget(item_factory, "<main>");
}


void create_fenetre(TxDonneesFenetre * fenetre, int index, GtkWidget *menubar, GtkWidget *toolbar)
{
    GtkWidget *vbox1;
    GtkWidget *scrolledwindow1;
    GtkWidget *viewport1;
    char widget_name[256];

    fenetre->region_rectangle = NULL;
    g_signal_connect(GTK_OBJECT(fenetre->window), "destroy",
                     G_CALLBACK(cb_destroy_fenetre), fenetre);

    gtk_widget_set_size_request(fenetre->window, 400, 300);
    gtk_object_set_data(GTK_OBJECT(fenetre->window), fenetre->name,
                        fenetre->window);
    gtk_window_set_title(GTK_WINDOW(fenetre->window), fenetre->name);

    vbox1 = gtk_vbox_new(FALSE, 1);
    gtk_widget_ref(vbox1);
    sprintf(widget_name, "vbox%i", index);
    gtk_object_set_data_full(GTK_OBJECT(fenetre->window), (const char*) widget_name, vbox1,
                             (GtkDestroyNotify) gtk_widget_unref);

    gtk_widget_show(vbox1);
    gtk_container_add(GTK_CONTAINER(fenetre->window), vbox1);

    if (menubar != NULL)
    {
       gtk_box_pack_start(GTK_BOX(vbox1), menubar, FALSE, TRUE, 0);
    }

    if (toolbar != NULL)
    {
       gtk_box_pack_start(GTK_BOX(vbox1), toolbar, FALSE, TRUE, 0);
    }

    /* Pack it all together */
    scrolledwindow1 = gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_ref(scrolledwindow1);
    sprintf(widget_name, "scrolledwindow%i", index);
    gtk_object_set_data_full(GTK_OBJECT(fenetre->window), (const char*) widget_name,
                             scrolledwindow1,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(scrolledwindow1);
    gtk_box_pack_start(GTK_BOX(vbox1), scrolledwindow1, TRUE, TRUE, 0);

    viewport1 = gtk_viewport_new(NULL, NULL);
    gtk_widget_ref(viewport1);
    sprintf(widget_name, "viewport%i", index);
    gtk_object_set_data_full(GTK_OBJECT(fenetre->window), (const char*) widget_name,
                             viewport1, (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(viewport1);
    gtk_container_add(GTK_CONTAINER(scrolledwindow1), viewport1);

    /*********************/
    fenetre->da = gtk_drawing_area_new();
    fenetre->width = taille_max_fenetre_x;
    fenetre->height = taille_max_fenetre_y;
    gtk_widget_set_size_request(fenetre->da, fenetre->width, fenetre->height);
    TxResetDisplayPosition(fenetre);

    fenetre->font =
        gdk_font_load("-*-courier-medium-r-*-*-*-120-*-*-*-*-*-*");

    gtk_widget_ref(fenetre->da);
    sprintf(widget_name, "drawingarea%i", index);
    gtk_object_set_data_full(GTK_OBJECT(fenetre->window), (const char*) widget_name,
                             fenetre->da,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(fenetre->da);
    gtk_container_add(GTK_CONTAINER(viewport1), fenetre->da);

  /***************************************************/
    fenetre->pixmap = NULL;     /* voir configure_event pour l'init et l'affichage */

    fenetre->graphic = NULL;


    fenetre->filew = NULL;
    fenetre->box1 = vbox1;

    g_signal_connect(fenetre->da, "expose_event", G_CALLBACK(kernel_expose_event), fenetre);
    g_signal_connect(fenetre->da, "configure_event",G_CALLBACK(kernel_configure_event), fenetre);
}


/********************************************************************************/

void create_fenetre1()
{
    GtkWidget *menubar;
    GtkWidget *toolbar;

    fenetre1.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    menubar = get_menubar_menu(fenetre1.window);
    toolbar = get_toolbar_menu();

    create_fenetre(&fenetre1, 1, menubar, toolbar);

    /* Signals used to handle backing pixmap */
    g_signal_connect(fenetre1.da, "button_press_event", G_CALLBACK(gestion_debug), NULL);
    g_signal_connect(GTK_OBJECT(fenetre1.window), "destroy", G_CALLBACK(exit), NULL);

    gtk_widget_set_events(fenetre1.da, gtk_widget_get_events(fenetre1.da) | GDK_BUTTON_PRESS_MASK);
}

void create_fenetre2()
{
    GtkWidget *menubar2;

    fenetre2.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    menubar2 = get_menubar_menu2(fenetre2.window);
    create_fenetre(&fenetre2, 2, menubar2, NULL);
}

/****************************************************************************************/
/*CREATION DE L'IMAGE1                                                                  */
/****************************************************************************************/


void create_fenetre3()
{
   int res;
   
   image1.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   create_fenetre(&image1, 3, NULL, NULL);

   /* Signals used to handle backing pixmap */
   g_signal_connect (image1.da, "button_press_event",
                     G_CALLBACK(image1_click_event), NULL);
   g_signal_connect (image1.da, "button_release_event",
                     G_CALLBACK(image1_release_event), NULL);
   g_signal_connect (image1.da, "motion_notify_event",
		     G_CALLBACK(image1_motion_event), NULL);
   gtk_widget_set_events(image1.da, gtk_widget_get_events(image1.da)
			 | GDK_LEAVE_NOTIFY_MASK
			 | GDK_BUTTON_PRESS_MASK
			 | GDK_BUTTON_RELEASE_MASK
			 | GDK_POINTER_MOTION_MASK
			 | GDK_POINTER_MOTION_HINT_MASK);
       
      
  res = sem_init(&(image1_obj.sem_fini), 0, 0);
  if (res != 0)
  {
    printf("ERROR: %s res init sem_fini  = %d \n", __FUNCTION__, res);
    exit(EXIT_FAILURE);
  }    
}

/****************************************************************************************/
/* CREATION DE L'IMAGE2                                                                 */
/****************************************************************************************/


void create_fenetre4()
{
   image2.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
   create_fenetre(&image2, 4, NULL, NULL);

   /* Signals used to handle backing pixmap */
   g_signal_connect (image2.da, "button_press_event",
                     G_CALLBACK(image2_click_event), NULL);
   g_signal_connect (image2.da, "button_release_event",
                     G_CALLBACK(image2_release_event), NULL);
   g_signal_connect (image2.da, "motion_notify_event",
		     G_CALLBACK(image2_motion_event), NULL);
   gtk_widget_set_events(image2.da, gtk_widget_get_events(image2.da)
			 | GDK_LEAVE_NOTIFY_MASK
			 | GDK_BUTTON_PRESS_MASK
			 | GDK_BUTTON_RELEASE_MASK
			 | GDK_POINTER_MOTION_MASK
			 | GDK_POINTER_MOTION_HINT_MASK);
}


/*--------------------------------------------------------------------------------*/


TxFloatVuMetre liste_VuMetre[nb_vumetre] = {
    {"eps", &eps, NULL},        /* le 3eme champs correspond au pointeur sur l'objet VuMetre cree apres coup */
    {"vigilance", &vigilence, NULL} /* Il doit donc etre a NULL au depart */
};



void create_range_controls(TxDonneesFenetre * fenetre)
{
    GtkWidget *box2, *box3;
    GtkWidget *button;
    GtkWidget *label;
    GtkWidget *separator, *scrolled_window;
    int i;

    /* Standard window-creating stuff */
    fenetre->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size (GTK_WINDOW (fenetre->window), 200, 400);

    g_signal_connect(GTK_OBJECT(fenetre->window), "destroy",
                     G_CALLBACK(cb_destroy_fenetre), fenetre);
/*    gtk_widget_hide_on_delete(fenetre->window); */
    gtk_window_set_title(GTK_WINDOW(fenetre->window), fenetre->name);


    scrolled_window=gtk_scrolled_window_new (NULL,NULL);
    fenetre->box1 = gtk_vbox_new(FALSE, 0);

    gtk_scrolled_window_add_with_viewport ((GtkScrolledWindow*)scrolled_window, fenetre->box1);
    gtk_container_add(GTK_CONTAINER(fenetre->window), scrolled_window);

 //   gtk_widget_show(hbox);
    gtk_widget_show(fenetre->box1);

/*----------------------------------------------------------------*/
/* on peut avoir plus de 2 VuMetre */
    for (i = 0; i < nb_vumetre; i++)
        TxAddVuMetre(fenetre, &liste_VuMetre[i]);

    /*TxAddVuMetre(fenetre,&liste_VuMetre[0]);
       TxAddVuMetre(fenetre,&liste_VuMetre[1]); */

/*----------------------------------------------------------------*/
    box2 = gtk_vbox_new(FALSE, 10);
    gtk_container_set_border_width(GTK_CONTAINER(box2), 10);
    gtk_box_pack_start(GTK_BOX(fenetre->box1), box2, FALSE, TRUE, 0);
    gtk_widget_show(box2);


/*..... sous boite horizontale ..................*/

    label = gtk_label_new("Tools to debug simulation");
    gtk_box_pack_start(GTK_BOX(box2), label, FALSE, FALSE, 0);
    gtk_widget_show(label);

    box3 = gtk_hbox_new(FALSE, 10);
    gtk_container_set_border_width(GTK_CONTAINER(box3), 10);
    gtk_box_pack_start(GTK_BOX(box2), box3, FALSE, TRUE, 0);
    gtk_widget_show(box3);



    button = gtk_button_new_with_label("Step by Step");
    g_signal_connect(GTK_OBJECT(button), "clicked",
                     G_CALLBACK(step_by_step_pressed), fenetre);
    gtk_box_pack_start(GTK_BOX(box3), button, TRUE, TRUE, 0);
    GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default(button);
    gtk_widget_show(button);

    button = gtk_button_new_with_label("Continue");
    g_signal_connect(GTK_OBJECT(button), "clicked",
                     G_CALLBACK(continue_pressed), fenetre);
    gtk_box_pack_start(GTK_BOX(box3), button, TRUE, TRUE, 0);
    GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default(button);
    gtk_widget_show(button);

    button = gtk_button_new_with_label("Abort Simul.");
    g_signal_connect(GTK_OBJECT(button), "clicked",
                     G_CALLBACK(cancel_pressed), fenetre);
    gtk_box_pack_start(GTK_BOX(box3), button, TRUE, TRUE, 0);
    GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default(button);
    gtk_widget_show(button);

    button = gtk_button_new_with_label("Stack threads");
    g_signal_connect(GTK_OBJECT(button), "clicked",
                     G_CALLBACK(stack_threads), fenetre);
    gtk_box_pack_start(GTK_BOX(box3), button, TRUE, TRUE, 0);
    GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default(button);
    gtk_widget_show(button);


/* .................. */

    separator = gtk_hseparator_new();
    gtk_box_pack_start(GTK_BOX(box2), separator, FALSE, TRUE, 0);
    gtk_widget_show(separator);

    button = gtk_button_new_with_label("Close");
    g_signal_connect(GTK_OBJECT(button), "clicked",
                     G_CALLBACK(cb_hide_fenetre), fenetre);
    gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 0);
    GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
    gtk_widget_grab_default(button);
    gtk_widget_show(button);
}
