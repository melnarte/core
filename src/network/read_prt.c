/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>


#include <net_message_debug_dist.h> /* Fourni par libkernel */

#include "read_prt.h"

/** Stock la prochaine ligne du fichier prt dans line */
static int read_line(FILE *f_prt, char *line)
{
	return (fgets(line, LINE_LIMIT, f_prt) == line);
}

/** Copie src dans dest en majuscule */
static void str_upper(const char *src, char *dest)
{
	int i;
	
	for (i = 0; (unsigned int) i < strlen(src); i++)
		dest[i] = toupper(src[i]);
	dest[i] = '\0';
}

/** retourne le numero d'hote associe a name si il existe sinon retourne -1 */
static int find_host_by_name(const char *name, prt_t *prt)
{
	int i;

	for (i = 0; (unsigned int) i < prt->num_hosts; i++)
		if (!strcmp(name, prt->hosts[i].vname))
			return (i);

	return -1;
}

/** retourne le numero de lien de nom name si il existe sinon retourne -1 */
static int find_link_by_name(const char *name, prt_t *prt)
{
	int i;

	for (i = 0; (unsigned int)i < prt->num_links; i++)
		if(!strcmp(name, prt->links[i].name))
			return (i);

	return -1;
}

prt_t* prt_init(const char* file_name, const char* virtual_name)
{
	
	FILE *f_prt;								/* fichier .prt */
	prt_section_t section;						/* utilisée pour mémoriser la section du fichier prt dans laquelle on se trouve */
	char line[LINE_LIMIT];						/* stock la dernière ligne lue dans le fichier prt */
	char tokens[LINE_TOKENS][TOKEN_LIMIT];		/* stock les mots de la derniere ligne du fichier */
	char upper_tokens[LINE_TOKENS][TOKEN_LIMIT];/* stock les mots de la derniere ligne du fichieren caractere majuscule */ 
	int nb_token;								/* stock le nombre de mot de la derniere ligne du fichier prt */
	int i;										/* itération */
	int source;									/* pour stocker la source d'un lien (numero d'hote) */
	int target;									/* pour stocker la destination d'un lien (numero d'hote) */
	prt_host_t *host_realloc;					/* utilisée pour la réallocation du tableau hosts */
	prt_link_t *link_realloc;					/* utilisée pour la réallocation du tableau links */
	prt_t *prt;

	/* Ouverture du fichier prt, on retourne NULL si échec */
	if( !(f_prt = fopen(file_name, "r")))
	{
		/* Il n'y a pas de prt*/
		return NULL;
	}

	/* on initialise la structure prt_t */
	if ((prt = malloc(sizeof(prt_t))) == NULL)
	  {
	    perror("init_prt : malloc");
	    exit(2);
	  }
	memset(prt, 0, sizeof(prt_t));
	prt->hosts = NULL;
	prt->num_hosts = 0;
	prt->my_host = -1;
	prt->links = NULL;
	prt->num_links = 0;
	
	section = NO_SECTION;
	
	/* On parcours le fichier prt ligne par ligne */
	while(read_line(f_prt, line))
	{
		nb_token = sscanf(line, "%s %s %s %s", tokens[0], tokens[1], tokens[2], tokens[3]);
		
		/* Si la ligne est une ligne de commentaire on ne la traite pas*/
		if(tokens[0][0] == '#')
			continue;

		/* On converti les mots de la ligne en majuscule pour les comparaison */
		for(i = 0; i < nb_token; i++)
			str_upper(tokens[i], upper_tokens[i]);

		switch(nb_token)
		{
			case 0:	/* Ligne vide */
				break;
			case 1:	/* 1 mot : on verifie s'il s'agit de la fin d'une section sinon on ne traite pas */
				if(!strcmp(END_TOKEN, upper_tokens[0]))
					section = NO_SECTION;
				break;
			case 2:	/* 2 mots : on verifie si on est au début d'une section */
				if(!strcmp(BEGIN_TOKEN, upper_tokens[0]))
				{
					if(!strcmp(NETWORK_TOKEN, upper_tokens[1]))
					{
						section = NETWORK_SECTION;
					}
					else if(!strcmp(LINK_TOKEN, upper_tokens[1]))
					{
						section = LINK_SECTION;
					}
				}
				break;
			case 3:
				if((section == NETWORK_SECTION) && ((source = find_host_by_name(tokens[0], prt)) < 0))
				{
					/* Ajout d'un hote */
					if(prt->hosts == NULL)
					{
					  if ((prt->hosts = malloc(sizeof(prt_host_t))) == NULL)
					    {
					      perror("prt_init : malloc");
					      exit(2);
					    }
					  memset(prt->hosts, 0, sizeof(prt_host_t));
					}
					else
					{
						host_realloc = realloc(prt->hosts, sizeof(prt_host_t) * (prt->num_hosts + 1));
						if(host_realloc)
						{
							prt->hosts = host_realloc;
						}
						else
						{
							perror("realloc");
							exit(EXIT_FAILURE);
						}
					}
/* 					if ((prt->hosts[prt->num_hosts].vname = malloc(sizeof(char) * (strlen(tokens[0]) + 1))) == NULL) */
/* 					  { */
/* 					    perror("init_prt : malloc"); */
/* 					    exit(2); */
/* 					  } */
/* 					if ((prt->hosts[prt->num_hosts].name = malloc(sizeof(char) * (strlen(tokens[1]) + 1))) == NULL) */
/* 					  { */
/* 					    perror("init_prt : malloc"); */
/* 					    exit(2); */
/* 					  } */
					memset(prt->hosts[prt->num_hosts].vname, 0, HOSTS_LIMIT);
					memcpy(prt->hosts[prt->num_hosts].vname, tokens[0], strlen(tokens[0]) + 1);

					memset(prt->hosts[prt->num_hosts].name, 0, HOSTS_LIMIT);
					memcpy(prt->hosts[prt->num_hosts].name, tokens[1], strlen(tokens[1]) + 1);

                    			prt->hosts[prt->num_hosts].port = atoi(tokens[2]);
					if(!strcmp(tokens[0], virtual_name))
						prt->my_host = prt->num_hosts;
					prt->num_hosts++;
				}
				else if(section == LINK_SECTION)
				{
					/* On vérifie que les hote concerné par le lien existe et que le lien n'est pas déja défini */
					if(find_link_by_name(tokens[0], prt) < 0 && (source = find_host_by_name(tokens[1], prt)) >= 0 && (target = find_host_by_name(tokens[2], prt)) >= 0)
					{
						/* On ajoute le lien seulement si on est soit la source soit la destination */
						if(!(strcmp(tokens[1], virtual_name)) || !(strcmp(tokens[2], virtual_name)))
						{
							/* Ajout du lien */
							if(prt->links == NULL)
							{
							  if ((prt->links = malloc(sizeof(prt_link_t))) == NULL)
							    {
							      perror("init_prt : malloc");
							      exit(2);
							    }

							}
							else
							{
								link_realloc = realloc(prt->links, sizeof(prt_link_t) * (prt->num_links + 1));
								if(link_realloc)
								{
									prt->links = link_realloc;
								}
								else
								{
									perror("realloc");
									exit(EXIT_FAILURE);
								}
							}
/* 							if ((prt->links[prt->num_links].name = malloc(sizeof(char) * (strlen(tokens[0]) + 1))) == NULL) */
/* 							  { */
/* 							    perror("init_prt : malloc"); */
/* 							    exit(2); */
/* 							  } */

							memset(prt->links[prt->num_links].name, 0, LINKS_LIMIT);
							memset(prt->links[prt->num_links].name, 0, LINKS_LIMIT);
							memcpy(prt->links[prt->num_links].name, tokens[0], strlen(tokens[0]) + 1);

							prt->links[prt->num_links].source = source;
							prt->links[prt->num_links].target = target;
							prt->links[prt->num_links].protocol = P_TCP;
							prt->num_links++;
						}
					}
				}
				break;
			case 4:
				if(section == LINK_SECTION)
				{
					/* On vérifie que les hote concerné par le lien existe et que le lien n'est pas déja défini */
					if(find_link_by_name(tokens[0], prt) < 0 && (source = find_host_by_name(tokens[1], prt)) >= 0 && (target = find_host_by_name(tokens[2], prt)) >= 0)
					{
						/* On ajoute le lien seulement si on est soit la source soit la destination */
						if(!(strcmp(tokens[1], virtual_name)) || !(strcmp(tokens[2], virtual_name)))
						{
							/* Ajout du lien */
							if(prt->links == NULL)
							{
							  if ((prt->links = malloc(sizeof(prt_link_t))) == NULL)
							    {
							      perror("init_prt : malloc");
							      exit(2);
							    }

							}
							else
							{
								link_realloc = realloc(prt->links, sizeof(prt_link_t) * (prt->num_links + 1));
								if(link_realloc)
								{
									prt->links = link_realloc;
								}
								else
								{
									perror("realloc");
									exit(EXIT_FAILURE);
								}
							}
/* 							if ((prt->links[prt->num_links].name = malloc(sizeof(char) * (strlen(tokens[0]) + 1))) == NULL) */
/* 							    { */
/* 							      perror("init_prt : malloc"); */
/* 							      exit(2); */
/* 							    } */
							
							memset(prt->links[prt->num_links].name, 0, LINKS_LIMIT);
							memcpy(prt->links[prt->num_links].name, tokens[0], strlen(tokens[0]) + 1);

							prt->links[prt->num_links].source = source;
				    			prt->links[prt->num_links].target = target;

							if(!strcmp(upper_tokens[3], LINK_TCP))
							{
								prt->links[prt->num_links].protocol = P_TCP;
							}
							else if(!strcmp(upper_tokens[3], LINK_UDP))
							{
								prt->links[prt->num_links].protocol = P_UDP;
							}
							else if(!strcmp(upper_tokens[3], LINK_IVY))
							{
								prt->links[prt->num_links].protocol = P_IVY;
							}
							else
							{
								prt->links[prt->num_links].protocol = P_TCP;
							}
			
							prt->num_links++;
						}
					}
				}
		}
	
	}

	if(prt->my_host < 0) EXIT_ON_ERROR("Virtual %s name not found in file %s\n", virtual_name, file_name);

	return (prt);
}

void prt_free(prt_t *prt)
{
	int i;

	if(prt)
	{
	  for(i = 0; (unsigned int)i < prt->num_hosts; i++)
		{
			free(prt->hosts[i].vname);
			free(prt->hosts[i].name);
		}

	  for(i = 0; (unsigned int)i < prt->num_links; i++)
		{
			free(prt->links[i].name);
		}

		free(prt->hosts);
		free(prt->links);

		free(prt);
	}
}




