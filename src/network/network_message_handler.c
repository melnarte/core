/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#define DEBUG
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <semaphore.h>

#include "global.h"
#include "consts.h"
#include "network_message.h"
#include "network_message_handler.h"
#include "network_link.h"
#include "virtual_link.h"
#include "libcomm_types.h"
#include "debug_network.h"

#include "Ivy/ivy.h"

/**
 *
 *	network_message_handler.c
 *
 *	handler pour construire et lire les differents types de messages
 *
 *	Construit et lit les messages reseau selon le protocol :
 *	---  size  ---  flag  ---  id  ---  nom lien  ---  timestamp  --- donnees  ---
 *
 **/



/*
  Construction d'un message classique
*/
int msg_handler_tx(network_link_t *netlink, virtual_link_t* vlink)
{

  unsigned int msg_size;
  char* pt;
  struct timeval tv;
  long_64_t nbsec;

  /* ALLOCATION MESSAGE */

  /* On calcul la taille du message */
  msg_size = netlink->size_netmsg + vlink->size_data;

  /* Allocation si besion */
  if(!netlink->message_tx)
  {
    netlink->message_tx = malloc(msg_size);
    if(!netlink->message_tx)
    {
      perror("libcomm : msg_handler_tx() : malloc()");
      exit(EXIT_FAILURE);
    }
    memset(netlink->message_tx, 0, msg_size);
    netlink->size_max_msg_tx = msg_size;
  }
  else if(msg_size > netlink->size_max_msg_tx)
  {
    netlink->message_tx = realloc(netlink->message_tx, msg_size);
    if(!netlink->message_tx)
    {
      perror("libcomm : msg_handler_tx() : realloc()");
      exit(EXIT_FAILURE);
    }
    netlink->size_max_msg_tx = msg_size;
  }

  /* REMPLISSAGE MESSAGE */

  /* On met le pointeur au debut du message */
  pt = netlink->message_tx;

  /* On met la taille du message dans le message */
  *(unsigned int*) pt = msg_size;
  pt += sizeof(unsigned int);

  /* On met le flag */
  *(unsigned int*) pt = MSG;
  pt += sizeof(unsigned int);

  /* On met l'id du message dans le message */
  *(unsigned int*) pt = netlink->id_tx;
  pt += sizeof(unsigned int);

  /* On met le timestamp du message dans le message */
  if(gettimeofday(&tv, NULL) < 0)
  {
    perror("libcomm : msg_handler_tx() : gettimeofday()");
    exit(EXIT_FAILURE);
  }

  nbsec = (long_64_t)tv.tv_sec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  nbsec = (long_64_t)tv.tv_usec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  /* On met le nom du lien dans le message */
  memcpy(pt, vlink->name, STR_LIMIT);
  pt += (STR_LIMIT * sizeof(char));

  /* On met les donnees dans le message */
  memcpy(pt, vlink->data, vlink->size_data);
  pt += vlink->size_data;

  return (RET_OK);
}




/*
  Construction d'un accuse de reception
*/
int msg_ack_handler_tx(network_link_t *netlink, virtual_link_t* vlink)
{
  unsigned int msg_size;
  char* pt;
  struct timeval tv;
  long_64_t nbsec;

  /* ALLOCATION MESSAGE */

  /* On calcul la taille du message */
  msg_size = netlink->size_netmsg;

  /* Allocation si besion */
  if(!netlink->message_tx)
  {
    netlink->message_tx = (char *)malloc(msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : malloc()");
      exit(EXIT_FAILURE);
    }
    memset(netlink->message_tx, 0, msg_size);
    netlink->size_max_msg_tx = msg_size;
  }
  else if(msg_size > netlink->size_max_msg_tx)
  {
    netlink->message_tx = realloc(netlink->message_tx, msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : realloc()");
      exit(EXIT_FAILURE);
    }
    netlink->size_max_msg_tx = msg_size;
  }

  /* REMPLISSAGE MESSAGE */

  /* On met le pointeur au debut du message */
  pt = netlink->message_tx;

  /* On met la taille du message dans le message */
  *(unsigned int*) pt = msg_size;
  pt += sizeof(unsigned int);

  /* On met le flag */
  *(unsigned int*) pt = MSG_ACK;
  pt += sizeof(unsigned int);

  /* On met l'id du message dans le message */
  *(unsigned int*) pt = netlink->id_tx;
  pt += sizeof(unsigned int);

  /* On met le timestamp du message dans le message */
  if(gettimeofday(&tv, NULL) < 0)
  {
    perror("tcp_message_send : gettimeofday");
    exit(EXIT_FAILURE);
  }

  nbsec = (long_64_t)tv.tv_sec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  nbsec = (long_64_t)tv.tv_usec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  /* On met le nom du lien dans le message */
  memcpy(pt, vlink->name, STR_LIMIT);
  pt += (STR_LIMIT * sizeof(char));

  return (RET_OK);

}



/*
  Construction d'un message de demande de connection
*/
int msg_connect_handler_tx(network_link_t *netlink, virtual_link_t* vlink)
{
  unsigned int msg_size;
  char* pt;
  struct timeval tv;
  long_64_t nbsec;

  if(!netlink || vlink)
    return (RET_ERR);

  /* ALLOCATION MESSAGE */

  /* On calcul la taille du message */
  msg_size = netlink->size_netmsg;

  /* Allocation si besion */
  if(!netlink->message_tx)
  {
    netlink->message_tx = (char *)malloc(msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : malloc()");
      exit(EXIT_FAILURE);
    }
    memset(netlink->message_tx, 0, msg_size);
    netlink->size_max_msg_tx = msg_size;
  }
  else if(msg_size > netlink->size_max_msg_tx)
  {
    netlink->message_tx = (char *)realloc(netlink->message_tx, msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : realloc()");
      exit(EXIT_FAILURE);
    }
    netlink->size_max_msg_tx = msg_size;
  }

  /* REMPLISSAGE MESSAGE */

  /* On met le pointeur au debut du message */
  pt = netlink->message_tx;

  /* On met la taille du message dans le message */
  *(unsigned int*) pt = msg_size;
  pt += sizeof(unsigned int);

  /* On met le flag */
  *(unsigned int*) pt = MSG_CONNECT;
  pt += sizeof(unsigned int);

  /* On met l'id du message dans le message */
  *(unsigned int*) pt = netlink->id_tx;
  pt += sizeof(unsigned int);

  /* On met le timestamp du message dans le message */
  if(gettimeofday(&tv, NULL) < 0)
  {
    perror("tcp_message_send : gettimeofday");
    exit(EXIT_FAILURE);
  }

  nbsec = (long_64_t)tv.tv_sec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  nbsec = (long_64_t)tv.tv_usec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  /* On met notre nom dans le message */
  memcpy(pt, my_name, STR_LIMIT);
  pt += (STR_LIMIT * sizeof(char));

  return (RET_OK);
}



/*
  Construction d'un message de confirmation de connection
*/
int msg_connect_ok_handler_tx(network_link_t *netlink, virtual_link_t* vlink)
{
  unsigned int msg_size;
  char* pt;
  struct timeval tv;
  long_64_t nbsec;
  (void)vlink;

  /* ALLOCATION MESSAGE */

  /* On calcul la taille du message */
  msg_size = netlink->size_netmsg;

  /* Allocation si besion */
  if(!netlink->message_tx)
  {
    netlink->message_tx = (char *)malloc(msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : malloc()");
      exit(EXIT_FAILURE);
    }
    memset(netlink->message_tx, 0, msg_size);
    netlink->size_max_msg_tx = msg_size;
  }
  else if(msg_size > netlink->size_max_msg_tx)
  {
    netlink->message_tx = realloc(netlink->message_tx, msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : realloc()");
      exit(EXIT_FAILURE);
    }
    netlink->size_max_msg_tx = msg_size;
  }

  /* REMPLISSAGE MESSAGE */

  /* On met le pointeur au debut du message */
  pt = netlink->message_tx;

  /* On met la taille du message dans le message */
  *(unsigned int*) pt = msg_size;
  pt += sizeof(unsigned int);

  /* On met le flag */
  *(unsigned int*) pt = MSG_CONNECT_OK;
  pt += sizeof(unsigned int);

  /* On met l'id du message dans le message */
  *(unsigned int*) pt = netlink->id_tx;
  pt += sizeof(unsigned int);

  /* On met le timestamp du message dans le message */
  if(gettimeofday(&tv, NULL) < 0)
  {
    perror("tcp_message_send : gettimeofday");
    exit(EXIT_FAILURE);
  }

  nbsec = (long_64_t)tv.tv_sec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  nbsec = (long_64_t)tv.tv_usec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  /* On met notre nom dans le message */
  memcpy(pt, my_name, STR_LIMIT);
  pt += (STR_LIMIT * sizeof(char));

  return (RET_OK);
}



/*
  Construction d'un message pour refuser la connection
*/
int msg_connect_refused_handler_tx(network_link_t *netlink, virtual_link_t* vlink)
{
  unsigned int msg_size;
  char* pt;
  struct timeval tv;
  long_64_t nbsec;
  (void)vlink;

  /* ALLOCATION MESSAGE */

  /* On calcul la taille du message */
  msg_size = netlink->size_netmsg;

  /* Allocation si besion */
  if(!netlink->message_tx)
  {
    netlink->message_tx = (char *)malloc(msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : malloc()");
      exit(EXIT_FAILURE);
    }
    memset(netlink->message_tx, 0, msg_size);
    netlink->size_max_msg_tx = msg_size;
  }
  else if(msg_size > netlink->size_max_msg_tx)
  {
    netlink->message_tx = realloc(netlink->message_tx, msg_size);
    if(!netlink->message_tx)
    {
      perror("network_message_build : realloc()");
      exit(EXIT_FAILURE);
    }
    netlink->size_max_msg_tx = msg_size;
  }

  /* REMPLISSAGE MESSAGE */

  /* On met le pointeur au debut du message */
  pt = netlink->message_tx;

  /* On met la taille du message dans le message */
  *(unsigned int*) pt = msg_size;
  pt += sizeof(unsigned int);

  /* On met le flag */
  *(unsigned int*) pt = MSG_CONNECT_REFUSED;
  pt += sizeof(unsigned int);

  /* On met l'id du message dans le message */
  *(unsigned int*) pt = netlink->id_tx;
  pt += sizeof(unsigned int);

  /* On met le timestamp du message dans le message */
  if(gettimeofday(&tv, NULL) < 0)
  {
    perror("tcp_message_send : gettimeofday");
    exit(EXIT_FAILURE);
  }

  nbsec = (long_64_t)tv.tv_sec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  nbsec = (long_64_t)tv.tv_usec;
  *(long_64_t*) pt = nbsec;
  pt += sizeof(long_64_t);

  /* On met notre nom dans le message */
  memcpy(pt, netlink->name, STR_LIMIT);
  pt += (STR_LIMIT * sizeof(char));

  return (RET_OK);
}


/*********************************************************************************************************/
/*********************************************************************************************************/
/*********************************************************************************************************/
/*********************************************************************************************************/

/*
  traitement message normal
*/
int msg_handler_rx(network_link_t *netlink)
{
  unsigned int size, size_tmp;
  unsigned int id, type;
  long_64_t sec;
  long_64_t usec;
  char name[STR_LIMIT];
  unsigned int flag;
  char* pt;
  int num_link;
  unsigned int data_size;
  int retval;
  int sval;
  virtual_link_t *vlink;

  /* LECTURE INFO */

  pt = netlink->message_rx;

  /* On recupere la taille du message */
  size = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupere le flag du message */
  flag = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupere l'id du message */
  id = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  if (id < netlink->id_rx)
  {
    PRINT_WARNING("msg_handler_rx : un message provenant du lien %s sur la machine %s avec le meme identifiant a deja ete recu precedemment : id recu = %i, id attendu = %i\n", netlink->name, netlink->ip, id, netlink->id_rx);
  }
  else if (id > netlink->id_rx)
  {
    PRINT_WARNING("msg_handler_rx : un message provenant du lien %s sur la machine %s a du etre perdu, car l'identifiant recu est %i alors qu'on attentendai %i\n", netlink->name, netlink->ip, id, netlink->id_rx);
  }

  /* On recupere les deux valeurs du timestamp */
  sec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

  usec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

  /* On recupere le nom du lien virtuel */
  memcpy(name, pt, STR_LIMIT * sizeof(char));
  pt += (STR_LIMIT * sizeof(char));

  /* VERIFFFFF */
  num_link = virtual_link_find(name);


  if(num_link < 0)
    return RET_ERR;

  /* TODO: VERIFFFFF SUR ID ET TIMESTAMP */

#ifdef DEBUG_NETWORK
  /** update debug network struct */
  debug_network_update_at_reception(&vlinks[num_link].debug, sec, usec,(int)size);
#endif

  /** Copie des donnees dans le lien virtuel */
  if ((pthread_mutex_lock(&vlinks[num_link].mut_data)) != 0) EXIT_ON_SYSTEM_ERROR("f_recv : read_data() : pthread_lock()");
  data_size = size - netlink->size_netmsg;

  if(!vlinks[num_link].data)
  {
    vlinks[num_link].data = malloc(data_size);
    if(!vlinks[num_link].data) EXIT_ON_SYSTEM_ERROR("message_handler_rx : malloc()");

    memset(vlinks[num_link].data, 0, data_size);
    vlinks[num_link].size_max_data = data_size;
  }
  else if(data_size > vlinks[num_link].size_max_data)
  {
    vlinks[num_link].data = realloc(vlinks[num_link].data, data_size);
    if(!vlinks[num_link].data) EXIT_ON_SYSTEM_ERROR("message_handler_rx : realloc()");
    vlinks[num_link].size_max_data = data_size;
  }
  vlinks[num_link].size_data = data_size;

  memcpy(vlinks[num_link].data, pt, data_size);


  /** mise a jour structure debug cas ext.  PAS OPTIMAL - on fait les
      calculs meme si debug desactive... necessaire si on ajoute la
      gestion separee pour les images.*/
  vlink = &vlinks[num_link];
  /**   verification du flag du message*/
  pt = vlink->data;
  flag = *(unsigned int *) pt;
  if(flag == DATA_IMG || flag == DATA_ACK_IMG) {

#ifdef DEBUG_NETWORK
    /** if img was recved and if debug is activated */
    /** activate special management in debug_network */
    vlink->debug.case_ext=1;
#endif

    /** On met le pointeur apres le flag */
    pt += sizeof(unsigned int);
    while (data_size>0) /** tant qu'il reste des donnees a lire*/
    {
      size_tmp = *(unsigned int *) pt;
      pt += sizeof(unsigned int);
      
      type = *(unsigned int *) pt;
      pt += sizeof(unsigned int);
      
      if (type == IMAGE) 
      {	/** donnees de type IMAGE trouvees */

	/** Remplissage de la structure data_ext si utilisation definie */
	/** DESACTIVE : (cf. recv_server_init_ext_management) */
	/** Principe : copie des images dans une structure separee
	    pour eviter de perdre une image lorsque qu'un message recu
	    qui ne contient pas d'image vienne ecraser un message non
	    lu qui contenait une image. Necessite gestion particuliere
	    dans f_recv*/
	/** BESOIN DOIT ETRE VALIDE AVANT MISE EN PLACE DEFINITIVE.*/
	if(vlink->read_data_ext!=-1) {   
	  /** extract data to data_ext of virtual link */
	  data_size = size_tmp;	
	  if(!vlinks[num_link].data_ext)
	  {
	    vlinks[num_link].data_ext = malloc(data_size);
	    if(!vlinks[num_link].data_ext) EXIT_ON_SYSTEM_ERROR("message_handler_rx : malloc()");
	    
	    memset(vlinks[num_link].data_ext, 0, data_size);
	    vlinks[num_link].size_max_data_ext = data_size;
	  }
	  else if(data_size > vlinks[num_link].size_max_data_ext)
	  {
	    vlinks[num_link].data_ext = realloc(vlinks[num_link].data_ext, data_size);
	    if(!vlinks[num_link].data_ext) EXIT_ON_SYSTEM_ERROR("message_handler_rx : realloc()");
	    vlinks[num_link].size_max_data_ext = data_size;
	  }
	  vlinks[num_link].size_data_ext = data_size;
	  
	  /** copy without size and type */
	  memcpy(vlinks[num_link].data_ext, pt-2*sizeof(unsigned int), data_size);
	  
	  /** On indique que le message n'est pas lu */
	  vlinks[num_link].read_data_ext = 0;
	  
	} /* fin remplissage structure data_ext */

	/** reduction taille restante - exit loop */
	data_size=0;	
	
#ifdef DEBUG_NETWORK
	/** update debug network struct - case ext */
	debug_network_update_at_reception_ext(&vlinks[num_link].debug, sec, usec,(int)size_tmp);
#endif
      }
      else
      {
	/** deplacement pointeur - reduction taille restante */
	pt += (size_tmp - (2 * sizeof(unsigned int)));
	data_size-=size_tmp;
      }
    }
  }

  /* On indique que le message n'est pas lu */
  vlinks[num_link].read_data = 0;

  if ((errno = pthread_mutex_unlock(&vlinks[num_link].mut_data)) != 0) EXIT_ON_SYSTEM_ERROR("f_recv : read_data() : pthread_unlock()");

  /* On indique la presense d'un nouveau message */
  retval = sem_getvalue(&vlinks[num_link].new_message, &sval);

  if(sval == 0)
  {
    retval = sem_post(&vlinks[num_link].new_message);
    if(retval < 0) EXIT_ON_SYSTEM_ERROR("libcomm : msg_handler_rx() : sem_post()");
  }
  return (RET_OK);
}



/*
  traitement accuse de reception
*/
int msg_ack_handler_rx(network_link_t *netlink)
{

//  unsigned int size;
  unsigned int id;
  char name[STR_LIMIT];
 // unsigned int flag;
  char* pt;
  int num_link;
  int retval;
  int sval;

  /* LECTURE INFO */

  pt = netlink->message_rx;

  /* On recupere la taille du message */
//  size = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupere le flag du message */
 // flag = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupère l'id du message */
  id = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  if (id < netlink->id_rx) PRINT_WARNING("msg_ack_handler_rx : un message provenant du lien %s sur la machine %s avec le meme identifiant a deja ete recu precedemment : id recu = %i, id attendu = %i\n", netlink->name, netlink->ip, id, netlink->id_rx);
  else if (id > netlink->id_rx)  PRINT_WARNING("msg_ack_handler_rx : un message provenant du lien %s sur la machine %s a du etre perdu, car l'identifiant recu est %i alors qu'on attentendai %i\n", netlink->name, netlink->ip, id, netlink->id_rx);

  /* On recupere les deux valeurs du timestamp */
 // sec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

 // usec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

  /* On recupere le nom du lien virtuel */
  memcpy(name, pt, STR_LIMIT * sizeof(char));
  pt += (STR_LIMIT * sizeof(char));

  /* VERIFFFFF */
  num_link = virtual_link_find(name);


  if(num_link < 0)
    return RET_ERR;

  /* On indique la reception de l accuse */
  retval = sem_getvalue(&vlinks[num_link].wait_ack, &sval);

  if(sval == 0)
  {
    retval = sem_post(&vlinks[num_link].wait_ack);
    if(retval < 0) EXIT_ON_SYSTEM_ERROR("libcomm : msg_ack_handler_rx() : sem_post()");
  }
  return (RET_OK);
}



/*
  traitement message de connection effectue par la partie serveur
*/
int msg_connect_handler_rx(network_link_t *netlink)
{
//  unsigned int size;
  unsigned int id;
/*  long_64_t sec;
  long_64_t usec;*/
  char name[STR_LIMIT];
//  unsigned int flag;
  char* pt;
  int num_link;

  /* LECTURE INFO */

  pt = netlink->message_rx;

  /* On recupere la taille du message */
 // size = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupere le flag du message */
//  flag = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupere l'id du message */
  id = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  if (id < netlink->id_rx) PRINT_WARNING("msg_connect_handler_rx : un message provenant du lien %s sur la machine %s avec le meme identifiant a deja ete recu precedemment : id recu = %i, id attendu = %i\n", netlink->name, netlink->ip, id, netlink->id_rx);
  else if (id > netlink->id_rx) PRINT_WARNING("msg_connect_handler_rx : un message provenant du lien %s sur la machine %s a du etre perdu, car l'identifiant recu est %i alors qu'on attentendai %i\n", netlink->name, netlink->ip, id, netlink->id_rx);

  /* On recupere les deux valeurs du timestamp */
//  sec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

//  usec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

  /* On recupere le nom du lien virtuel */
  memcpy(name, pt, STR_LIMIT * sizeof(char));
  pt += (STR_LIMIT * sizeof(char));

  /* On verifi si le lien existe et si il n'est pas connecté */
  num_link = network_link_find(name);

  if((num_link >= 0) && (netlinks[num_link].connected == 0))
  {
    /* Initialisation timestamp et id...... */
    /*  */
    return (num_link);
  }
  else
  {
    return (-1);
  }

}



/*
  traitement message de confirmation de connection effectue par la partie client
*/
int msg_connect_ok_handler_rx(network_link_t *netlink)
{
//  unsigned int size;
  unsigned int id;
 /*long_64_t sec;
  long_64_t usec;*/
  char name[STR_LIMIT];
//  unsigned int flag;
  char* pt;
  int num_link;

  /* LECTURE INFO */

  pt = netlink->message_rx;

  /* On recupere la taille du message */
//  size = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupere le flag du message */
 // flag = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  /* On recupère l'id du message */
  id = *(unsigned int*) pt;
  pt += sizeof(unsigned int);

  if (id < netlink->id_rx) PRINT_WARNING("msg_connect_ok_handler_rx : un message provenant du lien %s sur la machine %s avec le meme identifiant a deja ete recu precedemment : id recu = %i, id attendu = %i\n", netlink->name, netlink->ip, id, netlink->id_rx);
  else if (id > netlink->id_rx) PRINT_WARNING("msg_connect_ok_handler_rx : un message provenant du lien %s sur la machine %s a du etre perdu, car l'identifiant recu est %i alors qu'on attentendai %i\n", netlink->name, netlink->ip, id, netlink->id_rx);

  /* On recupere les deux valeurs du timestamp */
 // sec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

 // usec = *(long_64_t*) pt;
  pt += sizeof(long_64_t);

  /* On recupere le nom du lien virtuel */
  memcpy(name, pt, STR_LIMIT * sizeof(char));
  pt += (STR_LIMIT * sizeof(char));

  /* On verifi si le lien existe et si il n'est pas connecté */
  num_link = network_link_find(name);

  if(num_link >= 0)
  {
    return (RET_CONNECT_OK);
  }
  else
  {
    return (RET_CONNECT_REFUSED);
  }
}



/*
  traitement message de refus de connection effectue par la partie client
*/
int msg_connect_refused_handler_rx(network_link_t *netlink)
{
  (void)netlink;
  return (RET_OK);
}


