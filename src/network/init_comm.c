/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
   \defgroup libcomm Libcomm
   \brief Library for communication between different scripts
*/
/* #define DEBUG */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <net_message_debug_dist.h>

#include "init_comm.h"
#include "read_prt.h"
#include "network_link.h"
#include "virtual_link.h"
#include "consts.h"
#include "global.h"

network_link_t *netlinks;
unsigned int nb_netlink;

virtual_link_t *vlinks;
unsigned int nb_vlink;

char my_name[STR_LIMIT];

network_link_t *tcp_server;
network_link_t *udp_server;

/*
	Initialisation des connexion reseau
*/
void init_comm(prt_t *prt)
{
	int i;
	int numlink = 0;

	dprints("init comm\n");

	if(!prt)
	{
		return ;
	}

	netlinks = NULL;
	nb_netlink = 0;

	/* On initialise le nom de notre hote */
	if(strlen(prt->hosts[prt->my_host].vname) < STR_LIMIT)
	  {
	    memset(my_name, 0, STR_LIMIT);
	    memcpy(my_name, prt->hosts[prt->my_host].vname, strlen(prt->hosts[prt->my_host].vname) + 1);
	  }
	else EXIT_ON_ERROR("Nom d'hote superieur a %d caracteres!\n", STR_LIMIT);

	/* initialisation du tableau de virtual link */
	nb_vlink = prt->num_links;

	vlinks = malloc(sizeof(virtual_link_t) * nb_vlink);
	if(!vlinks)
	{
		perror("libcomm : init_comm() : malloc()");
		exit(EXIT_FAILURE);
	}
	memset(vlinks, 0, sizeof(virtual_link_t) * nb_vlink);
	/* Initialisation des network_link et des virtual_link */
	for(i = 0; (unsigned int)i < prt->num_links; i++)
	{
		/*TX_LINK*/
		if(!strcmp(prt->hosts[prt->links[i].source].vname , prt->hosts[prt->my_host].vname))
		{
			/* Initialisation du network link */
			if((numlink = network_link_find(prt->hosts[prt->links[i].target].vname)) < 0)
			{
				if(!netlinks)
				{
					netlinks = malloc(sizeof(network_link_t) * (nb_netlink + 1));
					if(!netlinks)
					{
						perror("libcomm : init_comm() : malloc()");
						exit(EXIT_FAILURE);
					}
					memset(netlinks, 0, sizeof(network_link_t) * (nb_netlink + 1));
				}
				else
				{
					netlinks = realloc(netlinks, sizeof(network_link_t) * (nb_netlink + 1));
					if(!netlinks)
					{
						perror("libcomm : init_comm() : malloc()");
						exit(EXIT_FAILURE);
					}
				}
				network_link_init(&netlinks[nb_netlink], prt->hosts[prt->links[i].target].vname, prt->hosts[prt->links[i].target].name ,
									prt->hosts[prt->links[i].target].port, prt->links[i].protocol);
				numlink = nb_netlink;
				nb_netlink++;
			}

			/* Initialisation du virtual link */
			virtual_link_init(&vlinks[i], prt->links[i].name, numlink, TX);
		}
		/*RX_LINK*/
		else if(!strcmp(prt->hosts[prt->links[i].target].vname , prt->hosts[prt->my_host].vname))
		{
			/* Initialisation du network link */
			if((numlink = network_link_find(prt->hosts[prt->links[i].source].vname)) < 0)
			{
				if(!netlinks)
				{
					netlinks = malloc(sizeof(network_link_t) * (nb_netlink + 1));
					if(!netlinks)
					{
						perror("libcomm : init_comm() : malloc()");
						exit(EXIT_FAILURE);
					}
					memset(netlinks, 0, sizeof(network_link_t) * (nb_netlink + 1));
				}
				else
				{
					netlinks = realloc(netlinks, sizeof(network_link_t) * (nb_netlink + 1));
					if(!netlinks)
					{
						perror("libcomm : init_comm() : malloc()");
						exit(EXIT_FAILURE);
					}
				}
				network_link_init(&netlinks[nb_netlink], prt->hosts[prt->links[i].source].vname, prt->hosts[prt->links[i].source].name, prt->hosts[prt->links[i].source].port, prt->links[i].protocol);
				numlink = nb_netlink;
				nb_netlink++;
			}

			/* Initialisation du virtual link */
			virtual_link_init(&vlinks[i], prt->links[i].name, numlink, RX);
		}
	}


	/* On tente de connecter les liens reseau */
	for(i =0 ; (unsigned int)i < nb_netlink; i++)
	{
	  network_link_connect(&netlinks[i]);
	}

	/* On lance les serveurs pour accepter les connexion non etablies */
	network_link_launch_server(my_name, prt->hosts[prt->my_host].port);
}



/*
	Ferme les connection reseau ouverte et
	libere les ressources utilisees
*/
void close_comm()
{
	int i;

	/* fermeture network_link */
	for(i = 0; (unsigned int)i < nb_netlink; i++)
		network_link_close(&netlinks[i]);

	if(netlinks)
		free(netlinks);

	/* fermeture virtual_link */
	for(i = 0; (unsigned int)i < nb_vlink; i++)
		virtual_link_close(&vlinks[i]);

	if(vlinks)
		free(vlinks);

	/* Fermeture des serveur */
	network_link_close_server();
}


