/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/* #define DEBUG  */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "debug_network_type.h"
#include "debug_network.h"

void debug_network_init(debug_network_t *debug, char *name) {
  dprints("debug network init\n");
  debug->name=name;
  dprints("name %s, %s\n",name, debug->name);
  debug->case_ext=0;
  debug->debug_cycle=-1;
  debug->recv_latence_mean=0;
  debug->recv_latence_min=-1000001;
  debug->recv_latence_max=-1000001;
  debug->recv_period_mean=0;
  debug->recv_period_min=-1000001;
  debug->recv_period_max=-1000001;
  debug->read_latence_mean=0;
  debug->read_latence_min=-1;
  debug->read_latence_max=-1;
  debug->paquet_size_mean=0;
  debug->paquet_size_min=-1;
  debug->paquet_size_max=-1;
  debug->nb_recv=0;
  debug->nb_read=0;
  debug->nb_read_new_msg=0;
  debug->nb_try_read=0;
  debug->miss_read_mean=0;
  debug->miss_read_min=-1;
  debug->miss_read_max=-1;
  
  /** init previous recv time stamp - even if not true */
  gettimeofday(&debug->prev_recv_time_tmp,(void *)NULL);
  /** init previous ext recv time stamp - even if not true */
  gettimeofday(&debug->ext_prev_recv_time,(void *)NULL);
  
  /** default : no debug of specific ext management */
  debug->case_ext=0;
  debug->ext_recv_latence_mean=0;
  debug->ext_recv_latence_min=-1000001;
  debug->ext_recv_latence_max=-1000001;
  debug->ext_recv_ante_period_mean=0;
  debug->ext_recv_ante_period_min=-1000001;
  debug->ext_recv_ante_period_max=-1000001;
  debug->ext_recv_post_period_mean=0;
  debug->ext_recv_post_period_min=-1000001;
  debug->ext_recv_post_period_max=-1000001;
  debug->ext_recv_period_mean=0;
  debug->ext_recv_period_min=-1000001;
  debug->ext_recv_period_max=-1000001;
  debug->ext_read_latence_mean=0;
  debug->ext_read_latence_min=-1;
  debug->ext_read_latence_max=-1;
  debug->ext_paquet_size_mean=0;
  debug->ext_paquet_size_min=-1;
  debug->ext_paquet_size_max=-1;
  debug->ext_miss_read_mean=0;
  debug->ext_miss_read_min=-1;
  debug->ext_miss_read_max=-1;
  debug->ext_nb_recv=0;
  debug->ext_nb_read=0;
  debug->ext_nb_read_new_msg=0;
  debug->ext_nb_try_read=0;
  debug->last_recv_ext=0;

  /** init last debug_print time */
  gettimeofday(&debug->last_debug_time,(void *)NULL);
}


void debug_network_print(debug_network_t *debug) {
  double elapsed_time=0;
  /** now time stamp */
  struct timeval now;

  if(debug->debug_cycle==-1)
    return;

  /** set now time stamp */
  gettimeofday(&now,(void *)NULL);
  /** test on debug period*/
  elapsed_time=(now.tv_sec - debug->last_debug_time.tv_sec)*1000000+(now.tv_usec - debug->last_debug_time.tv_usec);
  dprints("elapsed time : %f, debug_cycle %f\n",elapsed_time,(float)debug->debug_cycle);
  if(elapsed_time>debug->debug_cycle) {

    kprints("NETWORK RECV (%s): elapsed time (%d / %d ms)\n\tnb_recv=%ld \tnb_read=%ld\tread(new msg)=%ld\tlost=%ld\tmiss=%ld\n\trecv_latence (min/max/mean)=\t%5.3f \t%5.3f \t%5.3f \tms\n\tread_latence(min/max/mean)=\t%5.3f \t%5.3f \t%5.3f \tms\n\trecv_period (min/max/mean)=\t%5.3f \t%5.3f \t%5.3f \tms\n\tpaquet size(min/max/mean)=\t%d \t%d \t%d \to\n\tmiss reading(min/max/mean)=\t%5.0f \t%5.0f \t%5.2f\n",debug->name, (int)(elapsed_time/1000),(int)(debug->debug_cycle/1000), debug->nb_recv, debug->nb_read, debug->nb_read_new_msg,debug->nb_recv-debug->nb_read_new_msg,debug->nb_read-debug->nb_read_new_msg,debug->recv_latence_min/1000,debug->recv_latence_max/1000,debug->recv_latence_mean/1000,debug->read_latence_min/1000,debug->read_latence_max/1000,debug->read_latence_mean/1000,debug->recv_period_min/1000,debug->recv_period_max/1000,debug->recv_period_mean/1000,debug->paquet_size_min,debug->paquet_size_max,debug->paquet_size_mean,debug->miss_read_min,debug->miss_read_max,debug->miss_read_mean);
    
    if(debug->case_ext) {
      kprints("NETWORK RECV - CASE EXT (%s)\n\text_nb_recv=%ld \text_nb_read=%ld\tread(new msg)=%ld\tlost=%ld\tmiss=%ld\n\text_recv_latence (min/max/mean)=\t%.3f \t%.3f \t%.3f \tms\n\text_read_latence(min/max/mean)=  \t%.3f \t%.3f \t%.3f \tms\n\tante recv_period (min/max/mean)=\t\t%5.3f \t%5.3f \t%5.3f \tms\n\tpost recv_period (min/max/mean)=\t\t%5.3f \t%5.3f \t%5.3f \tms\n\t     recv_period (min/max/mean)=\t\t%5.3f \t%5.3f \t%5.3f \tms\n\text_paquet size(min/max/mean)=   \t%d \t%d \t%d \to\n\tmiss reading(min/max/mean)=\t\t%5.0f \t%5.0f \t%5.2f\n",debug->name, debug->ext_nb_recv, debug->ext_nb_read,debug->ext_nb_read_new_msg,debug->ext_nb_recv-debug->ext_nb_read_new_msg,debug->ext_nb_read-debug->ext_nb_read_new_msg,debug->ext_recv_latence_min/1000,debug->ext_recv_latence_max/1000,debug->ext_recv_latence_mean/1000,debug->ext_read_latence_min/1000,debug->ext_read_latence_max/1000,debug->ext_read_latence_mean/1000,debug->ext_recv_ante_period_min/1000,debug->ext_recv_ante_period_max/1000,debug->ext_recv_ante_period_mean/1000,debug->ext_recv_post_period_min/1000,debug->ext_recv_post_period_max/1000,debug->ext_recv_post_period_mean/1000,debug->ext_recv_period_min/1000,debug->ext_recv_period_max/1000,debug->ext_recv_period_mean/1000,debug->ext_paquet_size_min,debug->ext_paquet_size_max,debug->ext_paquet_size_mean,debug->ext_miss_read_min,debug->ext_miss_read_max,debug->ext_miss_read_mean);
      
      /** reset nb, mean, min and max values */
      debug->ext_recv_latence_mean=0;
      debug->ext_recv_latence_min=-1000001;
      debug->ext_recv_latence_max=-1000001;
      debug->ext_recv_ante_period_mean=0;
      debug->ext_recv_ante_period_min=-1000001;
      debug->ext_recv_ante_period_max=-1000001;
      debug->ext_recv_post_period_mean=0;
      debug->ext_recv_post_period_min=-1000001;
      debug->ext_recv_post_period_max=-1000001;
      debug->ext_recv_period_mean=0;
      debug->ext_recv_period_min=-1000001;
      debug->ext_recv_period_max=-1000001;
      debug->ext_read_latence_mean=0;
      debug->ext_read_latence_min=-1;
      debug->ext_read_latence_max=-1;
      debug->ext_paquet_size_mean=0;
      debug->ext_paquet_size_min=-1;
      debug->ext_paquet_size_max=-1;
      debug->ext_miss_read_mean=0;
      debug->ext_miss_read_min=-1;
      debug->ext_miss_read_max=-1;
      debug->ext_nb_recv=0;
      debug->ext_nb_read=0;
      debug->ext_nb_read_new_msg=0;
      debug->ext_nb_try_read=0;
      debug->last_recv_ext=0;
    }
    
    /** set last debug_print time */
    gettimeofday(&debug->last_debug_time,(void *)NULL);

    /** reset nb, mean, min and max values */
    debug->recv_latence_mean=0;
    debug->recv_latence_min=-1000001;
    debug->recv_latence_max=-1000001;
    debug->recv_period_mean=0;
    debug->recv_period_min=-1000001;
    debug->recv_period_max=-1000001;
    debug->read_latence_mean=0;
    debug->read_latence_min=-1;
    debug->read_latence_max=-1;
    debug->paquet_size_mean=0;
    debug->paquet_size_min=-1;
    debug->paquet_size_max=-1;
    debug->miss_read_mean=0;
    debug->miss_read_min=-1;
    debug->miss_read_max=-1;
    debug->nb_recv=0;
    debug->nb_read=0;
    debug->nb_read_new_msg=0;
    debug->nb_try_read=0;
  }
}


void debug_network_update_at_reception(debug_network_t *debug,long t_sec, long t_usec, int size) {
  float diff_time;

  if(debug->debug_cycle==-1)
    return;

  dprints("update at recv\n");

  /** move prev time stamp (stored in tmp) in the adequate variable */
  debug->prev_recv_time = debug->prev_recv_time_tmp;

  /** set send time stamp */
  debug->send_t_sec=t_sec;
  debug->send_t_usec=t_usec;

  /** get recv time stamp */
  gettimeofday(&debug->recv_time,(void *)NULL);

  /** increment nb recv */
  debug->nb_recv++;

  /** compute min, max, mean of channel latency */
  diff_time=(debug->recv_time.tv_sec - debug->send_t_sec)*1000000 + (debug->recv_time.tv_usec - debug->send_t_usec);
  debug->recv_latence_mean = (debug->recv_latence_mean*(debug->nb_recv-1)+diff_time)/debug->nb_recv;
  if(debug->recv_latence_min<-1000000) {
    debug->recv_latence_min=diff_time;
    debug->recv_latence_max=diff_time;
  }
  else {
    if(diff_time<debug->recv_latence_min) 
      debug->recv_latence_min=diff_time;
    if(diff_time>debug->recv_latence_max) 
      debug->recv_latence_max=diff_time; 
  }

  /** compute min, max, mean of paquet size */
  debug->paquet_size_mean = (debug->paquet_size_mean*(debug->nb_recv-1)+size)/debug->nb_recv;
  if(debug->paquet_size_min<0) {
    debug->paquet_size_min=size;
    debug->paquet_size_max=size;
  }
  else {
    if(size<debug->paquet_size_min) 
      debug->paquet_size_min=size;
    if(size>debug->paquet_size_max) 
      debug->paquet_size_max=size; 
  }

  /** compute min,max,mean of period between two receptions */
  diff_time=(debug->recv_time.tv_sec - debug->prev_recv_time.tv_sec)*1000000 + (debug->recv_time.tv_usec - debug->prev_recv_time.tv_usec);
  debug->recv_period_mean = (debug->recv_period_mean*(debug->nb_recv-1)+diff_time)/debug->nb_recv;
  if(debug->recv_period_min<-1000000) {
    debug->recv_period_min=diff_time;
    debug->recv_period_max=diff_time;
  }
  else {
    if(diff_time<debug->recv_period_min) 
      debug->recv_period_min=diff_time;
    if(diff_time>debug->recv_period_max) 
      debug->recv_period_max=diff_time; 
  }
  /** store in tmp - necessary in case ext debug need the prev_value */
  debug->prev_recv_time_tmp = debug->recv_time;

  if(debug->case_ext) {
    /** if last recv is ext */
    if(debug->last_recv_ext) {
      /** compute min,max,mean of period between two receptions after ext */
      diff_time=(debug->recv_time.tv_sec - debug->ext_recv_time.tv_sec)*1000000 + (debug->recv_time.tv_usec - debug->ext_recv_time.tv_usec);
      debug->ext_recv_post_period_mean = (debug->ext_recv_post_period_mean*(debug->ext_nb_recv-1)+diff_time)/debug->ext_nb_recv;
      if(debug->ext_recv_post_period_min<-1000000) {
	debug->ext_recv_post_period_min=diff_time;
	debug->ext_recv_post_period_max=diff_time;
      }
      else {
	if(diff_time<debug->ext_recv_post_period_min) 
	  debug->ext_recv_post_period_min=diff_time;
	if(diff_time>debug->ext_recv_post_period_max) 
	  debug->ext_recv_post_period_max=diff_time; 
      }
      /** reset */
      debug->last_recv_ext=0;
    }
  }
}


void debug_network_update_at_reception_ext(debug_network_t *debug,long t_sec, long t_usec, int size) {
  float diff_time;

  if(debug->debug_cycle==-1 || debug->case_ext==0)
    return;

  dprints("update at recv case ext\n");

  /** set send time stamp */
  debug->send_t_sec=t_sec;
  debug->send_t_usec=t_usec;

  /** get recv time stamp */
  gettimeofday(&debug->ext_recv_time,(void *)NULL);

  /** increment nb recv */
  debug->ext_nb_recv++;

  /** compute min, max, mean of channel latency */
  diff_time=(debug->ext_recv_time.tv_sec - debug->send_t_sec)*1000000 + (debug->ext_recv_time.tv_usec - debug->send_t_usec);
  debug->ext_recv_latence_mean = (debug->ext_recv_latence_mean*(debug->ext_nb_recv-1)+diff_time)/debug->ext_nb_recv;
  if(debug->ext_recv_latence_min<-1000000) {
    debug->ext_recv_latence_min=diff_time;
    debug->ext_recv_latence_max=diff_time;
  }
  else {
    if(diff_time<debug->ext_recv_latence_min) 
      debug->ext_recv_latence_min=diff_time;
    if(diff_time>debug->ext_recv_latence_max) 
      debug->ext_recv_latence_max=diff_time; 
  }

  /** compute min, max, mean of paquet size */
  debug->ext_paquet_size_mean = (debug->ext_paquet_size_mean*(debug->ext_nb_recv-1)+size)/debug->ext_nb_recv;
  if(debug->ext_paquet_size_min<0) {
    debug->ext_paquet_size_min=size;
    debug->ext_paquet_size_max=size;
  }
  else {
    if(size<debug->ext_paquet_size_min) 
      debug->ext_paquet_size_min=size;
    if(size>debug->ext_paquet_size_max) 
      debug->ext_paquet_size_max=size; 
  }
  
  /** compute min,max,mean of period between a reception and an ext
   * reception */
  /** for 2 successive ext reception, the prev time is the time
   * computed in the global debug reception */
  diff_time=(debug->ext_recv_time.tv_sec - debug->prev_recv_time.tv_sec)*1000000 + (debug->ext_recv_time.tv_usec - debug->prev_recv_time.tv_usec);
  debug->ext_recv_ante_period_mean = (debug->ext_recv_ante_period_mean*(debug->ext_nb_recv-1)+diff_time)/debug->ext_nb_recv;
  if(debug->ext_recv_ante_period_min<-1000000) {
    debug->ext_recv_ante_period_min=diff_time;
    debug->ext_recv_ante_period_max=diff_time;
  }
  else {
    if(diff_time<debug->ext_recv_ante_period_min) 
      debug->ext_recv_ante_period_min=diff_time;
    if(diff_time>debug->ext_recv_ante_period_max) 
      debug->ext_recv_ante_period_max=diff_time; 
  }


  /** compute min,max,mean of period between 2 ext reception */
  diff_time=(debug->ext_recv_time.tv_sec - debug->ext_prev_recv_time.tv_sec)*1000000 + (debug->ext_recv_time.tv_usec - debug->ext_prev_recv_time.tv_usec);
  debug->ext_recv_period_mean = (debug->ext_recv_period_mean*(debug->ext_nb_recv-1)+diff_time)/debug->ext_nb_recv;
  if(debug->ext_recv_period_min<-1000000) {
    debug->ext_recv_period_min=diff_time;
    debug->ext_recv_period_max=diff_time;
  }
  else {
    if(diff_time<debug->ext_recv_period_min) 
      debug->ext_recv_period_min=diff_time;
    if(diff_time>debug->ext_recv_period_max) 
      debug->ext_recv_period_max=diff_time; 
  }
  /** store prev ext recv time */
  debug->ext_prev_recv_time = debug->ext_recv_time;

  /** set last recv is ext to 1 */
  debug->last_recv_ext=1;
}


void debug_network_update_at_reading(debug_network_t *debug, int old_msg) {
  float diff_time;
  float nb_try_read;
  if(debug->debug_cycle==-1)
    return;
  dprints("update at reading\n");

  /** get reading time stamp */
  gettimeofday(&debug->read_time,(void *)NULL);

  /** increment nb read */
  debug->nb_read++;
  
  /** compute min, max, mean of reading latency */
  diff_time=(debug->read_time.tv_sec - debug->recv_time.tv_sec)*1000000 + (debug->read_time.tv_usec - debug->recv_time.tv_usec);
  debug->read_latence_mean = (debug->read_latence_mean*(debug->nb_read-1)+diff_time)/debug->nb_read;
  if(debug->read_latence_min<0) {
    debug->read_latence_min=diff_time;
    debug->read_latence_max=diff_time;
  }
  else {
    if(diff_time<debug->read_latence_min) 
      debug->read_latence_min=diff_time;
    if(diff_time>debug->read_latence_max) 
      debug->read_latence_max=diff_time; 
  }

  if(!old_msg) {
    /** increment nb of new msg read */
    debug->nb_read_new_msg++;
    /** compute min/max/mean of miss reading */
    nb_try_read=(float)debug->nb_try_read-1;
    debug->miss_read_mean = (debug->miss_read_mean*(debug->nb_read_new_msg-1)+nb_try_read)/(debug->nb_read_new_msg);
    if(debug->miss_read_min<0) {
      debug->miss_read_min=nb_try_read;
      debug->miss_read_max=nb_try_read;
    }
    else {
      if(nb_try_read<debug->miss_read_min) 
	debug->miss_read_min=nb_try_read;
      if(nb_try_read>debug->miss_read_max) 
	debug->miss_read_max=nb_try_read; 
    }
    debug->nb_try_read=0;
  }
}

void debug_network_update_at_reading_ext(debug_network_t *debug, int old_msg) {
  float diff_time;
  float nb_try_read;
  if(debug->debug_cycle==-1 || debug->case_ext==0)
    return;
  dprints("update at reading - case ext\n");

  /** get reading time stamp */
  gettimeofday(&debug->ext_read_time,(void *)NULL);

  /** increment nb read */
  debug->ext_nb_read++;
  
  /** compute min, max, mean of reading latency */
  diff_time=(debug->ext_read_time.tv_sec - debug->ext_recv_time.tv_sec)*1000000 + (debug->ext_read_time.tv_usec - debug->ext_recv_time.tv_usec);
  debug->ext_read_latence_mean = (debug->ext_read_latence_mean*(debug->ext_nb_read-1)+diff_time)/debug->ext_nb_read;
  if(debug->ext_read_latence_min<0) {
    debug->ext_read_latence_min=diff_time;
    debug->ext_read_latence_max=diff_time;
  }
  else {
    if(diff_time<debug->ext_read_latence_min) 
      debug->ext_read_latence_min=diff_time;
    if(diff_time>debug->ext_read_latence_max) 
      debug->ext_read_latence_max=diff_time; 
  }

  if(!old_msg) {
    /** increment nb of new msg read */
    debug->ext_nb_read_new_msg++;
    /** compute min/max/mean of miss reading */
    nb_try_read=(float)debug->ext_nb_try_read;
    debug->ext_miss_read_mean = (debug->ext_miss_read_mean*(debug->ext_nb_read_new_msg-1)+nb_try_read)/(debug->ext_nb_read_new_msg);
    if(debug->ext_miss_read_min<0) {
      debug->ext_miss_read_min=nb_try_read;
      debug->ext_miss_read_max=nb_try_read;
    }
    else {
      if(nb_try_read<debug->ext_miss_read_min) 
	debug->ext_miss_read_min=nb_try_read;
      if(nb_try_read>debug->ext_miss_read_max) 
	debug->ext_miss_read_max=nb_try_read; 
    }
    debug->ext_nb_try_read=0;
  }
  else
    debug->ext_nb_try_read++;
}

void debug_network_update_at_try_reading(debug_network_t *debug) {
  if(debug->debug_cycle==-1)
    return;
  dprints("update at try reading\n");

  /** increment of nb try read */
  debug->nb_try_read++;
}

