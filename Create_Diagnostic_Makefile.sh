#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################
####################################################
#script permettant de generer un Makefile pour compiler promethe
#v2.0 Maillard M. & Baccon J.C.
#v3.0 Hirel J.
###################################################


####################################################
#definition de $CFLAGS $FLAGS_OPTIM $FLAGS_DEBUG
####################################################
source ../scripts/COMPILE_FLAG

####################################################
#Definition des chemins d'acces, options de compile etc...
####################################################
# Nom du programme
PROG_NAME="libkernel_blind_diagnostic"


#les bibliotheques et leurs chemins d'acces

# Initialisation des libs, includes et flags
PACKAGES="glib-2.0"
LIBS=`pkg-config --libs $PACKAGES`
INCLUDES="-I$SIMULATOR_PATH/shared/include/ -I$PWD/include -I$PWD/include/network -I$SIMULATOR_PATH -I$HOME/.local/include "`pkg-config --cflags $PACKAGES`
#CFLAGS definis dans le COMPILE_FLAGS

# Gestion des parametres passes au Create_Makefile
echo "compile $PROG_NAME"

#Version finale des libs, includes et flags
FINALINCLUDES="$INCLUDES -I$PWD/include/blind "
FINALLIBS="$LIBS -L../lib/Linux/blc/ -lblc -lrt"
# Warning : USE_ENET est forcé à 0 pour que diagnostic compile
FINALCFLAGS="$CFLAGS -DAVEUGLE $FLAGS_OPTIM -DDIAGNOSTIC -DUSE_ENET=0"

#Les repertoires de destination des fichiers compiles
LIBDIR="$SIMULATOR_PATH/lib/$SYSTEM/kernel"
OBJDIR="$PWD/obj/$SYSTEM/$PROG_NAME"
mkdir -p $OBJDIR

#Les repertoires de destination des fichiers compiles
SOURCES="src/shared/gestion_diagnostic.c src/shared/gestion_threads.c src/shared/lenautil.c src/shared/promethe.c src/shared/gestion_ES.c src/shared/iostream_over_network.c src/shared/rttoken.c src/shared/gestion_sequ.c src/shared/lec_config.c src/shared/outils.c src/shared/script.c diagnostic/diagnostic.c"
OBJECTS=""

####################################################
#Creation du Makefile
####################################################

MAKEFILE="Makefile.$PROG_NAME"

#ecrasement du Makefile precedent
echo "" > $MAKEFILE
#regle par defaut
echo "default: $PROG_NAME" >> $MAKEFILE
echo "" >> $MAKEFILE

echo -e "include ../scripts/variables.mk\n" >> $MAKEFILE


# creer les regles
#pour chaque  .o
for i in $SOURCES
do
  echo "processing '$i'"
  FICHIER=`basename $i .c`
  CHEMIN=`echo $i | sed -e s@$FICHIER.c@@`
  echo "$OBJDIR/$FICHIER.o:$i" >> $MAKEFILE
  echo -e "\t@echo \"[processing $i...]\"">>$MAKEFILE
  echo -e "\t@(cd $CHEMIN; $CC $FINALCFLAGS $FINALINCLUDES -c -o $OBJDIR/$FICHIER.o $FICHIER.c)" >> $MAKEFILE
  echo "" >> $MAKEFILE
  OBJECTS="$OBJECTS $OBJDIR/$FICHIER.o"
done

#pour l'edition de liens et le lien sur le binaire
echo "$PROG_NAME: $OBJECTS" >> $MAKEFILE
echo -e "\t@echo \"[making Promethe kernel library...]\"" >> $MAKEFILE
echo -e "\t@mkdir -p $LIBDIR" >> $MAKEFILE
echo -e "\t@$AR -rcv $LIBDIR/$PROG_NAME.a $OBJECTS" >> $MAKEFILE
echo -e "\t@$CC  -o diagnostic/diagnostic  -pthread -L$PWD/../lib/$SYSTEM/kernel/ -lkernel_blind_diagnostic  -L$PWD/../lib/$SYSTEM/script/ -lscript -ldl $FINALLIBS" >> $MAKEFILE
echo -e	"\t@cp diagnostic/diagnostic \$(bindir)/." >> $MAKEFILE


echo "" >> $MAKEFILE


#regles additionnelles
echo "clean:" >> $MAKEFILE
echo -e "\trm -f  $OBJDIR/*.o" >> $MAKEFILE
echo "" >> $MAKEFILE

echo "reset:" >> $MAKEFILE
echo -e "\trm -f  $OBJDIR/*.o $LIBDIR/$PROG_NAME.a" >> $MAKEFILE
echo "" >> $MAKEFILE
