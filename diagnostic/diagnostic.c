/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/* programme de diagnostic du sequncement d'un script */
/* P. Gaussier Aout 2006 */


#include <locale.h>
#include <pthread.h>
#include "public.h"
#include <search.h>  /* pour l'initialisation de la table de hachage */

/* fonctions qui sont habituellement definies cote utilisateurs mais qui sont demandees par le kernel
   pour une bonne compilation du programme. Ces fonctions ne seront pas appelees. */

void initialisation_des_groupes()
{
}

void init_sequenceur_autonome()
{
}

void send_neurons_to_pandora()
{

}


void initialisation_utilisateur()
{
}
void fin_simulation_utilisateur()
{
}
void mise_a_jour(int gpe, int a, int b)
{
(void)gpe;
(void) a;
(void)b;
}                               /* appel fonction d'apprentissage pour un groupe */
void initialise_sortie_reseau()
{
}
void initialise_STM()
{
}
void initialise_gaussiennes()
{
}

extern void diagnostic(int use_secondary_link); /* fonction de gestion_sequ11.c */
extern void diagnostic_detaille(int max_ech_temps, int use_secondary_link); /* fonction de gestion_sequ11.c */

extern void launch_each_group_as_a_thread();
extern void terminate_each_group_as_a_thread();

/* compte le nbre de groupe n'ayant jamais pu recevoir de token a la fin d'un cycle complet
   dexecution de toutes les echelles de temps. */

int non_activable()
{
    int i, k;

    k = 0;
    for (i = 0; i < nbre_groupe; i++)
    {
        if (def_groupe[i].deja_active == 0)
        {
            printf("WARNING group name : %s (num %d) cannot be activated \n",def_groupe[i].no_name, i);
            k++;
        }
    }
    return k;
}

/* compte le nombre de liaisons secondaires */

int nb_secondary_links()
{
    int gpe, j, i;
    int cpt = 0;

    for (gpe = 0; gpe < nbre_groupe; gpe++)
    {
        j = 0;
        i = find_input_link(gpe, j);
        while (i != -1)         /* tant que le groupe a encore un lien en entree a verifier */
        {
            if (liaison[i].secondaire == 1)
            {
                cpt = cpt + 1;
                printf
                    ("secondary link between input group %d and output group %d \n",
                     liaison[i].depart, liaison[i].arrivee);
            }
            j = j + 1;
            i = find_input_link(gpe, j);
        }
    }
    return cpt;
}

int main(int argc, char *argv[])
{
    int nb_warning, nb_error, nb_s;

    if (argc < 4)
    {
        printf
            ("ERROR diagnostic must have 3 arguments: script res and config \n");
        return 1;
    }

    hcreate(10000);   /* table de hachage pour les no de groupes symboliques */

    strcpy(fscript, argv[1]);
    strcpy(freseau, argv[2]);
    strcpy(fconfig, argv[3]);

    setlocale(LC_ALL, "C");

    lecture_reseau(freseau);
    lecture_script(fscript, 1);
    printf("max_ech_temps = %d \n", max_ech_temps);
    lecture_config(fconfig);

    /* utile si vous voulez connaitre l'ordre exact d"execution de chaque groupe. 
       identique a step by step de promethe */
    /*  diagnostic_detaille(max_ech_temps,1); */

    printf("\n\n Begin diagnostic :\n\n");

    nb_s = nb_secondary_links();
    printf("There are %d secondary links in the script \n", nb_s);


    printf("\nHard test (no use of secondary links) : \n");
    diagnostic(0);
    nb_warning = non_activable();
    if (nb_warning == 0)
        printf("OK, no problem.\n");
    else
    {
        printf
            ("WARNING: there are %d groups that could not be launched without secondary links\n",
             nb_warning);
    }

    printf("\nNormal test (take into account the secondary links) : \n");
    diagnostic(1);
    nb_error = non_activable();

    if (nb_error == 0)
        printf("OK, no problem.\n");
    else
    {
        printf
            ("ERROR: there are %d groups that cannot be launched. \n\n Try the use of secondary links\n",
             nb_error);
        printf
            ("RTtokens and parallel scripts could trap the test (strange tokens...) \n");
    }

    if ((nb_error == 0) && (nb_s > 0))
        printf
            ("Warning, you could suppress the secondary links to avoid scheduling problems...\n");

    printf("END. \n");

    return 0;
}
