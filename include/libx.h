/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _LIBX_H
#define _LIBX_H
/*-----------------------------------------------------------------*/
/*   Promethe : simulateur de reseau de neurones                   */
/*   Copyright Philippe GAUSSIER 1992                              */
/*-----------------------------------------------------------------*/

/*#define DEBUG*/

#define UNUSED_PARAM (void)

#ifndef AVEUGLE
#define EndOfLine                FALSE
#define dseuil 20
#define Edge                     TRUE
#define SimplexSansSerif         4
#define BoldSansSerif            6
#endif

#include <stdio.h>
#include <math.h>

#ifndef NET_MESSAGE_DEBUG_DIST
#include <net_message_debug_dist.h>
#endif

#ifndef AVEUGLE
#include "graphic_Tx.h"
#else
typedef struct
{
    int x;
    int y;
} TxPoint;
#endif

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef _STRUCTLENA
#include "structlena.h"
#endif /* end of #ifndef _STRUCTLENA */

#ifndef _RESEAU
#include "reseau.h"
#endif /* end of #ifndef _RESEAU */

#ifndef OUTILS_H
#include <outils.h>
#endif /* end of #ifndef OUTILS_H */

/*#include "reduit.h"*/
/*#include "simul_res.h"*/

#ifndef GLOBAL_SIZES_H
#define GLOBAL_SIZES_H

#define nb_max_echelles 255
#define nb_max_groupes 1024
#define nb_max_liaisons_pour_un_groupe 1024

#define taille_chaine 255
#define nb_fichiers 255

#endif


/*---------------------------------------------------------------*/
/*       VARIABLES  GLOBALES A L'ENSEMBLE DU PROGRAMME           */
/*---------------------------------------------------------------*/

extern type_tableau neurone;    /*tableau contenant l'ens du reseau   */
extern type_tableau_pando info_neurone_pour_pando;
extern type_tableau_voies liaison;

extern type_matrice donnee;
extern type_matrice corrige;
extern type_matrice resultat;

extern type_groupe def_groupe[];
extern type_pando_groupe para_pando_group[];

extern char nom_groupe[nb_max_groupes][taille_chaine];   /* nom associe a un gpe, 200 gpes max */

extern int nbre_donnee;         /* nombre de vecteurs a etudier       */
extern int nbre_groupe;         /* nombre de groupes de neuones compet */
extern int nbre_neurone;        /* nombre de neurones dans le reseau  */
extern int nbre_entree;         /* nbre de vecteurs utilises en entree */
extern int nbre_sortie;         /* nbre de neurones utilises en sortie */

extern int cycle;               /*nombre de cycles de calcul avec 1 ex */
extern float periode;           /*periode moyennage entree            */

extern int nbre_couche;

extern float eps;               /* epsilon pour la modif des coeff    */

extern float tableau_gaussienne[30];


/*********************************************************************************/
extern int echelle_temps[];     /* nbre de cycles par echelle                */
                /* lu dans le fichier config                 */

extern int fin_echelle[];

/* Cela n'est plus utilise de depuis longtemps... PG */
                                                                                                                                                                                                                                                                                                                                                                                           /*extern type_vecteur_entier t; *//* nbre de neurones pour couche       */
                                                                                                                                                                                                                                                                                                                                                                                           /*extern type_vecteur_entier c; *//* type du groupe pour apprentissage  */


/*---------------------------------------------------------------*/
extern float variable_hasard;
extern char argv_fichier[nb_fichiers][taille_chaine];
extern unsigned char *im4;
extern int gpe_courant_a_afficher;
extern int rapide;

extern float image1_posx;
extern float image1_posy;
extern int image_click_event;
extern int dragging_image1;

#ifndef AVEUGLE
extern type_image_obj image1_obj;
#endif


extern float image2_posx;
extern float image2_posy;
extern int image_click_event2;
extern int dragging_image2;

extern int nbre_groupe, nbre_liaison;

extern type_liaison *deb_liaison, *fin_liaison;


extern unsigned char *im, *im_recons, *im_recons2;




extern int xmax, ymax;          /* taille de l'image traitee           */


extern int debug;


extern float vigilence;         /* variable entre 0 et 1                        */
            /* determine l'etat de vigilence du systeme     */
            /* il s'agit d'une constante multiplicative     */
            /* pour un seuil                                */


extern int global_learn;



/*Variable globale pour specifier le mode de depart : 
	ONLINE-LEARNING/USE-ONLY 
		ou 
	CONTINUE-LEARNING/CONTINUE-USING*/
extern int continue_simulation_status;
#define START 0
#define CONTINUE 1

enum arg_promethe {  No_arg_progname=0, No_arg_script, No_arg_res, No_arg_config, No_arg_dev, No_arg_gcd,
		     No_arg_prt, No_arg_VirtualNetwork, No_arg_DebugState, No_arg_StartMode, No_arg_Windows,
		     No_arg_mcpu, No_arg_telnet, No_arg_bus_ivy, No_arg_identity, No_arg_AutresFlags };

    /* Non utilise et non passe au niveau du kernel pour l'instant: No_arg_dev, No_arg_prt, No_arg_VirtualNetwork */

/*---------------------------------------------------------------------*/
/* variables et structures liees au graphisme                          */
/*---------------------------------------------------------------------*/

#ifndef AVEUGLE


extern TxDonneesFenetre fenetre1, fenetre2, fenetre3;
extern TxDonneesFenetre image1, image2;


#endif

/*#ifdef sparc*/
extern int p_trace;
/*#endif*/
#endif
