/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*---------------------------------------*/

/** file included in interface.c dans coeos */

group_mode_link group_mode_link_tab[nbre_type_groupes];

/** Example of all possible links:  

{"X",{No_mode_link_product_compet, "product (after compet)", {1}}},  
{"d",{No_mode_link_distance_compet, "distance (after compet)", {1}}},  
{"X",{No_mode_link_product_analog, "product (before compet)", {1}}},   
{"d",{No_mode_link_distance_analog, "distance (before compet)", {1}}}, 
{"learn",{NEUROMOD_learn, "learn_factor [neuromod]", {1}}},   
{"decay",{NEUROMOD_decay, "decay_factor [neuromod]", {1}}},  
{"MM",{MACRO_MC, "MACRO_MC", {1}}}, 
{"MD",{MICRO_DIST, "MICRO_DIST", {1}}},
{"MS",{MICRO_SUPERVISOR, "MICRO_SUPERVISOR", {1}}},
{"ML",{MACRO_LINKS, "MACRO_LINKS", {1}}},
{"RecL",{RECURRENT_LINKS, "RECURRENT_LINKS", {1}}},
{"IcL",{INCOMING_LINKS, "INCOMING_LINKS", {1}}},
{"IpL",{INPUT_LINKS, "INPUT_LINKS", {1}}},
{"PH",{PROXIMAL_HEBBIAN, "PROXIMAL_HEBBIAN", {1}}},
{"DG_CA",{DG_CA, "DG_CA", {1}}},
{"PNH",{PROXIMAL_NON_HEBBIAN, "PROXIMAL_NON_HEBBIAN", {1}}},
{"IH",{INTERMEDIATE_HEBBIAN, "INTERMEDIATE_HEBBIAN", {1}}},
{"REC_CA",{REC_CA_CA, "REC_CA_CA", {1}}},
{"DH",{DISTAL_HEBBIAN, "DISTAL_HEBBIAN", {1}}},
{"DNH", {DISTAL_NON_HEBBIAN, "DISTAL_NON_HEBBIAN",{1}}},
{"EC_CA",{EC_CA, "EC_CA", {1}}},
{"I", {INHIBITION, "INHIBITION",{1}}},
{"TS", {TYPE_SIGMA, "TYPE_SIGMA",{1}}},
{"RH",{RECURRENT_HEBBIAN, "RECURRENT_HEBBIAN", {1}}},
{"M",{MOTIVATIONNAL, "MOTIVATIONNAL", {1}}},
{"NMB",{NEURO_MOD_BUT, "NEURO_MOD_BUT", {1}}}, 
{"CA1",{CA1, "CA1", {1}}} 

*/


mode_lien default_group_mode_links[] = {
   {"X",{No_mode_link_product_compet, "product (after compet)", {1}}},
   {"d",{No_mode_link_distance_compet, "distance (after compet)", {1}}},
   {"X",{No_mode_link_product_analog, "product (before compet)", {1}}},
   {"d",{No_mode_link_distance_analog, "distance (before compet)", {1}}},
};

mode_lien hebb_group_mode_links[] = {
   {"X",{No_mode_link_product_compet, "product", {1}}},
   {"learn",{NEUROMOD_learn, "learn_factor [neuromod]", {1}}},
   {"decay",{NEUROMOD_decay, "decay [neuromod]", {1}}},
   {"decay2",{NEUROMOD_decay2, "decay2 [neuromod]", {1}}},
   {"decay3",{NEUROMOD_decay3, "decay3 [neuromod]", {1}}}
};

mode_lien lms_group_mode_links[] = {
   {"X",{No_mode_link_product_compet, "product", {1}}},
   {"learn",{NEUROMOD_learn, "learn_factor [neuromod]", {1}}}
};

mode_lien goal_group_mode_links[] = {
   {"ML",{MACRO_LINKS, "MACRO_LINKS", {1}}},
   {"M",{MOTIVATIONNAL, "MOTIVATIONNAL", {1}}},
   {"CA1",{CA1, "CA1", {1}}},
   {"RH",{RECURRENT_HEBBIAN, "RECURRENT_HEBBIAN", {1}}},
   {"NMB",{NEURO_MOD_BUT, "NEURO_MOD_BUT", {1}}}, 
   {"learn",{NEUROMOD_learn, "learn_factor [neuromod]", {1}}},
   {"inhibMotiv",{inhibMotiv, "inhib_motivation [No microN]", {1}}}
};


mode_lien saw_group_mode_links[] = {
   {"X",{No_mode_link_product_compet, "product (after compet)", {1}}},
   {"d",{No_mode_link_distance_compet, "distance (after compet)", {1}}},
   {"X",{No_mode_link_product_analog, "product (before compet)", {1}}},
   {"d",{No_mode_link_distance_analog, "distance (before compet)", {1}}},
   {"d2",{No_mode_link_distance_euclidienne, "Euclidian distance", {1}}},
   {"E",{No_mode_link_distance_expo, "Exponential distance", {1}}},
   {"P",{No_mode_link_produit, "Product PI", {1}}},
   {"sigma",{NEUROMOD_sigma, "sigma [neuromod]", {1}}}
};

mode_lien sigma_pi_group_mode_links[] = {
   {"ML",{MACRO_LINKS, "MACRO_LINKS", {1}}},
   {"I", {INHIBITION, "INHIBITION",{1}}},
   {"TS", {TYPE_SIGMA, "TYPE_SIGMA",{1}}}
};

mode_lien selective_winner_modulated_group_mode_links[] = {
   {"X",{No_mode_link_product_compet, "product (after compet)", {1}}},
   {"d",{No_mode_link_distance_compet, "distance (after compet)", {1}}},
   {"X",{No_mode_link_product_analog, "product (before compet)", {1}}},
   {"d",{No_mode_link_distance_analog, "distance (before compet)", {1}}},
   {"C", {DISTAL_HEBBIAN, "Hebbian-like (Context)",{1}}}
};



void init_group_mode_link_tab(void) {
   int i;
   group_mode_link *ptr;
   int size_m=sizeof(mode_lien);

   for(i=0;i<nbre_type_groupes;i++) { 
      ptr=&group_mode_link_tab[i];
      
      /** default initialization */
      ptr->nb_mode = sizeof(default_group_mode_links)/size_m;
      ptr->mode_tab = default_group_mode_links;
   } 
   
   /** init hebb like groups */
   group_mode_link_tab[No_Hebb].nb_mode=sizeof(hebb_group_mode_links)/size_m;
   group_mode_link_tab[No_Hebb].mode_tab=hebb_group_mode_links;

   group_mode_link_tab[No_Hebb_Seuil].nb_mode=sizeof(hebb_group_mode_links)/size_m;
   group_mode_link_tab[No_Hebb_Seuil].mode_tab=hebb_group_mode_links;

   group_mode_link_tab[No_Hebb_Seuil_binaire].nb_mode=sizeof(hebb_group_mode_links)/size_m;
   group_mode_link_tab[No_Hebb_Seuil_binaire].mode_tab=hebb_group_mode_links;

   group_mode_link_tab[No_Hopfield].nb_mode=sizeof(hebb_group_mode_links)/size_m;
   group_mode_link_tab[No_Hopfield].mode_tab=hebb_group_mode_links;


   /** init lms groups */
   group_mode_link_tab[No_LMS].nb_mode=sizeof(lms_group_mode_links)/size_m;
   group_mode_link_tab[No_LMS].mode_tab=lms_group_mode_links;


   /** init goal group */
   group_mode_link_tab[No_But].nb_mode=sizeof(goal_group_mode_links)/size_m;
   group_mode_link_tab[No_But].mode_tab=goal_group_mode_links;

   /** init saw groups */
   group_mode_link_tab[No_SAW].nb_mode=sizeof(saw_group_mode_links)/size_m;
   group_mode_link_tab[No_SAW].mode_tab=saw_group_mode_links;

   group_mode_link_tab[No_Kmean_R].nb_mode=sizeof(saw_group_mode_links)/size_m;
   group_mode_link_tab[No_Kmean_R].mode_tab=saw_group_mode_links;

   /** init sigma_pi group */
   group_mode_link_tab[No_Sigma_PI].nb_mode=sizeof(sigma_pi_group_mode_links)/size_m;
   group_mode_link_tab[No_Sigma_PI].mode_tab=sigma_pi_group_mode_links;

   /** init selective_winner_modulated group */
   group_mode_link_tab[No_selective_winner_modulated].nb_mode=sizeof(selective_winner_modulated_group_mode_links)/size_m;
   group_mode_link_tab[No_selective_winner_modulated].mode_tab=selective_winner_modulated_group_mode_links;


}
