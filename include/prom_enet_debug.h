/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef __PROM_ENET_DEBUG__
#define __PROM_ENET_DEBUG__

#include <glib.h>
#include <enet/enet.h>
#include <libx.h>
#include <semaphore.h>

#define enet_debug_packetSize 256
#define buffer_size 64
#define nPackets_per_frame 8
#define event_waitTime 10  /* number of milliseconds to wait for network events, corresponding to time between frames  */

extern long secsFirstCall;

#define NN_message_magic_card 4329

extern int hauteur_display_gpe;

typedef enum type_enet_message
{
  NOT_DEFINED = 0,
  GROUP_DEBUG = 1,
  INIT_DEFGROUPE = 2
} type_enet_message;

typedef struct type_nn_message
{
  type_enet_message type_trame;
   gint32 val_nn_message_id;
   gint32 channelID;
   gint32 NN_id;  /* No attribue a un script / promethe */
   gint32 no_message;
   gint32 time_stamp;
   gint32 gpe;
   gint32 type_message; /* ON, OFF, ... */
} type_nn_message;


typedef struct type_connexion_udp
{
   ENetAddress address;
   ENetHost * host;
   ENetPeer * peer;
   ENetEvent event;
   ENetPacket * packet;
   void *data;               /* pointeur vers la zone memoire contenant les donnees mises dans le packet */
   void (*data_treatment)(struct type_connexion_udp *this, int channel_number);
   unsigned int waitTime;    /* number of milliseconds to wait for network events, corresponding to time between frames  */
   int sendCounter; /* counter of sent events */
   sem_t lock;
} type_connexion_udp;


extern void send_debug_data(void *data, size_t packetSize,int channel_number);
extern void send_token_oscillo_kernel(int gpe, int phase, glong time);
void init_enet_token_oscillo_kernel(char *ip_adr, int port);
extern void quit_enet_token_oscillo_kernel();

#endif
