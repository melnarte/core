/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef PROMETHE_H
#define PROMETHE_H

#include <glib.h>
#include "libx.h"

extern int nbre_fichiers;       /* utilises en arg du programme */
extern int inverse_video;       /* 1 pour inverser pour sortie imprimante  */
extern int rafraichissement_image;
extern int debug;
extern int demo;
extern int rapide;              /* 1 lors interpretation n'affiche pas transfo */
extern int p_trace;             /* mode trace */
extern int global_learn;        /* learn=1 learning mode, 0 otherwise */

extern int echelle_temps_rt[nb_max_echelles];   /* vaut 1 si rttoken dans l'echelle de temps */

extern int promethe_simulation_en_cours; /* simulation en cours ou pas, a remplacer par un mutex ? (pas utile) */



#ifndef AVEUGLE
void begin_simulation(void);
void continue_simulation(void);
#endif


extern void promethe_main(int argc, char *argv[]);
extern void promethe_quit();

#ifndef AVEUGLE
#define DECLARATION_PARAMS_CALLBACK GtkWidget * widget, gpointer data
#else
#define DECLARATION_PARAMS_CALLBACK void *widget, void *data
#endif
#define SEND_PARAMS_CALLBACK widget, data

extern void simulationb(DECLARATION_PARAMS_CALLBACK);
extern void ecriture_reseaub(DECLARATION_PARAMS_CALLBACK);
extern void set_learn_and_use_mode(DECLARATION_PARAMS_CALLBACK);
extern void set_use_only_mode(DECLARATION_PARAMS_CALLBACK);
extern void set_continue_learning_mode(DECLARATION_PARAMS_CALLBACK);
extern void set_continue_only_mode(DECLARATION_PARAMS_CALLBACK);

extern void destroy_groupes(void);
extern int cancel_pressed(DECLARATION_PARAMS_CALLBACK);
extern int step_by_step_pressed(DECLARATION_PARAMS_CALLBACK);
extern int continue_pressed(DECLARATION_PARAMS_CALLBACK);
extern int breakpoint_pressed(DECLARATION_PARAMS_CALLBACK);


void new_statistical_debug_group(int index_of_group, int number_of_iterations);
void destroy_statistical_debug_group(int index_of_group);



void profile_auto(int index_of_group, int number_of_iterations);

#endif
