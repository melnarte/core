/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef GESTION_DEBUG_H
#define GESTION_DEBUG_H

#include <graphic_Tx.h>
/*----------------------------------------------------------------*/
/*  Mode de debugging affichage des sorties des neurones          */
/*               pour tous les groupes                            */
/*----------------------------------------------------------------*/

extern void affiche_debug(TxDonneesFenetre * fenetre, int ech);
extern void affiche_debug_gpe(TxDonneesFenetre * fenetre, int gpe);


/*------------------------------------------------------------------------*/
/*               dessine le RN                                            */
/*------------------------------------------------------------------------*/

extern void affiche_rn(TxDonneesFenetre * fenetre);



/*----------------------------------------------------------------------*/
/*       affiche les poids d'un neurone                                 */
/*----------------------------------------------------------------------*/

extern void affiche_neurone2(unsigned char *im3, int xmax, int ymax,
                             char *fsortie, int n, float seuil);

/*---------------------------------------------------------------------*/

extern void affiche_neurone(unsigned char *im3, int xmax, int ymax,
                            char *fsortie);

/*---------------------------------------------------------------------*/

extern void affiche_liaisons_groupe(GtkWidget * widget, gpointer data);

/*---------------------------------------------------------------------*/

extern void affiche_breakpoint(TxDonneesFenetre *fenetre,int gpe, int  visible);
extern void affiche_selected_group(TxDonneesFenetre *fenetre, int gpe, int visible);
extern void led_groupe(TxDonneesFenetre *fenetre, int gpe, int couleur);

extern void reset_all_debug(GtkWidget * widget, gpointer data);
extern void save_script(GtkWidget * widget, gpointer data);

extern int debug_output_number;
extern int selected_group;
/*
extern void affiche_liaisons(GdkEventButton *event);
extern void select_group(GdkEventButton *event);
extern void modifier_groupe(GdkEventButton *event);
extern void supprime_debug_groupe(GdkEventButton *event);
*/

/*--------------------------------------------------------------*/
/* affiche les liaisons du neurone selectionne a la sourie      */
/*--------------------------------------------------------------*/

extern gboolean gestion_debug(GtkWidget * widget, GdkEventButton * event,
                              gpointer data);


extern void ecriture_lena(unsigned char *im3, int xmax, int ymax, int gpe);
#endif
