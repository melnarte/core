/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding bay the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup net_message_debug_dist net_message_debug_dist.h
   \ingroup generic_tools

   \details

   definition des messages echanges sur le reseau pour le debug a distance de promethe.
   dialogue entre prom_kernel et dist_prom. P. Gaussier Jan. 2008  

   \file
   \ingroup net_message_debug_dist

   Author: Arnaud Blanchard
   Created: 7/8/2009

   Fourni des defines, des macros et des fonctions de bases securisees. Utilisable aussi bien par hardhare que user.
*/

#ifndef NET_MESSAGE_DEBUG_DIST
#define NET_MESSAGE_DEBUG_DIST

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>

#include <dlfcn.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <ivy.h>
#include <basic_tools.h>
#include <semaphore.h>
#include <prom_bus.h>

#define LISTENPORT       7500
#define BACKLOG          10

#define EXT         0
#define VIDEO_EXT   1
#define AUDIO_EXT   2
#define TACTILE_EXT 3

enum {
  DISTANT_DEBUG_START = 0,
  DISTANT_DEBUG_STOP
};

enum{
  FLAG_LOCAL_DEBUG = 1 << 0,
  FLAG_LOCAL_NORMALIZE_DEBUG = 1 << 1,
  FLAG_PANDORA_DEBUG_NEURONS = 1 << 2,
  FLAG_PANDORA_DEBUG_EXT = 1 << 3,
  FLAG_PANDORA_DEBUG_NEURO_LINKS = 1 << 4
};

/** Describe status of promethe.  

The jump in the values (>10) : to separate when promethe is connected
from when no promethe communicates.
Needed when determining if telnet should reconnect or not. Cf. ivy_here_callback in themis_ivy_cb.c*/
enum prom_status{
/** From No_Not_Running to up to No_Undef : promethe is connected */
  No_Not_Running = 0, 
  No_Use_Only=1,
  No_Learn_and_Use=2,
  No_Continue_Learning=3,
  No_Continue_Using=4,

/** No_Undef and after : promethe is not connected */
  No_Undef=10,
  No_Quit=11,
  /** if terminal failed to launch promethe */
  No_Error=12, 

/**ATTENTION : Doit toujours etre superieur au max des valeurs possibles !! */
  NUMBER_OF_POSSIBLE_STATES=20
};


enum {
	ENET_PROMETHE_DESCRIPTION_CHANNEL = 0,
	ENET_DEF_GROUP_CHANNEL,
	ENET_GROUP_EVENT_CHANNEL,
	ENET_COMMAND_CHANNEL,
	ENET_DEF_LINK_CHANNEL,
	ENET_DEF_NEURON_CHANNEL,
	ENET_UPDATE_EXT_CHANNEL,
	ENET_UPDATE_PHASES_INFO_CHANNEL,
	ENET_UPDATE_LINKS,
	ENET_MAJ_NEURONE,
	ENET_MAJ_FREQ,
	ENET_UPDATE_NEURON_CHANNEL,
	ENET_NUMBER_OF_CHANNELS /*Doit toujours etre en dernier */
};

enum {
	ENET_COMMAND_STOP_OSCILLO = 0,
	ENET_COMMAND_START_OSCILLO
};

enum {
  NET_MESSAGE_DEFAULT = 0,
  NET_MESSAGE_ACK
};

#define ENET_UNLIMITED_BANDWITH 0

/* static const char *message_of_states[NUMBER_OF_POSSIBLE_STATES]; */


#define NB_MAX_ARG 20

/** Delai d'inactivation du thread_server en attente de rupture*/
#define DUREE_SLEEP_SURVEILLANCE_SECONDE 2
/* socket debug print */

#define SUCCESS 1
#define FAILURE 0

#define MAXIMUM_SIZE_OF_FILENAME 128
/*-----------------------------------------------------------------------------------------*/


#define KERNEL_SERVER_PORT   10000
#define CONSOLE_SERVER_PORT  30000
#define DEBUG_SERVER_PORT    20000

#define MAXBUF          1024
#define PARAM_MAX 			256
#define LOGICAL_NAME_MAX 	64




extern char debug_buffer[MAXBUF];
extern int socket_debug;
extern int client_debug;

extern char kernel_buffer[MAXBUF];
extern int socket_kernel;
extern int client_kernel;

extern char console_buffer[MAXBUF];
extern int socket_console;
extern int client_console;

/** Flag to specify that all the interactions are through telnet */
extern int distant_terminal; 
extern int oscillo_dist_activated;

extern char virtual_name[LOGICAL_NAME_MAX];
extern char promethe_full_id[LOGICAL_NAME_MAX+BUS_ID_MAX+2];

enum io_server_socket_state {ACTIVATED, CLOSED, SUSPENDED};

/** Implemented in blc
    Envoie un message d'erreur avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables.
    Puis exit le programme avec le parametre EXIT_FAILURE.*/
extern  void fatal_error(const char *name_of_file, const char* name_of_function,  int numero_of_line, const char *message, ...);

/**
   Envoie un message de warning avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables.
*/
extern  void print_warning(const char *name_of_file, const char* name_of_function,  int numero_of_line, const char *message, ...);

typedef struct statistic
{
  unsigned int size;
  float mean, min, max, sigma2;
}Statistic;

extern void* load_libraryf(const char *format, ...);

static inline void __attribute__((always_inline)) try_to_link_function_with_library(void *pointer_to_function, void *lib_handle, const char* name_of_function)
{
  void *symbol;
  symbol = dlsym(lib_handle, name_of_function);

  if (symbol == NULL) pointer_to_function = NULL;
  else memcpy(pointer_to_function, &symbol, sizeof(void*));
}

static inline void __attribute__((always_inline)) link_function_with_library(void *pointer_to_function, void *lib_handle, const char* name_of_function)
{
  try_to_link_function_with_library(pointer_to_function, lib_handle, name_of_function);
  if (pointer_to_function == NULL) PRINT_WARNING("la fonction %s n'a pas ete trouvee dans la librairie \n",name_of_function);
}

/** Result in ms */
static inline float __attribute__((always_inline)) diff_timespec_in_ms(struct timeval *start, struct timeval *end)
{
  return 1000.0 * (end->tv_sec - start->tv_sec) + (end->tv_usec - start->tv_usec)*0.001;
}

static inline void __attribute__((always_inline)) statistic_add_item(Statistic *stat, float value)
{
  if (stat->size <= 0)
  {
    stat->mean = value;
    stat->max = value;
    stat->min = value;
    stat->sigma2 = 0;
    stat->size = 1;
  }
  else
  {
    stat->size++;
    if (value > stat->max) stat->max = value;
    if (value < stat->min) stat->min = value;
    stat->mean += (value - stat->mean)/(float)stat->size;
    stat->sigma2 += (pow(value-stat->mean, 2))/(float)stat->size;
  }
}

static inline void __attribute__((always_inline)) close_library(void *lib_handle)
{
  dlclose(lib_handle);
}

typedef struct type_io_server_socket
{
  char name[MAXBUF];
  char comment[MAXBUF];
  enum io_server_socket_state state;
  int port;
/** pointe vers la variable globale reellement utilisee pour aller plus vite */
  int *socket_file;  
/** pointe vers la variable globale reellement utilisee pour aller plus vite */
  int *client;  
  struct sockaddr_in self;
  struct sockaddr_in client_addr;
  void (*gestionnaire_de_signaux)();
  pthread_t pthread;
} type_io_server_socket;

extern type_io_server_socket io_server_kernel;
extern type_io_server_socket io_server_debug;
extern type_io_server_socket io_server_console;


/** defined in iostrem_over_network.c */
extern void vkprints(const char *fmt, va_list ap);
extern void kprints(const char *fmt, ...);
extern void kscans(const char *fmt, ...);
extern void cscans(const char *fmt, ...);
extern void cprints(const char *fmt, ...);


#ifndef DEBUG
#define dprints(...) do{}while(0)  /* on ne fait rien et evite pbs si dprints suit un if par ex. (evite les pbs de ; )*/
#else
/** extern de basic_tools/iostream_over_network.c */
extern void true_dprints(const char *fmt, ...); 
#define dprints true_dprints
#endif

extern void cscans(const char *fmt, ...);

extern void open_io_server(type_io_server_socket *io_server, char *nom, int server_port, int *socket_file, int *client, const char *comment,void (*gestionnaire_de_signaux)());
extern void close_io_server(type_io_server_socket *io_server);
extern void global_close_io_server();

#ifndef DIAGNOSTIC
/** Fonctions possibles via ivy */
void Hello_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void help_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void status_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void autotest_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void run_online_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void run_use_only_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void continue_learning_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void continue_using_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void step_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void unstep_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void save_NN_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void save_NN_link_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void load_NN_link_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void quit_Callback (IvyClientPtr app, void *data, int argc, char **argv);
void cancel_Callback(IvyClientPtr app, void *data, int argc, char **argv);
void profile_group_callback(IvyClientPtr app, void *data, int argc, char **argv);
void connect_profiler_callback(IvyClientPtr app, void *data, int argc, char **argv);
void distant_debug_callback(IvyClientPtr app, void *data, int argc, char **argv);
void pandora_callback(IvyClientPtr app, void *data, int argc, char **argv);
void available_variable_callback(IvyClientPtr app, void *data, int argc, char **argv);
void waiting_variable_callback(IvyClientPtr app, void *data, int argc, char **argv);
void enet_server_callback(IvyClientPtr app, void *data, int argc, char **argv);


typedef struct maj_neuro_enet
{
  int no_group;
  int no_neuro;
  float s;
  float s1;
  float s2;
}maj_neuro_enet;

typedef struct type_commande
{
  char name[MAXBUF];
  void (*function)(IvyClientPtr app, void *data, int argc, char **argv);
  char help[MAXBUF];
} type_commande;

extern type_commande liste_commande[];
#endif

#endif



