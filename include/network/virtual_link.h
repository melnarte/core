/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#if !defined (__VIRTUAL_LINK__)
#define __VIRTUAL_LINK__

#include <pthread.h>
#include <semaphore.h>

#include "net_message_debug_dist.h"
#include "consts.h"
#include "debug_network_type.h"

/* Utilise pour error_flag. Ce doit être de puissances de 2 pour pouvoir cumuler les erreurs (31 possibles)*/

#define NO_DATA_TO_READ 1
#define NETLINK_NOT_CONNECTED 2


typedef struct virtual_link_s
{
  char name[STR_LIMIT];
  unsigned int num_groupe;
  unsigned int network_link;
  unsigned short mode;
  unsigned short raz; 
  unsigned short img;
  unsigned short block;
  unsigned short ack;
  unsigned short fft;
  unsigned short next;
  unsigned short timeout;
  unsigned int nbsec_timeout;
  unsigned int nbwait_raz;
  unsigned int nb_iter_since_raz;
  pthread_mutex_t mut_data;
  char* data;
  unsigned int size_data;
  unsigned int size_max_data;
  unsigned short read_data;
  /** en plus de data, gestion separee des donnees liees au ext */
  char* data_ext;
  unsigned int size_data_ext;
  unsigned int size_max_data_ext;
  short read_data_ext;
  sem_t new_message;
  sem_t wait_ack;
  pthread_mutex_t mut_send;
  unsigned long error_flag;
  Statistic waiting_time;
  /** index of the neuron that can set to noext / -1 if not used */
  int noext_set;
  /** pointer on next virtual link for the same network */
  struct virtual_link_s *vl_next;
/** debug_network ptr */
  debug_network_t debug;
} virtual_link_t;

int virtual_link_find(char* name);

virtual_link_t* virtual_links_set_flags(char* link_info);

int virtual_link_send_data(virtual_link_t *vlink);

int virtual_link_send_ack(virtual_link_t *vlink);

void virtual_link_init(virtual_link_t *vlink, char* name, int network_link, unsigned int mode);

void virtual_link_close(virtual_link_t *vlink);

void virtual_link_disconnect(int num_netlink);

/** set debug server period */
void recv_server_set_debug(virtual_link_t *vlink, int debug_period);

/** set debug server period */
void recv_server_update_debug(virtual_link_t* vlink, unsigned int update_ext_debug);

/** set debug server period */
void recv_server_init_ext_management(virtual_link_t* vlink);

/** update try reading info */
void recv_server_update_debug_try_reading(virtual_link_t* vlink);

#endif /* __VIRTUAL_LINK__ */
