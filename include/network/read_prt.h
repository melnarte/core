/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#if !defined (__READ_PRT__)
#define __READ_PRT__

/* Constantes pour lire le fichier prt */
#define TOKEN_LIMIT	255
#define LINE_TOKENS	4
#define BEGIN_TOKEN     "BEGIN"
#define END_TOKEN       "END"
#define NETWORK_TOKEN   "NETWORK"
#define LINK_TOKEN   	"LINK"
#define LINK_TCP	"TCP"
#define LINK_UDP	"UDP"
#define LINK_IVY	"IVY"
#define LINE_LIMIT	LINE_TOKENS * TOKEN_LIMIT

/* Nombre max de machines et de liens réseaux */
#define HOSTS_LIMIT	256
#define LINKS_LIMIT	256

/* Les différents protocole de communication utilisés */
typedef enum prt_protocol_e
{
	P_TCP,
	P_UDP,
	P_IVY
} prt_protocol_t;

/* Utilisé pour les différentes sections du fichier prt */
typedef enum prt_section_e
{
	NETWORK_SECTION,
	LINK_SECTION,
	NO_SECTION
} prt_section_t;

/* Structure utilisée pour contenir les différentes machines du réseau*/
typedef struct prt_host_s
{
  char vname[HOSTS_LIMIT];				/* Nom virtuel */
  char name[HOSTS_LIMIT];					/* Nom ou ip */
  unsigned short port;		/* TCP/IP Port */
} prt_host_t;

/* Structure utilisée pour contenir les différents liens réseau */
typedef struct prt_link_s
{
	char name[LINKS_LIMIT];					/* Nom du lien */
	unsigned int source;		/* Numero de machine source */
	unsigned int target;		/* Numero de la machine destinataire */
	prt_protocol_t protocol;	/* Protocol utilisé pour le lien réseau */
} prt_link_t;


typedef struct prt_s
{
	prt_host_t *hosts;			/* Tableau des differents hotes */
	unsigned int num_hosts;		/* Nombre d'hotes */
	int my_host;		/* Le numero de notre hote */
	prt_link_t *links;			/* Tableau des différents liens */
	unsigned int num_links;		/* Nombre de liens */
} prt_t;

/**
* Parse le fichier de configuration du réseau .prt
* Initialise une structure de type prt_t et stocke les information du fichier prt dedans
* retourne un pointeur vers une structure prt si le fichier de configuration éxiste sinon retourne NULL
*/
prt_t* prt_init(const char* file_name, const char* virtual_name);

/**
* Libère la mémoire allouée pour une structure de type prt_t 
*/
void prt_free(prt_t *prt);

#endif /* __READ_PRT__ */


