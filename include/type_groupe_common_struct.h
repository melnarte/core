/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** contenu de base pour typedef struct type_groupe */

char no_name[SIZE_NO_NAME]; /** numero symbolique utile pour des scripts symboliques complexes */
char nom[TAILLE_CHAINE]; /** nom du groupe pour entree ou sortie Numero du groupe pour utilisateur */
int no;

int posx; /**   position X du centre du groupe      */
int posy; /**   position Y du centre du groupe      */

int p_posx; /**   position du centre du groupe      */
int p_posy; /**  dans le debug de promethe          */

int premier_ele; /** no du premier neurone du groupe     */

int reverse; /** 1: orientation inverse pour dessin  */
int nbre_voie; /** nbre de voies de liaisons           */
int fault; /** pour dire si un groupe est en faute */

int deja_active; /** pour eviter de le re-etudier  */
int deja_appris;
int ech_temps; /** echelle temporelle pour savoir quand l'executer */

int selected; /** pour les futures ameliorations de l'IHM : selection multiple de groupes a deplacer... */
int debug; /** 1 si le groupe doit etre affiche lors du debug ... , voir FLAGs in net_message_debug_dist.h */

int type; /** type du groupe */

my_int( type2); /** specifie si c'est une sortie pour rt_token : numero correspondant a la priorite du token */

my_int( taillex); /** nbre de lignes de la carte          */
my_int( tailley); /** nbre de colonnes de la carte        */

my_int( nbre); /** nbre de neurones dans le groupe     */

my_float( seuil); /** seuil pour chaque neurone si diff 0 */
my_float( tolerance); /** pour neurones a trois etats, colonne */

my_int( dvp);
my_int( dvn); /** distance voisinage d diff et compet */

my_float( alpha); /** coeff pour le renforcement dans PTM */
my_float( nbre_de_1); /** estimation normalisation pour PTM   */
my_float( sigma_f); /** constante pour les fcts de sortie   */

my_float( learning_rate); /** simulation speed   */
my_float( simulation_speed);/** learning rate   */
my_float( noise_level);

float tableau_gaussienne[taille_max_tableau_diff_gaussienne]; /** init diff si carte           */

/** valeur de retour lors de l'utilisation du gpe: 0 si le groupe n'a
 pu s'executer, 1 si OK, 2 si un break intervient et 3 si le groupe a
 deja ete detruit et le thread normalement recupere */
int return_value;

int rttoken; /**Rend le groupe directement activable. Gestion Execution temps reel */

my_int( nb_threads); /**nombre de threads lors du calcul de la mise a jour M.M. 07/02/07*/

int breakpoint; /** flag pour gerer les breakpoints */

