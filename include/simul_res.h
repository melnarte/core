/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef SIMUL_RES_H
#define SIMUL_RES_H

/*--------------------------------------------------------------*/
/*            SIMULATION D'UN RESEAU DE NEURONE                 */
/*       simul_res.h      Ph GAUSSIER                          */
/*  auto organisation , Winner-take-all , struc groupe diff     */
/*    sortie >0                                                 */
/*   version generale vecteurs entrees sortie Kohonen           */
/*                                                              */
/* doit lire le scripte du systeme en plus et seuil neurone     */
/*                                                              */
/*  Convention :                                                */
/*    Les premiers neurones correspondent a des entrees (No)    */
/*    Les derniers correspondent aux sorties (No)               */
/*                                                              */
/* nouvelle version Dec 1993                                    */
/* tient compte du temps de memorisation des liaisons           */
/*--------------------------------------------------------------*/


/*--------------------------------------------------------------*/
/*                                                              */
/* Fonctions utilisables de l'exterieur:                        */
/*                                                              */
/*   .lecture_reseau(freseau)       |                           */
/*   .lecture_entree(fentree)       |                           */
/*   .lecture_corrige(fcorrige)     |                           */
/*   .ecriture_sortie(fsortie)      |                           */
/*   .ecriture_reseau(freseau)      |                           */
/*                                                              */
/*   .simulation()                                              */
/*   .float taux_succes()                                       */
/*   .apprend()                                                 */
/*                                                              */
/*   .affiche_neurone(im,xmax,ymax,fsortie)                     */
/*   .ecriture_lena(im,xmax,ymax,gpe,fsortie)                   */
/*                                                              */
/*   .fabrique_entree()                                         */
/*                                                              */
/*   .mise_a_jour(n,learn)          |                           */
/*   .simul_1(n,learn)              |                           */
/*                                                              */
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*  coefficients modif et contenant leur regle d'utilisation    */
/*  variable coeff->type =                                      */
/*                          0 :   ei*wij    non modifiable      */
/*                          1 : |ei - wij |     modifiable      */
/*                          2 : |ei - wij | non modifiable      */
/*                          3 :   ei*wij        modifiable      */
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*   fonctionnement des neurones :                              */
/*   l'entree prise en compte est la moyenne de l'entree reelle */
/*   moyenne prise sur une periode dependant de la liaison      */
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/* seuil pour les neurones de facon a simuler plus facilement ET*/
/* pour le CMAC ou les neurones Sigma-Pi en binaire             */
/*--------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*                    SIMULATION DU RESEAU                            */
/*--------------------------------------------------------------------*/

extern void initialise_sortie_reseau();

/*--------------------------------------------------------------------*/

//extern void initialise_STM();
extern void recherche_max();
extern void apprend_winner();
extern void apprend_normal();

/*--------------------------------------------------------------------*/
/*            n'apprend pas si le neurone n'est pas active            */
/*            poids>=0 pour ceux appris                               */
/*            comme le type special - correlation                     */
/*            OU entre les entrees modifiables                        */
/*--------------------------------------------------------------------*/

extern void apprend_accumul();

/*--------------------------------------------------------------------*/
/*     apprend_winner_correl() proc simplifiee de apprend_winner()    */
/*     apprend image entree que ce qui est actif pour correlation     */
/*     apprentissage par liaisons non modifiables coeff>1 pour ne     */
/*     pas etre influence par autres cas deja appris tjs m gagant     */
/*--------------------------------------------------------------------*/

extern void apprend_winner_correl();

/*--------------------------------------------------------------------*/
/* il doit y a voir une erreur, faire attention a ne garder que max   */
/* pb lorsque liaison non modifiable on ne passe pas dans la boucle   */
/*           !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!             */
/*--------------------------------------------------------------------*/

extern void apprend_kohonen();

extern void gestion_groupe_kohonen();

extern void gestion_groupe_generique_winner();

extern void mise_a_jour_neurone_kohonen();

extern void mise_a_jour_neurone_special();

extern void mise_a_jour_neurone_entree();

extern void mise_a_jour_neurone_trad();
#endif
