/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef OUTILS_H
#define OUTILS_H

#include "reseau.h"
#include <stdarg.h>

/** For parsing links */
typedef union {
   int integer;
   float real;
   char const *string;
   type_groupe const *group;
} type_link_option_value;

typedef struct {
   char const *name;
   char const *help;
   enum {
      OPTIONAL_INT, OPTIONAL_FLOAT, OPTIONAL_STRING, OPTIONAL_INPUT_GROUP, FLAG,
      REQUIRED_INT, REQUIRED_FLOAT, REQUIRED_STRING, REQUIRED_INPUT_GROUP
   }type;
   type_link_option_value default_value;
} type_link_option;

/* Les outils de base sont definis dans net_message_debug_dist.h (include/Tools/basic_tools.h (eg. EXIT_ON_ERROR, ALLOCATION, link_function_with_library ... */

/*---------------------------------------------------------------*/
/*      CREATION DU TABLEAU CONTENANT LE RESEAU                  */
/*---------------------------------------------------------------*/

extern type_tableau creer_reseau(int n);    /*liste des neurones              */

extern type_tableau_pando creer_reseau_para_supp_pando(int n); /*liste des neurones              */

extern type_tableau_voies creer_voies(int n);   /*liste des liens indexee          */

/*--------------------------------------------------------------*/

extern type_noeud **creer_groupe(int n);    /*liste neurones du groupe  */

/*--------------------------------------------------------------*/
/*      reservation de la place pour 1 coefficient synaptique   */
/*--------------------------------------------------------------*/

extern type_coeff *creer_coeff(void);

/*--------------------------------------------------------------*/
/*     reservation de la place pour 1 neurone d'un groupe        */
/*--------------------------------------------------------------*/

extern type_noeud *creer_noeud(void);

/*---------------------------------------------------------------*/
/*             CREATION DU TABLEAU CONTENANT LES DONNEES         */
/*---------------------------------------------------------------*/

extern type_matrice creer_matrice(int n, int m);

/*-----------------------------------------------------------------*/
/*                CREATION VECTEUR                                 */
/*-----------------------------------------------------------------*/

extern type_vecteur_entier creer_vecteur(int n);

/*-------------------------------------------------------------*/
/*   Allocation de la memoire pour la creation des noeuds      */
/*-------------------------------------------------------------*/




/*--------------------------------------------------------------*/

extern int trouver_entree(int gpe, const char *s);

/*---------------------------------------------------------------*/

extern int trouver_nbre_entree(int gpe);


extern void init_input_link_matrix(void);
/**
 cherche dans une table le numero du xeme groupe de lien connecte en
 entree du groupe considere si c'est la premiere fois que cette
 requete est lance alors la fonction recherche et enregistre tous les
 liens du groupe de maniere a accelerer les recherches suivantes.
 retourne -1 s'il n'y a pas de numero de lien correspondant  */
extern int find_input_link(int group, int link_number);

/* get the parameters given on one specific link */
void get_parameters_on_link(int gpe, type_liaison *link, type_link_option *link_options, char const* option_name0, void *result_pt0, ...);

/* get parameters given on all links */
void get_parameters(int gpe, type_link_option *link_options, char const* option_name0, void *result_pt0, ...);

/*mask pour les eventuels threads crees*/
extern void gestion_mask_signaux();

extern type_groupe* trouver_groupe(int gpe);

/*
#ifdef DEBUG
#define prom_debug(format, arg...) printf("%s: " format "\n" , \
	__FILE__ , ## arg)
#else
#define prom_debug(format, arg...) do {} while (0)
#endif

*/


#endif
