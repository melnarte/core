/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef PUBLIC_H
#define PUBLIC_H

/*-----------------------------------------------------------------*/
/*   Promethe : simulateur de reseau de neurones                   */
/*   Copyright Philippe GAUSSIER 1992                              */
/*-----------------------------------------------------------------*/



/*-------------------------------------------------------------------*/
/*   gere le sequencement des actions dans le reseau                 */
/*   utilise uniquement pour la partie algo                          */
/*   la partie RN fonctionne en mode synchrone qd sequencement fini  */
/*                                                                   */
/*   commence toujours par le noeud debut                            */
/*   lorsque un noeud a toute ses entrees actives, il s'active       */
/*   une fois active, il devient deja active                         */
/*   lorsque un groupe a toute ses entrees deja activees il s'active */
/*                                                                   */
/*   peut declencher un groupe si une liaison secondaire n'a pas de  */
/*   jeton en entree, si la liaison n'est pas secondaire: impossible */
/*-------------------------------------------------------------------*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>


#include <net_message_debug_dist.h>
#include "graphic_Tx.h"
#include "graphique.h"
#include "libx.h"

/*---------------------------------------------------------------------*/
/* variables et structures liees au graphisme                          */
/*---------------------------------------------------------------------*/

#define EndOfLine                FALSE
#define dseuil 20
#define Edge                     TRUE
#define SimplexSansSerif         4
#define BoldSansSerif            6

#define taille_max_fenetre_x  3000
#define taille_max_fenetre_y  3000

extern int gpe_courant_a_afficher;
extern int rapide;
extern int nbre_element_gpe_courant;
extern int premier_element_gpe_courant;

extern int *pos_n_x;            /* position de tous les neurones dans l'image */
extern int *pos_n_y;
extern int position_neurones_deja_initialisee;
extern int xmax_image, ymax_image;  /* taille de l'image traitee           */

extern int debug;
extern int p_trace;


extern TxDonneesFenetre image1, image2;


/*
#define TRUE  1
#define FALSE 0
*/

/*---------------------------------------------------------------------*/

#include "structlena.h"
#include "reseau.h"
#include "gestion_ES.h"
#include "gestion_debug.h"
#include "gestion_sequ.h"
#include "lec_config.h"
#include "outils.h"
#include "promethe.h"
/*#include "reduit.h" */
/*#include "simul_res.h"*/
#include "script.h"
#include "main.h"

#include "callbacks.h"
#include "interface.h"
/*---------------------------------------------------------------*/
/*       VARIABLES  GLOBALES A L'ENSEMBLE DU PROGRAMME           */
/*---------------------------------------------------------------*/

extern type_tableau neurone;    /*tableau contenant l'ens du reseau   */
extern type_tableau_pando info_neurone_pour_pando;
extern type_tableau_voies liaison;

extern type_matrice donnee;
extern type_matrice corrige;
extern type_matrice resultat;

extern type_groupe def_groupe[];
extern type_pando_groupe para_pando_group[];

extern int input_link_number[nb_max_groupes][nb_max_liaisons_pour_un_groupe];


extern char nom_groupe[nb_max_groupes][taille_chaine];  /* nom associe a un gpe, 200 gpes max */

extern int nbre_donnee;         /* nombre de vecteurs a etudier       */
extern int nbre_groupe;         /* nombre de groupes de neuones compet */
extern int nbre_neurone;        /* nombre de neurones dans le reseau  */
extern int nbre_entree;         /* nbre de vecteurs utilises en entree */
extern int nbre_sortie;         /* nbre de neurones utilises en sortie */

extern int cycle;               /*nombre de cycles de calcul avec 1 ex */
extern float periode;           /*periode moyennage entree            */

extern int nbre_couche;

extern float eps;               /* epsilon pour la modif des coeff    */

extern float tableau_gaussienne[30];



/*---------------------------------------------------------------*/

extern float variable_hasard;
extern int fildes;
extern char nomfich1[], caract[], freseau[], fscript[], fconfig[], fgcd[];
extern char argv_fichier[nb_fichiers][taille_chaine];


extern unsigned char *im4;
/*----------------------------------------------------------------------------*/
/*                     gestion des donnees relatives au script                */
/*----------------------------------------------------------------------------*/

extern int nbre_groupe, nbre_liaison;


extern unsigned char *im, *im_recons, *im_recons2;


/*-----------------------------------------------------------*/

extern FILE *f1;
extern descripteur decri;


/*----------------------------------------------------------*/

extern int max_ech_temps;       /* on prend max lors de la lecture du script */
               /* sert pour la gestion_sequ.h               */
               /* evite calculs inutiles car repetitif ident */


/*-----------------------------------------------------------*/

extern int echelle_temps[];     /* nbre de cycles par echelle                */
                /* lu dans le fichier config                 */


extern int echelle_temps_dynamique[];   /* vaut 1 si ech decalee pour cause action apprise sur un transitoire */


extern int taille_standart_trans;

extern float vigilence;         /* variable entre 0 et 1                        */
            /* determine l'etat de vigilence du systeme     */
            /* il s'agit d'une constante multiplicative     */
            /* pour un seuil                                */
extern int flag_normalisation;

#endif
